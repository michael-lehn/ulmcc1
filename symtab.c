#include <stdlib.h>

#include "codegen.h"
#include "instr_ulm.h"
#include "memregion.h"
#include "msg.h"
#include "symtab.h"
#include "string.h"

enum Namespace {
    NS_IDENT = 0,
    NS_TYPE,
    NS_CONST,
    NS_COUNT_,
};

static struct Symtab global[NS_COUNT_];
static struct Symtab *prev[NS_COUNT_]; 
static struct Symtab *curr[NS_COUNT_] = {
    &global[NS_IDENT],
    &global[NS_TYPE],
    &global[NS_CONST],
};


static struct Symbol *
findInSymtab(const char *ident, const struct Symtab *symtab)
{
    if (! ident || ! symtab) {
	return 0;
    }
    for (struct SymtabEntry *e = symtab->first; e; e = e->next) {
	if (ident == e->symbol.ident) {
	    return &e->symbol;
	}
    }
    return 0;
}

static struct Symbol *
findInCurrScope(const char *ident, enum Namespace ns)
{
    return findInSymtab(ident, curr[ns]);
}

static struct Symbol *
findInAnyScope(const char *ident, enum Namespace ns)
{
    struct Symtab *symtab = curr[ns];
    do {
	struct Symbol *found = findInSymtab(ident, symtab);
	if (found) {
	    return found;
	}
    } while ((symtab = symtab->up));
    return 0;
}

static void
openScope_(enum Namespace ns, enum ScopeKind kind)
{
    struct Symtab *symtab = alloc(SYMTAB, sizeof(*symtab));
    symtab->kind = kind;
    symtab->up = curr[ns];
    symtab->len = 0;
    symtab->maxAlign = 1;
    symtab->offset0 = kind == BLOCK ? symtab->up->offset : 0;
    symtab->minOffset = symtab->offset = symtab->offset0;
    symtab->first = symtab->last = 0;

    prev[ns] = 0;
    curr[ns] = symtab;
}

static void
closeScope_(enum Namespace ns)
{
    if (curr[NS_IDENT]->kind == BLOCK) {
	if (curr[ns]->minOffset > curr[ns]->offset) {
    	    curr[ns]->minOffset = curr[ns]->offset;
    	}
    	if (curr[ns]->up->minOffset > curr[ns]->minOffset) {
    	    curr[ns]->up->minOffset = curr[ns]->minOffset;
    	}
    }
    prev[ns] = curr[ns];
    curr[ns] = curr[ns]->up;
}

static void
reopenScope_(enum Namespace ns)
{
    if (! prev[ns]) {
	fprintf(stderr, "internal error: can not reopen previous scope");
	exit(2);
    }
	
    curr[ns] = prev[ns];
    prev[ns] = 0;
}

void
openScope(enum ScopeKind kind)
{
    switch (kind) {
	case PARAM_LIST:
	case BLOCK:
	    openTypeScope();
	    openScope_(NS_IDENT, kind);
	    openScope_(NS_TYPE, kind);
	    break;
	
	case STRUCT_FIELD_LIST:
	case UNION_FIELD_LIST:
	    openScope_(NS_IDENT, kind);
	    break;

	default:
	    fprintf(stderr, "internal error: openScope with kind = %d\n",
		    kind);
	    exit(2);
    }
}


void
reopenPreviousScope()
{
    reopenPreviousTypeScope();
    reopenScope_(NS_IDENT);
    reopenScope_(NS_TYPE);
    if (curr[NS_IDENT]->kind != PARAM_LIST) {
	fprintf(stderr, "internal error: previous scope kind = %d\n",
		curr[NS_IDENT]->kind);
	exit(2);
    }
    curr[NS_IDENT]->kind = BLOCK;
    curr[NS_TYPE]->kind = BLOCK;
}

size_t
closeScope(enum ScopeKind kind)
{
    ptrdiff_t stackFrame = 0;
    switch (kind) {
	case PARAM_LIST:
	case BLOCK:
	    closeTypeScope();
	    closeScope_(NS_IDENT);
	    closeScope_(NS_TYPE);
	    stackFrame = -curr[NS_IDENT]->minOffset;
	    break;

	case UNION_FIELD_LIST:
	case STRUCT_FIELD_LIST:
	    closeScope_(NS_IDENT);
	    break;

	default:
	    fprintf(stderr, "internal error: closeScope with kind = %d\n",
		    kind);
	    exit(2);
    }
    return roundUp(stackFrame, 8);
}

static struct Symbol *
append_(size_t ns, struct SymtabEntry *entry)
{
    if (curr[ns]->last) {
	curr[ns]->last = curr[ns]->last->next = entry;
    } else {
	curr[ns]->last = curr[ns]->first = entry;
    }
    ++curr[ns]->len;
    if (entry->symbol.storageClass == SC_STACK) {
	ptrdiff_t align = entry->symbol.type->align;
	ptrdiff_t size = entry->symbol.type->size;

	curr[ns]->maxAlign = maxAlign(curr[ns]->maxAlign, align);

	if (curr[ns]->kind == UNION_FIELD_LIST) {
	    if (size > curr[ns]->offset) {
		curr[ns]->offset = size;
	    }
	    entry->symbol.memLoc.offset = 0;
	} else if (curr[ns]->kind == STRUCT_FIELD_LIST) {
	    curr[ns]->offset = roundUp(curr[ns]->offset, align);
	    entry->symbol.memLoc.offset = curr[ns]->offset;
	    curr[ns]->offset += size;
	} else {
	    curr[ns]->offset = roundDown(curr[ns]->offset - size, align);
	    entry->symbol.memLoc.offset = curr[ns]->offset;
	}
    }
    if (curr[ns] == &global[ns] || entry->symbol.storageClass == SC_EXTERN) {
	// string literals already have a memLoc name
	if (! entry->symbol.memLoc.name) {
	    entry->symbol.memLoc.name = entry->symbol.ident;
	    entry->symbol.memLoc.offset = 0;
	}
    } else if (entry->symbol.storageClass == SC_STATIC) {
	entry->symbol.memLoc.name = makeLabel(entry->symbol.ident);
	if (global[ns].last) {
	    global[ns].last = global[ns].last->next = entry;
	} else {
	    global[ns].last = global[ns].first = entry;
	}
    }
    
    return &curr[ns]->last->symbol; 
}

const struct Symbol *
addSignedLiteral(int64_t val, const char *ident)
{
    if (findInCurrScope(ident, NS_IDENT)) {
	fprintf(stderr, "internal error: 'addSignedLiteral'\n");
	exit(2);
    }
    struct SymtabEntry *entry = alloc(SYMTAB, sizeof(*entry));
    entry->next = 0;
    entry->symbol.ident = ident;
    entry->symbol.type = makeSignedIntegerType(8);
    entry->symbol.storageClass = SC_LITERAL;
    entry->symbol.u.value.int64 = val;
    entry->symbol.memLoc.name = 0;
    entry->symbol.memLoc.offset = -667;
    return append_(NS_IDENT, entry);
}

const struct Symbol *
getStringLiteral(const char *str)
{
    for (struct SymtabEntry *e = curr[NS_CONST]->first; e; e = e->next) {
	if (e->symbol.type->kind == ARRAY && e->symbol.u.value.s.str == str) {
	    return &e->symbol;
	}
    }
    return 0;
}

const struct Symbol *
addStringLiteral(const char *str, const char *escStr)
{
    struct SymtabEntry *entry = alloc(SYMTAB, sizeof(*entry));
    entry->next = 0;
    entry->symbol.ident = escStr;
    entry->symbol.type = makeArrayType(makeUnsignedIntegerType(1),
				       getStringLen(str));
    entry->symbol.storageClass = SC_LITERAL;
    entry->symbol.u.value.s.str = str;
    entry->symbol.memLoc.name = makeLabel("str");
    entry->symbol.memLoc.offset = 0;
    return append_(NS_CONST, entry);
}

const struct Symbol *
addSymbol(struct TokenLoc loc, const char *ident,
	  enum StorageClass storageClass,
	  const struct Type *type)
{
    struct Symbol *found = findInCurrScope(ident, NS_IDENT);
    if (found) {
	if (storageClass == SC_STACK) {
	    errorMsg(&loc, "redeclaration of '%s'", ident);
	    errorMsg(&found->loc, "previous declaration of '%s' was here",
		     ident);
	    exit(1);
	} else if (found->storageClass == SC_TYPEDEF) {
	    if (storageClass != SC_TYPEDEF) {
		errorMsg(&loc, "'%s' redeclared as different kind of symbol",
			 ident);
		errorMsg(&found->loc, "previous declaration of '%s' was here",
			 ident);
		exit(1);
		return 0;
	    } else if (found->type != type) {
		errorMsg(&loc, "conflicting types for '%s'", ident);
		errorMsg(&found->loc, "previous declaration of '%s' was here",
			 ident);
		exit(1);
		return 0;

	    }
	    return found;
	}
	if (found->storageClass != SC_STATIC && storageClass == SC_STATIC) {
	    errorMsg(&loc, "static declaration of '%s' follows non-static"
		     " declaration.", ident);
	    errorMsg(&found->loc, "previous declaration of '%s' was here",
		     ident);
	    exit(1);
	    return 0;
	}
	if (found->storageClass == SC_STATIC && storageClass != SC_STATIC
	    && storageClass != SC_EXTERN)
	{
	    errorMsg(&loc, "non-static declaration of '%s' follows static"
		     " declaration.", ident);
	    errorMsg(&found->loc, "previous declaration of '%s' was here",
		     ident);
	    exit(1);
	    return 0;
	}
	if (found->type->kind == ARRAY && found->type->p.dim ==0
	    && found->type->u.ref == type->u.ref)
	{
	    found->type = type;
	    found->storageClass = storageClass;
	    return found;
	} else if (type != found->type) {
	    errorMsg(&loc, "redeclaration of '%s' with different type", ident);
	    errorMsg(&found->loc, "previous declaration of '%s' was here",
		     ident);
	    printf("new type: '%p'\n", (void *) type);
	    printType(type); printf("\n");
	    printf("previous type: '%p'\n", (void *) found->type);
	    printType(found->type); printf("\n");
	    exit(1);
	    return 0;
	} else {
	    found->storageClass = storageClass;
	    return found;
	}
    }

    if (! type->size) {
	if (type->kind == ARRAY && curr[NS_IDENT]->kind == UNION_FIELD_LIST) {
	    errorMsg(&loc, "flexible array member '%s' in a union is not"
		     " allowed", ident);
	    printType(type);
	    return 0;
	} else if (type->kind != ARRAY
		   && curr[NS_IDENT]->kind != STRUCT_FIELD_LIST
		   && (curr[NS_IDENT]->kind != PARAM_LIST
		       || curr[NS_IDENT]->len == 0))
	{
	    errorMsg(&loc, "'%s' has incomplete type.", ident);
	    printType(type);
	    return 0;
	}
    }
    /*
    if (curr[NS_IDENT]->last && curr[NS_IDENT]->last->symbol.type->size == 0) {
	errorMsg(&curr[NS_IDENT]->last->symbol.loc, "flexible array member"
		 " '%s' is not at the end of struct",
		 curr[NS_IDENT]->last->symbol.ident);
	noteMsg(&loc, "next field declaration '%s' is here.", ident);
	return 0;
    }
    */

    struct SymtabEntry *entry = alloc(SYMTAB, sizeof(*entry));
    entry->next = 0;
    entry->symbol.loc = loc;
    entry->symbol.ident = ident;
    entry->symbol.type = type;
    entry->symbol.storageClass = storageClass;
    entry->symbol.init = 0;
    entry->symbol.memLoc.name = 0;
    entry->symbol.memLoc.offset = -668;

    if (type->kind == TK_STRUCT || type->kind == TK_UNION) {
	entry->symbol.u.members = type->u.tag->u.members;
    }

    return append_(NS_IDENT, entry);
}

const struct Symbol *
getSymbol(const char *ident)
{
    return findInAnyScope(ident, NS_IDENT);
}

static const struct Expr *
initSymbol_(bool global, const char *ident, const char *name, ptrdiff_t offset,
	    const struct Type *type, const struct Expr *init)
{
    struct Expr *expr = 0;
    switch (type->kind) {
	case ARRAY:
	    if (init && init->op != EO_LIST) {
		errorMsg(&init->loc, "initializer list expected");
		exit(1);
	    }
	    if (init) {
		expr = makeExprList(init->loc);
		struct ExprListItem *item = init->u.l.first;

		size_t index;
		for (index = 0; item; ++index, item = item->next) {
		    if (index >= type->p.dim) {
			break;
		    }
		    init = initSymbol_(global, 0, name, offset,
				       unqualType(type->u.ref),
				       item->expr);
		    exprListAppend(expr, init);
		    offset += type->u.ref->size;
		}
		for (; global && index < type->p.dim; ++index) {
		    init = initSymbol_(global, 0, name, offset,
				       unqualType(type->u.ref), 0);
		    exprListAppend(expr, init);
		    offset += type->u.ref->size;
		}
		if (item) {
		    warningMsg(&item->expr->loc, "excess elements in array"
			       " initializer");
		}
	    } else if (global) {
		expr = makeExprList(tokenLoc);
		for (size_t index = 0; index < type->p.dim; ++index) {
		    init = initSymbol_(global, 0, name, offset, type->u.ref, 0);
		    exprListAppend(expr, init);
		    offset += type->u.ref->size;
		}
	    }
	    break;

	case TK_ENUM:
	    expr = makeRefExpr(tokenLoc, ident, name, offset, type);
	    expr = makeBinaryExpr(tokenLoc, EO_ASSIGN, expr, init);
	    break;

	case TK_STRUCT:
	    {
		if (init && init->op != EO_LIST) {
		    expr = makeRefExpr(tokenLoc, ident, name, offset, type);
		    expr = makeBinaryExpr(tokenLoc, EO_ASSIGN, expr, init);
		    break;
		}
		expr = makeExprList(init ? init->loc : tokenLoc);
		struct ExprListItem *item = init ? init->u.l.first : 0;
		const struct SymtabEntry *mem = type->u.tag->u.members->first;
		for (; mem && item; mem = mem->next, item = item->next) {
		    ptrdiff_t displ = offset + mem->symbol.memLoc.offset;
		    const struct Type *t = unqualType(mem->symbol.type);
		    init = initSymbol_(global, 0, name, displ, t, item->expr);
		    exprListAppend(expr, init);
		}
		if (global) {
		    for (; mem; mem = mem->next) {
			ptrdiff_t displ = offset + mem->symbol.memLoc.offset;
			const struct Type *t = unqualType(mem->symbol.type);
			init = initSymbol_(global, 0, name, displ, t, 0);
			exprListAppend(expr, init);
		    }
		}
		if (item) {
		    warningMsg(&item->expr->loc, "excess elements in array"
			       " initializer");
		}
	    }
	    break;

	case INTEGER:
	case POINTER:
	    if (init && init->op == EO_LIST) {
		errorMsg(&init->loc, "scalar initializer expected");
		exit(1);
	    }
	    if (global && init && !init->is_const) {
		errorMsg(&init->loc, "initializer element is not a compile-time"
			 " constant");
		printf("type of element: ");
		printType(init->type);
		printf("\n");
		exit(1);
	    }
	    if (! init) {
		init = makeSIntExpr(tokenLoc, type->size, 0);
	    }
	    expr = makeRefExpr(tokenLoc, ident, name, offset, type);
	    expr = makeBinaryExpr(tokenLoc, EO_ASSIGN, expr, init);
	    break;

	default:
	    fprintf(stderr, "internal error 'initSymbol_'\n");
	    exit(2);
    }
    return expr;
}

const struct Expr *
initSymbol(const char *ident, const struct Expr *init)
{
    struct Symbol *sym = findInCurrScope(ident, NS_IDENT);
    if (! sym) {
	fprintf(stderr, "internal error 'initSymbol'\n");
	exit(2);
    } else if (sym->init) {
	errorMsg(&init->loc, "redefinition of '%s'", ident);
	errorMsg(&sym->loc, "previous definition of '%s' was here", ident);
	exit(1);
    }
    sym->init = init;
    bool global = sym->storageClass == SC_STATIC || !curr[NS_IDENT]->up;
    const struct Expr *expr = initSymbol_(global, ident, sym->memLoc.name,
					  sym->memLoc.offset,
					  unqualType(sym->type), init);
    return expr;
}

enum LookupIn {
    ANY_SCOPE,
    CURR_SCOPE,
};

static struct Symbol *
taggedType_(enum LookupIn scope,  struct TokenLoc loc, enum TypeKind kind,
	    const char *ident)
{
    struct Symbol *found = scope == ANY_SCOPE
	? findInAnyScope(ident, NS_TYPE)
	: findInCurrScope(ident, NS_TYPE);
    if (found) {
	if (found->type->kind != kind) {
	    errorMsg(&loc, "redeclaration of '%s' with different type.", ident);
	    errorMsg(&found->loc, "previous definition of '%s'", ident); 
	    exit(1);
	    return 0;
	}
	return found;
    }

    struct SymtabEntry *entry = alloc(SYMTAB, sizeof(*entry));
    entry->next = 0;
    entry->symbol.loc = loc;
    entry->symbol.ident = ident;
    switch (kind) {
	case TK_STRUCT:
	    entry->symbol.type = makeIncompleteStructType(&entry->symbol);
	    break;
	
	case TK_UNION:
	    entry->symbol.type = makeIncompleteUnionType(&entry->symbol);
	    break;

	case TK_ENUM:
	    entry->symbol.type = makeIncompleteEnumType(&entry->symbol);
	    entry->symbol.type = completeTaggedType(&entry->symbol, 8, 8);
	    break;

	default:
	    fprintf(stderr, "internal error: in 'addTaggedType'\n");
	    exit(2);
    }
    entry->symbol.storageClass = SC_NONE;
    entry->symbol.u.members = 0;
    entry->symbol.memLoc.name = 0;
    entry->symbol.memLoc.offset = -666;
    return append_(NS_TYPE, entry);
}

struct Symbol *
addTaggedType(struct TokenLoc loc, enum TypeKind kind, const char *ident)
{
    return taggedType_(CURR_SCOPE, loc, kind, ident);
}

const struct Symbol *
getTaggedType(struct TokenLoc loc, enum TypeKind kind, const char *ident)
{
    return taggedType_(ANY_SCOPE, loc, kind, ident);
}

bool
addParam(struct TokenLoc loc, const char *ident, const struct Type *type)
{
    if (curr[NS_IDENT]->kind != PARAM_LIST) {
	fprintf(stderr, "internal error: adding paramters to a symbol table"
		" with kind = %d\n", curr[NS_IDENT]->kind);
	exit(2);
    }
    const struct Symbol *sym = addSymbol(loc, ident, SC_STACK, type);

    return sym;
}

void
finishParamList(const struct Type *retType)
{
    reopenPreviousTypeScope();
    reopenScope_(NS_IDENT);
    reopenScope_(NS_TYPE);
    if (curr[NS_IDENT]->kind != PARAM_LIST) {
	fprintf(stderr, "internal error: previous scope kind = %d\n",
		curr[NS_IDENT]->kind);
	exit(2);
    }

    size_t offset = 16 + retType->size;;
    for (struct SymtabEntry *e = curr[NS_IDENT]->first; e; e = e->next) {
	offset = roundUp(offset, e->symbol.type->align);

	e->symbol.memLoc.offset = offset;
	offset += e->symbol.type->size;
    }
    curr[NS_IDENT]->offset0 = offset;
    curr[NS_IDENT]->offset = 0;

    // closeScope(PARAM_LIST);
    closeTypeScope();
    closeScope_(NS_IDENT);
    closeScope_(NS_TYPE);
}

const struct Type **
getParamArray()
{
    if (curr[NS_IDENT]->kind != PARAM_LIST) {
	fprintf(stderr, "internal error: getParamArray from symbol table"
		" with kind = %d\n", curr[NS_IDENT]->kind);
	exit(2);
    }
    size_t numParam = curr[NS_IDENT]->len;
    const struct Type **param = allocParamArray(numParam);

    const struct SymtabEntry *e = curr[NS_IDENT]->first;
    for (size_t i = 0; i < numParam; ++i, e = e->next) {
	param[i] = e->symbol.type;
    }
    return param;
}

void
beginMemberDef(struct Symbol *taggedType)
{
    enum ScopeKind kind;

    switch (taggedType->type->kind) {
	case TK_UNION:
	    kind = UNION_FIELD_LIST;
	    break;
	case TK_STRUCT:
	    kind = STRUCT_FIELD_LIST;
	    break;
	default:
	    fprintf(stderr, "internal error: 'beginMemberDef'\n");
	    exit(2);
    }

    openScope(kind);
    taggedType->u.members = curr[NS_IDENT];
}

void
endMemberDef(struct Symbol *taggedType)
{
    enum ScopeKind kind;

    switch (taggedType->type->kind) {
	case TK_UNION:
	    kind = UNION_FIELD_LIST;
	    break;
	case TK_STRUCT:
	    kind = STRUCT_FIELD_LIST;
	    break;
	default:
	    fprintf(stderr, "internal error: 'beginMemberDef'\n");
	    exit(2);
    }

    size_t align = curr[NS_IDENT]->maxAlign;
    size_t size = roundUp(curr[NS_IDENT]->offset, align);
    taggedType->type = completeTaggedType(taggedType, size, align);
    closeScope(kind);
}

const struct Symbol *
getMemberSymbol(const struct Symbol *taggedType, const char *member)
{
    return findInSymtab(member, taggedType->u.members);
}

static void
printIndent(size_t indent)
{
    for (size_t i = 0; i < indent; ++i) {
	printf(" ");
    }
}

static void
printSymtab(const struct Symtab *symtab, size_t indent)
{
    if (! symtab) {
	return;
    }
    for (const struct SymtabEntry *e = symtab->first; e; e = e->next) {
	printIndent(indent);
	printf("%10s: ", e->symbol.ident);
	printType(e->symbol.type);
	if (e->symbol.memLoc.name) {
	    printf(", by name '%s'", e->symbol.memLoc.name);
	} else {
	    printf(", offset: %10td", e->symbol.memLoc.offset);
	}
	printf("\n");
	if (e->symbol.u.members) {
	    printIndent(indent + 4);
	    printf("fields:\n");
	    printSymtab(e->symbol.u.members, indent+4);
	}
    }
}

void
createGlobalSymtab()
{
    bool first = true;

    for (const struct SymtabEntry *e = global[NS_IDENT].first; e; e = e->next) {
	if (e->symbol.type->kind == FUNCTION) {
	    continue;
	}
	if (e->symbol.storageClass == SC_EXTERN) {
	    continue;
	}
	if (e->symbol.storageClass == SC_TYPEDEF) {
	    continue;
	}
	if (e->symbol.storageClass == SC_LITERAL) {
	    continue;
	}
	if (e->symbol.init) {
	    continue;
	}
	if (first) {
	    first = false;
	    printDirective(BSS);
	}
	if (e->symbol.storageClass != SC_STATIC) {
	    printDirective(GLOBL, e->symbol.memLoc.name);
	}
	printDirective(ALIGN, e->symbol.type->align);
	printLabel(e->symbol.memLoc.name);
	printDirective(SPACE, e->symbol.type->size);
    }
}

void
printCurrentSymtab()
{
    printf("types in current symtab:\n");
    printSymtab(curr[NS_TYPE], 0);
    printf("identifiers in current symtab:\n");
    for (const struct Symtab *s = curr[NS_IDENT]; s; s = s->up) {
	printSymtab(s, 0);
	if (s->up) {
	    printf("--\n");
	}
    }
    printf("symtab->offset0: %td\n", curr[NS_IDENT]->offset0);
    printf("symtab->offset: %td\n", curr[NS_IDENT]->offset);
    printf("symtab->minOffset: %td\n", curr[NS_IDENT]->minOffset);
}
