#ifndef ULMCC_INSTR_ULM_H
#define ULMCC_INSTR_ULM_H 1

#include <stddef.h>

#include "symtab.h"
#include "expr.h"

typedef size_t Reg;

extern Reg ZERO;
extern Reg FP;
extern Reg SP;
extern Reg RET;

void setOut(FILE *out_);

void releaseReg(Reg id);
size_t requiredRegSaveSpace();
void saveUsedRegs(size_t offset);
void restoreUsedRegs(size_t offset);

enum Directive {
    D_BYTE = 1,
    D_WORD = 2,
    D_LONG = 4,
    D_QUAD = 8,
    D_STRING,
    ALIGN,
    BSS,
    DATA,
    GLOBL,
    SPACE,
    TEXT,
};

enum {
    size_int = 2
};

void printDirective(enum Directive directive, ...);
void printComment(const char *fmt, ...);
void printLabel(const char *name);

Reg loadAddress(const char *name, ptrdiff_t offset, Reg *dest);
Reg loadSignedLiteral(size_t size, int64_t imm, Reg *dest);
Reg loadUnsignedLiteral(size_t size, uint64_t imm, Reg *dest);
Reg loadReg(Reg src, Reg *dest);

Reg fetch(size_t numBytes, bool is_signed, ptrdiff_t offset, Reg addr,
	  Reg index, size_t scale, Reg *dest_);
void store(size_t numBytes, Reg src, ptrdiff_t offset, Reg addr, Reg index,
	   size_t scale);

Reg fetchValue(size_t numBytes, bool is_signed, struct MemLocation memLoc,
	       Reg *dest);
void storeValue(Reg src, size_t numBytes, struct MemLocation memLoc);

Reg fetchAddressedValue(size_t numBytes, bool is_signed, Reg addr, Reg *dest);
void storeAddressedValue(Reg src, size_t numBytes, Reg addr);

enum InstrOp {
    ADD_INSTR = EO_ADD,
    SUB_INSTR = EO_SUB,
    MUL_INSTR = EO_MUL,
    BITWISE_NOT_INSTR = EO_BITWISE_NOT,
    BITWISE_OR_INSTR = EO_BITWISE_OR,
    BITWISE_XOR_INSTR = EO_BITWISE_XOR,
    BITWISE_AND_INSTR = EO_BITWISE_AND,
    DIV_SI_INSTR = EO_LAST,
    DIV_UI_INSTR,
    MOD_SI_INSTR,
    MOD_UI_INSTR,
    SHL_UI_INSTR,
    SHL_SI_INSTR,
    SHR_UI_INSTR,
    SHR_SI_INSTR,
    CALL_INSTR,
    JZ_INSTR,
    JNZ_INSTR,
    GE_SI_INSTR,
    GE_UI_INSTR,
    GT_SI_INSTR,
    GT_UI_INSTR,
    LE_SI_INSTR,
    LE_UI_INSTR,
    LT_SI_INSTR,
    LT_UI_INSTR,
};

void callInstr(Reg call);
void returnInstr();
void jmpToLabel(const char *label);
void condJmpToLabel(enum InstrOp op, const char *label);

Reg urrInstr(enum InstrOp op, uint64_t imm0, Reg reg1, Reg *reg2);
Reg srrInstr(enum InstrOp op, int64_t imm0, Reg reg1, Reg *reg2);
Reg rrrInstr(enum InstrOp op, size_t reg0, Reg reg1, Reg *reg2);
Reg rrInstr(enum InstrOp op, size_t reg0, Reg *reg1);

#endif // ULMCC_CODEGEN_H
