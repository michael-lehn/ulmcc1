#ifndef ULMCLIB_STDDEF_H
#define ULMCLIB_STDDEF_H

#include <stdint.h>

typedef uint64_t size_t;
typedef int64_t ptrdiff_t;

#endif // ULMCLIB_STDDEF_H
