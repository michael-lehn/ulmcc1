#ifndef ULMCLIB_TIME_H
#define ULMCLIB_TIME_H

#include <stdint.h>

typedef uint64_t time_t;
//typedef unsigned long long time_t;

time_t time(time_t *arg);
//unsigned long long time(unsigned long long *arg);

#endif // ULMCLIB_TIME_H
