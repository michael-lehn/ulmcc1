#include <stdio.h>

int
main()
{
    int a, b;

    a = -4;
    b = 5;
    printf("%d + %d is %s\n", a, b, a + b ? "not zero" : "zero");

    a = -5;
    b = 5;
    printf("%d + %d is %s\n", a, b, a + b ? "not zero" : "zero");
}
