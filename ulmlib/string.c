#include <string.h>

size_t
strlen(const char *s)
{
    const char *cp;
    for (cp = s; cp && *cp; ++cp) {
    }
    return cp - s;
}

int
strcmp(const char *s1, const char *s2)
{
    for (; *s1 && *s1 == *s2; ++s1, ++s2) {
    }
    return *s1 - *s2;
}

char *
strcpy(char *dst, const char *src)
{
    char *s = dst;
    while (src && (*s++ = *src++)) {
    }
    return dst;
}

char *
strcat(char *s1, const char *s2)
{
    char *s = s1 + strlen(s1);
    strcpy(s, s2);
    return s1;
}
