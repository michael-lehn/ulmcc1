#include <stdlib.h>

static uint64_t current_rand = 0;

void
srand(uint64_t seed)
{
   current_rand = seed;
}

uint64_t
rand()
{
    // https://en.wikipedia.org/wiki/Lehmer_random_number_generator
    current_rand =  2147483647 * current_rand + 16807;
    return current_rand;
}

