#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static struct {
    int ch;
    bool active;
} reuse;

int _getchar();

void
_ulm_unget()
{
    if (reuse.active) {
	printf("libulm: _ulm_unget failed");
	exit(2);
    }
    reuse.active = true;
}

int
getchar()
{
    if (reuse.active) {
	reuse.active = false;
	return reuse.ch;
    }
    return reuse.ch = _getchar();
}
