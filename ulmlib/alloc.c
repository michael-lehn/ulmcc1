#include <stdio.h>
#include <stdlib.h>

struct MemNode
{
    size_t size;
    struct MemNode *next;
};

enum { MEM_SIZE = 16 * 1024 };
static struct MemNode mem[MEM_SIZE] = {
    {0, &mem[1]},
    {sizeof(mem) - 2*sizeof(mem[0]), mem}
};

static struct MemNode *node = mem;
static struct MemNode *root = mem;
static const int ALIGN = sizeof(mem[0]);

/* ermittelt, ob die Speicherflaeche bei ptr belegt ist */
static char
is_allocated(struct MemNode *ptr)
{
    if (ptr == root) {
	return 0;
    } else if (ptr->next > ptr) {
	return 0;
    } else if (ptr->next != root) {
	return 1;
    } else if (root->next > ptr) {
	return 1;
    }
    return root->next == root;
}

static struct MemNode *
successor(struct MemNode *p)
{
    return (struct MemNode *)((char *)(p + 1) + p->size);
}

void *
malloc(size_t size)
{
    if (! size) {
	return 0;
    }
    if (size % ALIGN) {
	size += ALIGN - size % ALIGN;
    }
    struct MemNode *prev = node;
    struct MemNode *ptr = prev->next;
    do {
	if (ptr->size >= size) {
	    break;
	}
	prev = ptr;
	ptr = ptr->next;
    } while (ptr != node);
    if (ptr->size < size) {
	return 0;
    }
    if (ptr->size < size + 2*sizeof(mem[0])) {
	node = ptr->next;
	prev->next = ptr->next;
	for (struct MemNode *p = prev; p < ptr; p = successor(p)) {
	    prev = p;
	}
	ptr->next = prev;
	if (ptr == node) {
	    node = root;
	}
	return ptr + 1;
    }
    node = ptr;
    struct MemNode *newnode = (struct MemNode *)((char*)ptr + ptr->size - size);
    newnode->size = size;
    newnode->next = ptr;
    struct MemNode *next = successor(ptr);
    if (next < mem + MEM_SIZE && next->next == ptr) {
	next->next = newnode;
    }
    ptr->size -= size + sizeof(mem[0]);
    return newnode + 1;
}

static void
join_if_possible(struct MemNode *prev, struct MemNode *ptr)
{
    struct MemNode *p
	= (struct MemNode *)((char *)prev + sizeof(mem[0]) + prev->size);
    if (p != ptr) {
	return;
    }
    if (prev->size == 0) {
	return;
    }
    prev->next = ptr->next;
    prev->size += ptr->size + sizeof(mem[0]);
    struct MemNode *next = successor(ptr);
    if (next < mem + MEM_SIZE && next->next == ptr) {
	next->next = prev;
    }
    if (node == ptr) {
	node = prev;
    }
}

void
free(void *ptr)
{
    if (! ptr) {
	return;
    }
    struct MemNode *node = (struct MemNode *)((char *)ptr - sizeof(mem[0]));

    struct MemNode *prev = node->next;
    while (is_allocated(prev)) {
	prev = prev->next;
    }
    node->next = prev->next;
    prev->next = node;
    join_if_possible(node, node->next);
    join_if_possible(prev, node);
}
