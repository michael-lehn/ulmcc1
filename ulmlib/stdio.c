#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

static void
print_uint64(bool sign, bool leadingZeros, size_t width, uint64_t x)
{
    int d = x % 10;
    x = x / 10;

    if (x) {
	width = width ? width - 1 : 0;
	print_uint64(sign, leadingZeros, width, x);
    } else {
	for (size_t i = 1; i < width; ++i) {
	    putchar(leadingZeros ? '0' : ' ');
	}
	if (sign) {
	    putchar('-');
	}
    }
    putchar(d + '0');
}

static void
print_int64_hex(bool leadingZeros, size_t width, uint64_t x)
{
    int d = x % 16;
    x = x / 16;

    if (x) {
	width = width ? width - 1 : 0;
	print_int64_hex(leadingZeros, width, x);
    } else {
	for (size_t i = 1; i < width; ++i) {
	    putchar(leadingZeros ? '0' : ' ');
	}
    }
    if (d < 10) {
	putchar(d + '0');
    } else {
	putchar(d - 10 + 'a');
    }
}

static void
print_uint64_oct(bool leadingZeros, size_t width, uint64_t x)
{
    int d = x % 8;
    x = x / 8;

    if (x) {
	width = width ? width - 1 : 0;
	print_uint64_oct(leadingZeros, width, x);
    } else {
	for (size_t i = 1; i < width; ++i) {
	    putchar(leadingZeros ? '0' : ' ');
	}
    }
    putchar(d + '0');
}

static void
print_int64(bool leadingZeros, size_t width, int64_t x)
{
    bool sign = false;
    if (x < 0) {
	sign = true;
	x = -x;
	if (width) {
	    --width;
	}
    }
    print_uint64(sign, leadingZeros, width, x);
}

static int
puts_(const char *s)
{
    if (! s) {
	return puts_("(null)");
    }
    while (*s) {
	putchar(*s);
	++s;
    }
    return 1;
}

int
puts(const char *s)
{
    puts_(s);
    putchar('\n');
    return 1;
}

int
vprintf(const char *fmt, const char **arg)
{
    for (size_t p = 0; *fmt; ++fmt) {
	bool leadingZeros = false;
	size_t width = 0;
	bool sign = false;

	if (*fmt != '%') {
	    putchar(*fmt);
	} else {
	    ++fmt;
	    if (*fmt == '%') {
	        putchar(*fmt);
	    } else {
		if (*fmt == '+') {
		    ++fmt;
		    sign = true;
		}
		if (*fmt == '0') {
		    leadingZeros = true;
		    ++fmt;
		}
		while (*fmt >= '0' && *fmt <= '9') {
		    width = 10 * width + *fmt - '0';
		    ++fmt;
		}

		if (*fmt == 's') {
		    puts_(arg[p]);
		} else if (*fmt == 'c') {
		    putchar((uint64_t) arg[p]);
		} else {
		    size_t size = 2;
		    if (*fmt == 'l') {
			++fmt;
			if (*fmt == 'l') {
			    ++fmt;
			    size = 4;
			} else {
			    size = 8;
			}
		    } else if (*fmt == 'z' || *fmt == 't') {
			++fmt;
			size = 8;
		    } else if (*fmt == 'h') {
			++fmt; 
			if (*fmt == 'h') {
			    ++fmt;
			    size = 1;
			} else {
			    size = 2;
			}
		    }

		    if (*fmt == 'd' || *fmt == 'i') {
			const char *ptr = (const char *) &arg[p] + 8 - size;
			int64_t v
			    = size == 1 ? *(int8_t *) ptr
			    : size == 2 ? *(int16_t *) ptr
			    : size == 4 ? *(int32_t *) ptr
					: *(int64_t *) ptr;
			if (sign && v >= 0) {
			    putchar('+');
			}
			print_int64(leadingZeros, width, v);
		    } else if (*fmt == 'u') {
			const char *ptr = (const char *) &arg[p] + 8 - size;
			uint64_t v
			    = size == 1 ? *(uint8_t *) ptr
			    : size == 2 ? *(uint16_t *) ptr
			    : size == 4 ? *(uint32_t *) ptr
					: *(uint64_t *) ptr;
			print_uint64(false, leadingZeros, width, v);
		    } else if (*fmt == 'o') {
			const char *ptr = (const char *) &arg[p] + 8 - size;
			uint64_t v
			    = size == 1 ? *(uint8_t *) ptr
			    : size == 2 ? *(uint16_t *) ptr
			    : size == 4 ? *(uint32_t *) ptr
					: *(uint64_t *) ptr;
			print_uint64_oct(leadingZeros, width, v);
		    } else if (*fmt == 'x') {
			const char *ptr = (const char *) &arg[p] + 8 - size;
			uint64_t v
			    = size == 1 ? *(uint8_t *) ptr
			    : size == 2 ? *(uint16_t *) ptr
			    : size == 4 ? *(uint32_t *) ptr
					: *(uint64_t *) ptr;
			print_int64_hex(leadingZeros, width, v);
		    } else if (*fmt == 'p') {
			uint64_t v = (uint64_t) arg[p];
			print_int64_hex(leadingZeros, width, v);
		    } else {
			printf("\nprintf: unexpected format specifier `%c'\n",
				*fmt);
			continue;
		    }
		}
		++p;
	    }
	}
    }
    return 1;
}

int
printf(const char *fmt, ...)
{
    const char **argp = &fmt + 1;
    return vprintf(fmt, argp);
}
