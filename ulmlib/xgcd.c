#include <stdbool.h>
#include <stdio.h>

int
askForInteger(const char *ident)
{
    int value;
    for (bool first = true, ok = false; !ok; first = false) {
	printf("Enter an integer: %s = ", ident);
	if (! first) {
	    // consume previous user input up-to the newline
	    while (getchar() != '\n');
        }
	ok = scanf("%d", &value);
    }
    return value;
}

int
main()
{
    int a, b;

    a = askForInteger("a");
    b = askForInteger("b");
    printf("You entered: a = %d\n", a);
    printf("You entered: b = %d\n", b);
}
