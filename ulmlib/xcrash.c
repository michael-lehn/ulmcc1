#include <stdbool.h>
#include <stdio.h>

int
main()
{
    int a = 0;

    do {
        printf("Enter an integer: a = ");
    } while (! scanf("%d", &a));
    printf("You entered: a = %d\n", a);
}
