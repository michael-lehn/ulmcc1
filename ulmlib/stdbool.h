#ifndef ULMCLIB_STDBOOL_H
#define ULMCLIB_STDBOOL_H

typedef _Bool bool;

enum { false = 0, true = 1 };

#endif // ULMCLIB_STDBOOL_H
