#ifndef ULMCLIB_STDLIB_H
#define ULMCLIB_STDLIB_H

#include <stddef.h>

int exit(int);
void *malloc(size_t size);
void free(void *ptr);
void srand(uint64_t seed);
uint64_t rand(void);

#endif // ULMCLIB_STDLIB_H
