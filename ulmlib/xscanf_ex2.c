#include <stdbool.h>
#include <stdio.h>

int
main()
{
    int a = 0;

    printf("&a = %p\n", &a);

    do {
        printf("Enter an integer: a = ");
    } while (! scanf("%ld", &a));
    printf("You entered: a = %d\n", a);
}
