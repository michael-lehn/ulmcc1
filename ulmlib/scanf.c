#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

static int ch = ' ';

static void
nextch()
{
    ch = getchar();
}

static void
skip_spaces()
{
    while (isspace(ch)) {
	nextch();
    }
}

static bool
read_uint_oct(size_t size, void *value)
{
    bool found = false;

    uint64_t v = 0;

    for (; ch != EOF; nextch()) {
	found = true;
	if (ch < '0' || ch > '7') {
	    break;
	}
	v *= 8;
	v += ch - '0';
    }
    if (size == 1) {
	* (uint8_t *) value = v;
    } else if (size == 2) {
	* (uint16_t *) value = v;
    } else if (size == 4) {
	* (uint32_t *) value = v;
    } else if (size == 8) {
	* (uint64_t *) value = v;
    }

    return found;
}

static bool
is_hexdigit(int ch)
{
    return ch >= '0' && ch <= '9' || ch >= 'a' && ch <= 'f'
	|| ch >= 'A' && ch <= 'F';
}

static int
hexvalue(int ch)
{
    if (ch >= '0' && ch <= '9') {
	return ch - '0';
    } else if (ch >= 'a' && ch <= 'f') {
	return 10 + ch - 'a';
    } else if (ch >= 'A' && ch <= 'F') {
	return 10 + ch - 'A';
    } else {
	printf("internal error: '%c' is not a hex digit\n", ch);
    }
}

static bool
read_uint_hex(size_t size, void *value)
{
    bool found = false;

    uint64_t v = 0;

    if (ch == '0') {
	nextch();
	found = true;
	if (ch == 'x' || ch == 'X') {
	    nextch();
	}
    }
    for (; ch != EOF && is_hexdigit(ch); nextch()) {
	found = true;
	v *= 16;
	v += hexvalue(ch);
    }
    if (size == 1) {
	* (uint8_t *) value = v;
    } else if (size == 2) {
	* (uint16_t *) value = v;
    } else if (size == 4) {
	* (uint32_t *) value = v;
    } else if (size == 8) {
	* (uint64_t *) value = v;
    }

    return found;
}

static bool
read_uint(bool leadingZeros, size_t size, void *value)
{
    bool found = false;

    uint64_t v = 0;

    for (bool first = true; ch != EOF; nextch()) {
	if (ch < '0' || ch > '9') {
	    break;
	}
	if (first) {
	    first = false;
	    found = true;
	    if (ch == '0' && !leadingZeros) {
		break;
	    }
	}
	v *= 10;
	v += ch - '0';
    }
    if (size == 1) {
	* (uint8_t *) value = v;
    } else if (size == 2) {
	* (uint16_t *) value = v;
    } else if (size == 4) {
	* (uint32_t *) value = v;
    } else if (size == 8) {
	* (uint64_t *) value = v;
    }

    return found;
}

static bool
read_int(bool leadingZeros, size_t size, void *value)
{
    bool neg = ch == '-' ? true : false;

    if (ch == '-') {
	nextch();
    }
    bool found = read_uint(leadingZeros, size, (uint64_t *) value);
    if (found && neg) {
	if (size == 1) {
	    * (uint8_t *) value *= -1;
	} else if (size == 2) {
	    * (uint16_t *) value *= -1;
	} else if (size == 4) {
	    * (uint32_t *) value *= -1;
	} else if (size == 8) {
	    * (uint64_t *) value *= -1;
	}
    }
    return found;
}

void _ulm_unget();

int
vscanf(const char *fmt, const char **arg)
{
    size_t found = 0;

    skip_spaces();
    for (size_t p = 0; *fmt; ++fmt) {
	if (*fmt == ' ') {
	    skip_spaces();
	} else if (*fmt == '%') {
	    ++fmt;
	    if (*fmt == '%') {
		break;
	    }
	    size_t size = 2;
	    if (*fmt == 'l') {
		++fmt;
		if (*fmt == 'l') {
		    ++fmt;
		    size = 8;
		} else {
		    size = 4;
		}
	    } else if (*fmt == 'z' || *fmt == 't') {
		++fmt;
		size = 8;
	    }
	    if (*fmt == 'd') {
		found += read_int(false, size, (int64_t *) arg[p]);
	    } else if (*fmt == 'u') {
		found += read_uint(false, size, (int64_t *) arg[p]);
	    } else if (*fmt == 'o') {
		found += read_uint_oct(size, (int64_t *) arg[p]);
	    } else if (*fmt == 'x') {
		found += read_uint_hex(size, (int64_t *) arg[p]);
	    } else {
		printf("ignoring unsupported '%c' format specifier\n", *fmt);
	    }
	    ++p;
	} else {
	    break;
	}
    }
    if (*fmt) {
	printf("format error: '%c' not supported\n", *fmt);
    }
    if (! found) {
	ch = ' ';
	_ulm_unget();
    }
    return found;
}

int
scanf(const char *fmt, ...)
{
    const char **argp = &fmt + 1;
    return vscanf(fmt, argp);
}
