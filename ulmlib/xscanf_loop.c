#include <stdbool.h>
#include <stdio.h>

int
main()
{
    int a = 0;

    printf("Enter an integer: a = ");
    while (! scanf("%d", &a)) {
	while (getchar() != '\n');
        printf("Enter an integer: a = ");
    }
    printf("You entered: a = %ld\n", a);
}
