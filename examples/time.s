	.equ	 ZERO,	 0
	.equ	 FP,	 1
	.equ	 SP,	 2
	.equ	 RET,	 3


	.text
/*
 * function time
 **/
	.globl 	time
time:
	// function prologue
	movq	 %RET,	 (%SP)
	movq	 %FP,	 8(%SP)
	addq	 0,	 %SP,	 %FP
	subq	 8,	 %SP,	 %SP

	// begin of the function body
	#0 {...
	ldswq	 2,	 %4
	trap	 %4,	 %0,	 %4
	movq	 %4,	 -8(%FP)
	#if (arg)
	movq	 24(%FP),	 %4
	subq	 %0,	 %4,	 %0
	jz	 endif.L1
	#then
	#1 {...
	# *arg = r;
	movq	 -8(%FP),	 %4
	movq	 24(%FP),	 %5
	movq	 %4,	 (%5)
	#1 ...}
endif.L1:
	#endif
	# return r;
	movq	 -8(%FP),	 %4
	movq	 %4,	 16(%FP)
	jmp	 leave.L0
	#0 ...}
	// end of the function body

	// function epilogue
leave.L0:
	addq	 0,	 %FP,	 %SP
	movq	 8(%SP),	 %FP
	movq	 (%SP),	 %RET
	jmp	 %RET,	 %0

