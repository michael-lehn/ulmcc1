#include <stdio.h>
#include <stdlib.h>

#include "memregion.h"

struct Block
{
    size_t size;
    char *avail;
    char *end;
    struct Block *next;
};

static struct Block *regionBase[MEMREGION_];
static struct Block *regionCurr[MEMREGION_];

static struct Block *
makeBlock(size_t size)
{
    struct Block *block = malloc(sizeof(struct Block) + size);

    if (! block) {
	printf("malloc: out of memory.\n");
	exit(2);
    }

    block->size = size;
    block->avail = (char *) block + sizeof(*block);
    block->end = block->avail + size;
    block->next = 0;

    return block;
}

void *
alloc(enum MemRegion memRegion, size_t numBytes)
{
    if (! regionBase[memRegion]) {
	regionBase[memRegion] = makeBlock(16*numBytes);
	regionCurr[memRegion] = regionBase[memRegion];	
    }

    //size_t align = alignof(void *);
    size_t align = sizeof(void *);
    numBytes = (numBytes + align - 1) / align * align;

    if (regionCurr[memRegion]->end < regionCurr[memRegion]->avail + numBytes) {
	if (regionCurr[memRegion]->next) {
	    struct Block *block = regionCurr[memRegion]->next;
	    regionCurr[memRegion] = block;
	    regionCurr[memRegion]->avail = (char *) block + sizeof(*block);
	} else {
	    size_t size = 2 * regionCurr[memRegion]->size;
	    while (size < numBytes) {
	        size *= 2;
	    }
	    regionCurr[memRegion]->next = makeBlock(size);
	    regionCurr[memRegion] = regionCurr[memRegion]->next;
	}
    }
    char *p = regionCurr[memRegion]->avail;
    regionCurr[memRegion]->avail = regionCurr[memRegion]->avail + numBytes;
    return p;
}

void
dealloc(enum MemRegion memRegion)
{
    struct Block *block = regionBase[memRegion];

    regionCurr[memRegion] = block;
    regionCurr[memRegion]->avail = (char *) block + sizeof(*block);
}

void
printMemRegions()
{
    for (size_t reg = 0; reg < MEMREGION_; ++reg) {
	printf("region %zu\n", reg);

	for (struct Block *b = regionBase[reg]; b; b = b->next) {
	    printf("   size: %zu\n", b->size);
	}
    }
}
