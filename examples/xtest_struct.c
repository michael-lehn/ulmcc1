#include <stdio.h>

struct Expr {
    int val;
};

struct Instr
{
    struct Expr expr[5];
    struct Expr x;
};

struct Instr
foo()
{
    struct Instr tmp = {
	{{1}, {2}, {3}, {4}, {5}},
	{6}
    };
    return tmp;
}

struct Instr
bar(struct Instr f)
{
    for (int i = 0; i < 5; ++i) {
	++f.expr[i].val;
    }
    ++f.x.val;
    return f;
}

int i;

int
main()
{
    struct Instr f = foo();

    for (int i = 0; i < 5; ++i) {
	printf("f.expr[%d].val = %d\n", i, f.expr[i].val);
    }
    printf("f.x.val = %d\n", f.x.val);
    printf("\n");

    struct Instr b = bar(f);

    for (int i = 0; i < 5; ++i) {
	printf("f.expr[%d].val = %d\n", i, f.expr[i].val);
    }
    printf("f.x.val = %d\n", f.x.val);
    for (int i = 0; i < 5; ++i) {
	printf("b.expr[%d].val = %d\n", i, b.expr[i].val);
    }
    printf("b.x.val = %d\n", b.x.val);

    //f = i == 42 ? f : b;
    f = f;
    f = b;
}
