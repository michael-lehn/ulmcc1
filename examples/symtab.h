#ifndef ULMAS_SYMTAB_H
#define ULMAS_SYMTAB_H

#include <stdio.h>

#include "expr.h"

struct Entry *addSym(const char *sym, struct Expr *expr);

void globalSym(const char *sym);

void resolveSym(const char *sym, struct Expr *expr);

struct Expr *getSym(const char *sym);

enum ExprType getSymType(const char *sym);

void fixSymtab();

void printSymTab();

#endif // ULMAS_SYMTAB_H
