#ifndef ULMAS_PARSER_H
#define ULMAS_PARSER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

extern size_t errorCount;

bool parse();

#endif // ULMAS_PARSER_H
