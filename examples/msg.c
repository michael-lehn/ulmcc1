#include <stdio.h>


#include "msg.h"

enum Color {
    NORMAL,
    BOLD, // normal bold
    RED,
    BLUE,
    BOLD_RED,
    BOLD_BLUE,
};

static const char *colorStr[] = {
    "\x1B\x5B\x30\x3B\x31\x30\x6D", // "\033[0;10m",
    "\x1B\x5B\x31\x3B\x31\x30\x6D", // "\033[1;10m",
    "\x1B\x5B\x30\x3B\x33\x31\x6D", //"\033[0;31m",
    "\x1B\x5B\x30\x3B\x33\x34\x6D", // "\033[0;34m",
    "\x1B\x5B\x31\x3B\x33\x31\x6D", //"\033[1;31m",
    "\x1B\x5B\x31\x3B\x33\x34\x6D", //"\033[1;34m",
};

static void
setColor(enum Color color)
{
    printf("%s", colorStr[color]);
}

static void
vmsg(const struct TokenLoc *loc, enum Color msgTypeColor, const char *msgType,
     const char *fmt, const char **argp)
{
    if (loc) {
	setColor(BOLD);
	printf("%lu.%lu-%lu.%lu: ", loc->begin.line,
	       loc->begin.col, loc->end.line, loc->end.col);
	setColor(NORMAL);
    }
   
    if (msgType) {
	setColor(msgTypeColor);
	printf("%s: ", msgType);
	setColor(NORMAL);
    }

    if (fmt) {
	vprintf(fmt, argp);
    }
    printf("\n");

    /*
    if (loc) {
	if (currentInputLineNumber() == loc->begin.line) {
	    printf("%s", currentInputLine());
	    for (size_t i = 1; i < loc->begin.col; ++i) {
		printf(" ");
	    }
	    printf("\033[1;31m^\033[0;30m\n");
	} else if (previousInputLineNumber() == loc->begin.line) {
	    printf("%s", previousInputLine());
	    for (size_t i = 1; i < loc->begin.col; ++i) {
		printf(" ");
	    }
	    printf("\033[1;31m^\033[0;30m\n");
	} else {
	    // printf("token = %s\n", tokenKindStr(tokenKind));
	    // printf("token = %s\n", tokenKindStr(tokenKind));
	    // printf("currentInputLineNumber() = %lu\n",
	    //        currentInputLineNumber());
	    // printf("loc->begin.line = %lu\n", loc->begin.line);
	    // printCh();
	}
   }
   */
}

void
errorMsg(const struct TokenLoc *loc, const char *fmt, ...)
{
    const char **argp = &fmt + 1;
    vmsg(loc, BOLD_RED, "\xF0\x9F\x98\xA2 error", fmt, argp);
}

void
warningMsg(const struct TokenLoc *loc, const char *fmt, ...)
{
    const char **argp = &fmt + 1;
    vmsg(loc, BOLD_BLUE, "\xF0\x9F\x98\xAC warning", fmt, argp);
}
