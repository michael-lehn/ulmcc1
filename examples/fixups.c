#include <stdio.h>
#include <stdlib.h>

#include "fixups.h"
#include "instr.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"

struct Entry
{
    struct Entry *next;
    // where to fix
    size_t seg;
    uint64_t addr;
    size_t offset;
    size_t numBytes;

    // how to fix
    enum FixupType fixupType;
    const char *sym;
    bool addDispl;
    uint64_t displ;
};

static const char *segStr[] = {
    "[text]",
    "[data]",
    "[bss]",
};

static struct Entry *fixups;

void
fixupError(const struct Expr *expr)
{
    errorMsg(&expr->loc, "expected relocatable expression");
}

uint64_t
extractValue(size_t seg, const struct Expr *expr, uint64_t addr, size_t offset,
	     size_t numBytes, enum FixupType fixupType)
{
    uint64_t val = evalExpression(expr);

    if (expr->type == ABSOLUTE) {
	return val;
    }

    // expr->type != UNKNOWN means it is TEXT, DATA, or BSS
    if (fixupType == FIXUP_RELATIVE && expr->type == seg) {
	return val;
    }

    if (expr->op == OP_W0) {
	fixupType = FIXUP_W0_;
	expr = expr->u.child[0];
    } else if (expr->op == OP_W1) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W1_;
    } else if (expr->op == OP_W2) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W2_;
    } else if (expr->op == OP_W3) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W3_;
    }

    struct Entry *newEntry = alloc(FIXUP, sizeof(*newEntry));

    newEntry->seg = seg;
    newEntry->addr = addr;
    newEntry->offset = offset;
    newEntry->numBytes = numBytes;
    newEntry->fixupType = fixupType;
    newEntry->displ = val;

    if (expr->type != UNKNOWN) {
	newEntry->sym = segStr[expr->type];
	newEntry->displ -= getSegmentOffset(expr->type);
	newEntry->addDispl = true;
    } else if (expr->op == SYM) {
	newEntry->sym = expr->u.sym;
	newEntry->addDispl = true;
    } else if (expr->op == ADD || expr->op == SUB) {
	newEntry->addDispl = expr->op == ADD;

	const struct Expr *left = expr->u.child[0];
	const struct Expr *right = expr->u.child[1];

	if (right->type == UNKNOWN) {
	    const struct Expr *tmp = left;
	    left = right;
	    right = tmp;
	}

	if (right->type != ABSOLUTE) {
	    fixupError(expr);
	}

	if (left->type == UNKNOWN) {
	    newEntry->sym = left->u.sym;
	} else {
	    newEntry->sym = segStr[left->type];
	    newEntry->displ -= getSegmentOffset(expr->type);
	}

    } else {
	fixupError(expr);
    }

    newEntry->next = fixups;
    fixups = newEntry;
    return val;
}

void
printFixupTab()
{
    if (! fixups) {
	return;
    }

    printf("#FIXUPS\n");
    for (struct Entry *f = fixups; f; f = f->next) {
	if (f->seg == TEXT) {
	    printf("text ");
	} else if (f->seg == DATA) {
	    printf("data ");
	} else {
	    printf("internal error: unexpected segment %zu",
	    f->seg);
	    stop(2);
	}

	printf("0x%016x", f->addr - getSegmentOffset(f->seg));
	printf("%zu ", f->offset);
	printf("%zu ", f->numBytes);

	if (f->fixupType == FIXUP_ABSOLUTE) {
	    printf("absolute ");
	} else if (f->fixupType == FIXUP_RELATIVE) {
	    printf("relative ");
	} else if (f->fixupType == FIXUP_W0_) {
	    printf("w0 ");
	} else if (f->fixupType == FIXUP_W1_) {
	    printf("w1 ");
	} else if (f->fixupType == FIXUP_W2_) {
	    printf("w2 ");
	} else if (f->fixupType == FIXUP_W3_) {
	    printf("w3 ");
	} else {
	    printf("internal error: unexpected fixup type %d",
		   f->fixupType);
	    stop(2);
	}
	printf("%s", f->sym);
	if (f->displ) {
	    if (f->addDispl) {
		printf("+");
	    } else {
		printf("-");
	    }
	    printf("%x", f->displ);
	}
	printf("\n");
    }
}
