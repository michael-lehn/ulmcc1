#ifndef ULMAS_EXPR_H
#define ULMAS_EXPR_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "lexer.h"
#include "memregion.h"

enum ExprOp
{
    // expressions without any child nodes
    VAL,
    SYM,

    // expressions with one child node
    NEG,
    OP_W0,
    OP_W1,
    OP_W2,
    OP_W3,

    // expressions with two child nodes
    ADD,
    SUB,
    MUL,
    DIV,
    MOD,
};

enum ExprType
{
    ET_TEXT = TEXT,
    ET_DATA = DATA,
    ET_BSS = BSS,
    ABSOLUTE,
    UNKNOWN,
};

struct Literal
{
    uint64_t val;
    const char *repr;
};

struct Expr
{
    enum ExprOp op;
    enum ExprType type;
    struct TokenLoc loc;

    union {
	struct Expr *child[2];
	struct Literal lit;
	const char *sym;
    } u;
};

size_t fixExpressionType(struct Expr *expr);
uint64_t evalExpression(const struct Expr *expr);

struct Expr *makeValExpr(const struct TokenLoc *loc, enum ExprType type,
			 uint64_t val, const char *repr);

struct Expr *makeIdentExpr(const struct TokenLoc *loc, const char *ident);

struct Expr *makeLabelExpr(const struct TokenLoc *loc, enum ExprType seg,
			   uint64_t offset);

struct Expr *makeBinaryExpr(enum ExprOp op, struct Expr *left,
			    struct Expr *right);

struct Expr *makeUnaryExpr(const struct TokenLoc *loc, enum ExprOp op,
			   struct Expr *operand);

void printExprTree(const struct Expr *expr);

void printExpr(const struct Expr *expr);

#endif // ULMAS_EXPR_H
