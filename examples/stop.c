#include <stdlib.h>

#include "stop.h"

/*
static FILE *out_;
static const char *outputfile_;

void
deleteOutputOnStop(FILE *out, const char *outputfile)
{
    out_ = out;
    outputfile_ = outputfile;
}
*/

void
stop(int exitCode)
{
    /*
    if (out_) {
	fclose(out_);
    }

    if (outputfile_) {
	remove(outputfile_);
    }
    */
    exit(exitCode);
}
