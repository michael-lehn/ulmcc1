#ifndef ULMAS_MSG_H
#define ULMAS_MSG_H

#include "lexer.h"

void errorMsg(const struct TokenLoc *loc, const char *msg, ...);
void warningMsg(const struct TokenLoc *loc, const char *msg, ...);

#endif // ULMAS_ERROR_H
