#include <stdio.h>

static const char str[] = "hello, world!🍺";

int X[] = {1, 2, 3, 4};

int
main()
{
    puts("hello, world!");
    puts(str);
    printf("Hello and %s, %s\n", "foo", "bar");
    printf("Hello: %%lld = %lld\n", 123);
    printf("Hello and  %s\n", "some string");
    printf("Hello and  %s\n", str);
    printf("Hello and ch %c\n", 'x');
    printf("Hello and ..  %llK%c\n", 'x');

    for (int i = 0; i < sizeof(X)/sizeof(X[0]); ++i) {
	printf("X[%d] = %d\n", i, X[i]);
    }
    for (int i = 0; i < sizeof(X)/sizeof(X[0]); ++i) {
	int ch;
	X[i] = (ch = i, X[i]);
	printf("%d[X] = %d, %d.\n", i, i[X], ch);
    }

    printf("%04d\n", 15);
    printf("%4d\n", 15);
    printf("%14s\n", "foo");
    printf("%02x %02x %02x\n", 15, 7, 2);

    return 0;
}
