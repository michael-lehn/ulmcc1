#include <stdbool.h>
#include <stdint.h>
//#include <stdlib.h>
#include "stdlib.h"
#include <stdio.h>

int
main()
{
    size_t total = 0;
    for (size_t i = 1; ; ++i) {
	void *p = malloc(i);
	if (! p) {
	    printf("out of memory\n");
	    break;
	} else
	    total += i;
	    printf("got %zu bytes at address %u (total %zu)\n", i, p, total);
    }
}
