#include <ctype.h>

int
isspace(int ch)
{
    return ch == ' ' || ch == '\t' || ch == '\n';
}


