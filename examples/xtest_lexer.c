#include <stdio.h>

#include "lexer.h"

int
main()
{
    while ( (tokenKind = getToken()) != EOI) {
	printf("%s", tokenKindStr(tokenKind));
	if (tokenKind != EOL && tokenKind != EMPTY_LABEL) {
	    printf(" '%s'", tokenValue);
	    if (tokenKind == STRING_LITERAL) {
		printf(" processed as '%s'", processedTokenValue);
	    }
	}
	printf(" at %lu.%lu-%lu.%lu\n", tokenLoc.begin.line,
	       tokenLoc.begin.col, tokenLoc.end.line, tokenLoc.end.col);
    }
    printf("%s", tokenKindStr(tokenKind));
    printf(" at %lu.%lu-%lu.%lu\n", tokenLoc.begin.line,
	   tokenLoc.begin.col, tokenLoc.end.line, tokenLoc.end.col);

}
