#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "expr.h"
#include "instr.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"
#include "ustring.h"
#include "symtab.h"

enum {NODE_BUFFER_SIZE = 2*4096 };

enum ErrorCode {
    SCALE_NOT_ALLOWED,
    EXPECTED_ABSOLUT_EXPRESSION,
    TEXT_ALIGNMENT_FAILURE,
    SYMBOL_ALREADY_DEFINED,
};

static const char *errorStr[] = {
    // SCALE_NOT_ALLOWED
    "scale factor in address must be 1, 2, 4 or 8",

    // EXPECTED_ABSOLUT_EXPRESSION
    "error: expected absolute expression",

    // TEXT_ALIGNMENT_FAILURE,
    "can not align instruction within text segment by using nop instructions",

    // SYMBOL_ALREADY_DEFINED
    "symbol already defined.",
};

static void
instrError(const struct TokenLoc *loc, enum ErrorCode errorCode)
{
    errorMsg(loc, errorStr[errorCode]);
    stop(1);
}

void
printInstr(const struct Instr *instr)
{
    // TODO: enum InstrFmt fmt = instr->operand ? instr->operand->fmt : FMT_;
    enum InstrFmt fmt;
    if (instr->operand) {
	fmt = instr->operand->fmt;
    } else {
	fmt = FMT_;
    }

    printf("%s", mnemonicStr(instr->mnemonic));
    if (fmt == FMT_I) {
	printf(" \t");
	printExpr(instr->operand->u.expr[0]);
    } else if (fmt == FMT_R) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[0]);
    } else if (fmt == FMT_RR) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[0]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[1]);
    } else if (fmt == FMT_IR) {
	printf(" \t");
	printExpr(instr->operand->u.expr[0]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[1]);
    } else if (fmt == FMT_IRR) {
	printf(" \t");
	printExpr(instr->operand->u.expr[0]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
    } else if (fmt == FMT_RRR) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[0]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
    } else if (fmt == FMT_IMR_R) {
	printf(" \t");
	printExpr(instr->operand->u.expr[0]);
	printf("(%%");
	printExpr(instr->operand->u.expr[1]);
	printf("), \t%%");
	printExpr(instr->operand->u.expr[4]);
    } else if (fmt == FMT_MRR1_R) {
	printf(" \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf("), \t%%");
	printExpr(instr->operand->u.expr[4]);
    } else if (fmt == FMT_MRR2_R) {
	printf(" \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 2), \t%%");
	printExpr(instr->operand->u.expr[4]);
    } else if (fmt == FMT_MRR4_R) {
	printf(" \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 4), \t%%");
	printExpr(instr->operand->u.expr[4]);
    } else if (fmt == FMT_MRR8_R) {
	printf(" \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 8), \t%%");
	printExpr(instr->operand->u.expr[4]);
    } else if (fmt == FMT_R_IMR) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[4]);
	printf(", \t");
	printExpr(instr->operand->u.expr[0]);
	printf("(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(")");
    } else if (fmt == FMT_R_MRR1) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[4]);
	printf(", \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(")");
    } else if (fmt == FMT_R_MRR2) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[4]);
	printf(", \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 2)");
    } else if (fmt == FMT_R_MRR4) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[4]);
	printf(", \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 4)");
    } else if (fmt == FMT_R_MRR8) {
	printf(" \t%%");
	printExpr(instr->operand->u.expr[4]);
	printf(", \t(%%");
	printExpr(instr->operand->u.expr[1]);
	printf(", \t%%");
	printExpr(instr->operand->u.expr[2]);
	printf(", 8)");
    } else if (fmt == FMT_STRING) {
	printf(" \t%s", instr->operand->u.str[1]);
    } else {
	printf("internal error fmt not handled\n");
    }
}

struct InstrOperand *
make1AddrOperand(enum InstrFmt fmt, struct Expr *expr0)
{
    struct InstrOperand *instrOperand = alloc(TEXT, sizeof(*instrOperand));
    instrOperand->fmt = fmt;
    instrOperand->u.expr[0] = expr0;
    return instrOperand;
}

struct InstrOperand *
make2AddrOperand(enum InstrFmt fmt, struct Expr *expr0,
		 struct Expr *expr1)
{
    struct InstrOperand *instrOperand = alloc(TEXT, sizeof(*instrOperand));
    instrOperand->fmt = fmt;
    instrOperand->u.expr[0] = expr0;
    instrOperand->u.expr[1] = expr1;
    return instrOperand;
}

struct InstrOperand *
make3AddrOperand(enum InstrFmt fmt, struct Expr *expr0,
		 struct Expr *expr1, struct Expr *expr2)
{
    struct InstrOperand *instrOperand = alloc(TEXT, sizeof(*instrOperand));
    instrOperand->fmt = fmt;
    instrOperand->u.expr[0] = expr0;
    instrOperand->u.expr[1] = expr1;
    instrOperand->u.expr[2] = expr2;
    return instrOperand;
}

struct InstrOperand *
make5AddrOperand(enum InstrFmt fmt, struct Expr *expr0,
		 struct Expr *expr1, struct Expr *expr2,
		 struct Expr *expr3, struct Expr *expr4)
{
    struct InstrOperand *instrOperand = alloc(TEXT, sizeof(*instrOperand));
    instrOperand->fmt = fmt;
    instrOperand->u.expr[0] = expr0;
    instrOperand->u.expr[1] = expr1;
    instrOperand->u.expr[2] = expr2;
    instrOperand->u.expr[3] = expr3;
    instrOperand->u.expr[4] = expr4;
    return instrOperand;
}

struct InstrOperand *
fixFetchOperand(struct InstrOperand *operand, struct Expr *dest)
{
    operand->u.expr[4] = dest;
    if (operand->u.expr[0]) {
	operand->fmt = FMT_IMR_R;
	return operand;
    }
 
    /*
    TODO:
    uint64_t scale = operand->u.expr[3]
	? evalExpression(operand->u.expr[3])
	: 1;
	*/

    uint64_t scale = 1;
    if (operand->u.expr[3]) {
	evalExpression(operand->u.expr[3]);
    }

    if (scale == 1) {
	operand->fmt = FMT_MRR1_R;
    } else if (scale == 2) {
	operand->fmt = FMT_MRR2_R;
    } else if (scale == 4) {
	operand->fmt = FMT_MRR4_R;
    } else if (scale == 8) {
	operand->fmt = FMT_MRR8_R;
    } else  {
	instrError(&operand->u.expr[3]->loc, SCALE_NOT_ALLOWED);
	stop(1);
	return 0;
    }
    return operand;
}

struct InstrOperand *
fixStoreOperand(struct InstrOperand *operand, struct Expr *dest)
{
    operand->u.expr[4] = dest;
    if (operand->u.expr[0]) {
	operand->fmt = FMT_R_IMR;
	return operand;
    }

    /* TODO:
    uint64_t scale = operand->u.expr[3]
	? evalExpression(operand->u.expr[3])
	: 1;
    */
    uint64_t scale = 1;
    if (operand->u.expr[3]) {
	evalExpression(operand->u.expr[3]);
    }

    if (scale == 1) {
	operand->fmt = FMT_R_MRR1;
    } else if (scale == 2) {
	operand->fmt = FMT_R_MRR2;
    } else if (scale == 4) {
	operand->fmt = FMT_R_MRR4;
    } else if (scale == 8) {
	operand->fmt = FMT_R_MRR8;
    } else {
	instrError(&operand->u.expr[3]->loc, SCALE_NOT_ALLOWED);
	stop(1);
	return 0;
    }
    return operand;
}

static enum MemRegion seg = TEXT;
struct Instr *segment[3];
size_t segmentAlignment[3] = {4, 1, 1};
static struct Instr *segmentLast[3];
static size_t currAddrInSeg[3];

void
assembleText()
{
    seg = TEXT;
}

void
assembleData()
{
    seg = DATA;
}

void
assembleBss()
{
    seg = BSS;
}

static size_t
gcd(size_t a, size_t b)
{
    while (b != 0) {
	size_t r = a %b;
	a = b;
	b = r;
    }
    return a;
}

static size_t
roundUp(size_t a, size_t m)
{
    return (a + m - 1) / m * m;
}


void
setAlignment(size_t align)
{
    if (segmentAlignment[seg] % align) {
	segmentAlignment[seg] *= align / gcd(segmentAlignment[seg], align);
    }
    if (currAddrInSeg[seg] % align == 0) {
	return;
    }
    if (seg == TEXT) {
	if (currAddrInSeg[TEXT] % 4 || align % 4) {
	    instrError(0, TEXT_ALIGNMENT_FAILURE);
	}
	while (currAddrInSeg[TEXT] % align) {
	    makeInstr(NOP, 0);
	}
    } else {
	size_t newAddr = roundUp(currAddrInSeg[seg], align);
	struct Expr *expr = makeValExpr(0, ABSOLUTE,
					newAddr - currAddrInSeg[seg],
					"[added for padding]");
	makeInstr(DOT_SPACE, make1AddrOperand(FMT_I, expr));
    }
}

size_t
getSegmentAlignment(size_t seg)
{
    return segmentAlignment[seg];
}

size_t
getSegmentSize(size_t seg)
{
    return currAddrInSeg[seg];
}

size_t
getSegmentOffset(size_t seg)
{
    if (seg == TEXT) {
	return 0;
    } else if (seg == DATA) {
	return roundUp(getSegmentSize(TEXT), getSegmentAlignment(DATA));
    } else if (seg == BSS) {
	return roundUp(getSegmentOffset(DATA) + getSegmentSize(DATA),
		       getSegmentAlignment(BSS));
    }
    printf("internal error: getSegmentOffset called with seg = %lu\n",
	   seg);
    stop(2);
    return 0;
}

bool
setLabel(const struct TokenLoc *loc, const char *ident)
{
    const struct Expr *found = getSym(ident);
    
    // TODO:
    if (found){
	if (found->type != UNKNOWN) {
	    instrError(loc, SYMBOL_ALREADY_DEFINED);
	    stop(1);
	}
    }
    resolveSym(ident, makeValExpr(loc, seg, getSegmentSize(seg), ident));
    return false;
}

static struct Instr *
makeInstr_(size_t size, size_t align)
{
    setAlignment(align);
    struct Instr *instr = alloc(seg, sizeof(*instr));
    if (segmentLast[seg]) {
	segmentLast[seg]->next = instr;
    } else {
	segment[seg] = segmentLast[seg] = instr;
    }
    segmentLast[seg] = instr;
    instr->addr = currAddrInSeg[seg];
    instr->size = size;
    instr->next = 0;

    currAddrInSeg[seg] += size;
    return instr;
}

struct Instr *
makeInstr(enum TokenKind mnemonic, struct InstrOperand *operand)
{
    uint64_t size, align;

    if (mnemonic >= ADDQ && mnemonic <= TRAP) {
	size = 4;
	align = 4;
    } else if (mnemonic == DOT_BYTE) {
	size = 1;
	align = 1;
    } else if (mnemonic == DOT_WORD) {
	size = 2;
	align = 2;
    } else if (mnemonic == DOT_LONG) {
	size = 4;
	align = 4;
    } else if (mnemonic == DOT_QUAD) {
	size = 8;
	align = 8;
    } else if (mnemonic == DOT_SPACE) {
	size = evalExpression(operand->u.expr[0]);
	align = 1;
    } else {
	printf("internal error: size/alignment for %s not handled",
	       tokenKindStr(mnemonic));
	stop(2);
    }

    struct Instr *instr = makeInstr_(size, align);
    instr->mnemonic = mnemonic;
    instr->operand = operand;
    return instr;
}

struct Instr *
makeStringPseudoOp(const char *processedStr, const char *str)
{
    struct InstrOperand *instrOperand = alloc(seg, sizeof(*instrOperand));
    instrOperand->fmt = FMT_STRING;
    instrOperand->u.str[0] = processedStr;
    instrOperand->u.str[1] = str;

    struct Instr *instr = makeInstr_(getStringLen(processedStr)+1, 1);
    instr->mnemonic = DOT_STRING;
    instr->operand = instrOperand;
    return instr;
}
