#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int
main()
{
    time_t t;
    t = time(0);

    printf("time = %u\n", t);
    srand(t);
    for (int i = 0; i < 10; ++i) {
	printf("rand = %u\n", rand() % 100);
    }
}
