#include <stdio.h>

#include "memregion.h"
#include "symtab.h"

struct Entry
{
    struct Entry *next;
    const char *sym;
    struct Expr *expr;
    size_t useCount;
    bool global;
};

static struct Entry *symTab;
static size_t countEntriesWithExpr;

static struct Entry *
findEntry(const char *sym)
{
    struct Entry *l = symTab;
    for (; l; l = l->next) {
	if (l->sym == sym) {
	    return l;
	}
    }
    return 0;
}

struct Entry *
addSym(const char *sym, struct Expr *expr)
{
    struct Entry *found = findEntry(sym);

    if (found) {
	//TODO:
	if (found->useCount == 0) {
	    if (found->expr) {
		*found->expr = *expr;
	    } else {
		found->expr = expr;
	    }
	} else {
	    found->expr = expr;
	}
	return found;
    }

    struct Entry *newEntry = alloc(SYMTAB, sizeof(*newEntry));
    newEntry->next = symTab;
    newEntry->sym = sym;
    newEntry->expr = expr;
    newEntry->useCount = 0;
    newEntry->global = false;
    symTab = newEntry;
    if (expr) {
	++countEntriesWithExpr;
    }
    return symTab;
}

void
globalSym(const char *sym)
{
    struct Entry *found = findEntry(sym);

    if (! found) {
	found = addSym(sym, 0);
    }
    found->global = true;
}

void
resolveSym(const char *sym, struct Expr *expr)
{
    struct Entry *found = findEntry(sym);

    if (found && found->expr) {
	*found->expr = *expr;
	return;
    }
    addSym(sym, expr);
}

struct Expr *
getSym(const char *sym)
{
    struct Entry *found = findEntry(sym);
    if (found) {
	return found->expr;
    }
    return 0;
}

enum ExprType
getSymType(const char *sym)
{
    struct Entry *found = findEntry(sym);
    if (found && found->expr) {
	return found->expr->type;
    }
    return UNKNOWN;
}

void
fixSymtab()
{
    size_t fixed;
    do {
	fixed = 0;
	for (struct Entry *l = symTab; l; l = l->next) {
	    fixed += fixExpressionType(l->expr);
	}
    } while (fixed);
}

void
printSymTab()
{
    if (! countEntriesWithExpr) {
	return;
    }

    printf("#SYMTAB\n");
    for (struct Entry *l = symTab; l; l = l->next) {
	if (! l->expr) {
	    continue;
	}
	if (l->expr->type == TEXT) {
	    l->global ? printf("T") : printf("t");
	} else if (l->expr->type == DATA) {
	    l->global ? printf("D") : printf("d");
	} else if (l->expr->type == BSS) {
	    l->global ? printf("B") : printf("b");
	} else if (l->expr->type == ABSOLUTE) {
	    l->global ? printf("A") : printf("a");
	} else if (l->expr->type == UNKNOWN) {
	    printf("U");
	} else {
	    printf("internal error in symtab.\n");
	}

	uint64_t val = evalExpression(l->expr);
	
	printf(" %27s 0x%016llx", l->sym, val);
	printf("\n");
    }
}
