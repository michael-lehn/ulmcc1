#include <stdbool.h>
#include <stdio.h>
#include <string.h>

const char *list[] = {
    "Welt",
    "Hallo",
    "AC/DC",
    "Michael Lehn",
    "Sandra Lehn",
    "Hello",
};

void
bubblesort(size_t n, void *array_, size_t elemSize,
	   int cmp(const void *, const void *))
{
    char *array = (char *) array_;
    int i = 3;
    // int x = strcmp(array, array);
}
    /*
void
bubblesort(size_t n, char *array_, size_t elemSize,
	   int cmp(const void *, const void *))
{
    char *array = (char *) array_;
    int i = 3;
    int x = cmp(array_, array);
    printf("%s, %s, x = %d", array_, array_, x);
    char *array = (char *) array_;
    bool swapped;
    do {
	swapped = false;
	for (size_t i = 1; i < n; ++i) {
	    //x = cmp(&array[(i - 1) * elemSize], &array[i * elemSize]);
	    //x = cmp(&array[(i - 1) * elemSize], &array[i * elemSize]);
	    if (cmp(&array[(i - 1) * elemSize], &array[i * elemSize]) > 0) {
		for (size_t j = 0; j < elemSize; ++j) {
		    char tmp = array[(i - 1) * elemSize + j];
		    array[(i - 1) * elemSize + j] = array[i * elemSize + j];
		    array[i * elemSize + j] = tmp;
		}
	    }
	}
    } while (swapped);
}
    */

int
mycmp(const void *s1_, const void *s2_)
{
    const char *s1 = (const char *)s1_;
    const char *s2 = (const char *)s2_;

    printf("cmp '%s', '%s':\n", s1, s2);
    return 1; // strcmp(s1, s2);
}

int
main()
{
    printf("some unsorted list of strings:\n");
    for (size_t i = 0; i < sizeof(list)/sizeof(list[0]); ++i) {
	printf("%zu: %s\n", i, list[i]);
    }

    size_t n = sizeof(list)/sizeof(list[0]);
    size_t elemSize = sizeof(list[0]);

    printf("%s\n", list[0]);

    bubblesort(n, list, elemSize, mycmp);
    //bubblesort(sizeof(list)/sizeof(list[0]), list, sizeof(list[0]), mycmp);

    printf("sorted:\n");
    for (size_t i = 0; i < sizeof(list)/sizeof(list[0]); ++i) {
	printf("%zu: %s\n", i, list[i]);
    }
}
