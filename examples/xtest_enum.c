enum Foo {
    A,
    B = 3,
    C
};

enum Foo foo(enum Foo foo);

enum Foo foo(enum Foo foo)
{
}

int
main()
{
    int x = 4;
    enum Foo foo;
    foo = x;
    x = foo * 2;
    x = foo < 4;
    return foo;
}
