#include "lexer.h"
#include "msg.h"

int
main()
{
    struct TokenLoc loc = {{1,3}, {1,8}};

    errorMsg(&loc, "mnemonic expected");
    warningMsg(&loc, "might overflow");
}
