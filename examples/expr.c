#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "expr.h"
#include "instr.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"
#include "symtab.h"

static void
combineError(enum ExprOp op, const struct Expr *left,
	     const struct Expr *right)
{
    printf("can not combine '");
    printExpr(left);
    printf("' and '");
    printExpr(right);
    printf("' with op=%di\n", op);
    stop(1);
}

static enum ExprType
combineExprTypes(enum ExprOp op, const struct Expr *left,
		 const struct Expr *right)
{
    enum ExprType t1 = left->type;
    enum ExprType t2 = right->type;

    if (t2 == ABSOLUTE) {
	enum ExprType tmp = t1;
	t1 = t2;
	t2 = tmp;
    }

    if (op == ADD || op == SUB) {
	if (t1 == ABSOLUTE && t2 == ABSOLUTE) {
	    return ABSOLUTE;
	} else if (t1 == ABSOLUTE && t2 != UNKNOWN) {
	    return t2;
	} else if (t1 != UNKNOWN && t1 == t2) {
	    return ABSOLUTE;
	}
	return UNKNOWN;
    } else if (op == MUL || op == DIV || op == MOD) {
	if (t1 == ABSOLUTE && t2 == ABSOLUTE) {
	    return ABSOLUTE;
	} else if (t1 == ABSOLUTE && t2 == UNKNOWN) {
	    return UNKNOWN;
	}
	combineError(op, left, right);
	stop(1);
	return UNKNOWN;
    } else {
	combineError(op, left, right);
	//printf("internal error. Function 'combineExprTypes' called for op = %d\n", op);
	stop(2);
	return UNKNOWN;
    }
}

struct Expr *
makeValExpr(const struct TokenLoc *loc, enum ExprType type, uint64_t val,
	    const char *repr)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    struct TokenLoc loc_ = {{ 0, 0}, {0, 0}};
    expr->op = VAL;
    expr->type = type;
    //TODO: expr->loc = loc ? *loc : loc_;
    if (loc) {
	expr->loc = *(struct TokenLoc *)loc;
    } else {
	expr->loc = loc_;
    }
    expr->u.lit.val = val;
    expr->u.lit.repr = repr;
    return expr;
}

struct Expr *
makeIdentExpr(const struct TokenLoc *loc, const char *ident)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));

    expr->op = SYM;
    expr->loc = *loc;
    expr->type = getSymType(ident);

    expr->u.sym = ident;
    return expr;
}

struct Expr *
makeBinaryExpr(enum ExprOp op, struct Expr *left,
	       struct Expr *right)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));
    expr->op = op;
    expr->type = combineExprTypes(op, left, right);
    expr->loc.begin = left->loc.begin;
    expr->loc.end = right->loc.end;
    expr->u.child[0] = left;
    expr->u.child[1] = right;
    return expr;
}

struct Expr *
makeUnaryExpr(const struct TokenLoc *loc, enum ExprOp op,
	      struct Expr *operand)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));
    expr->loc.begin = loc->begin;
    expr->loc.end = operand->loc.end;
    expr->op = op;
    expr->type = operand->type;
    expr->u.child[0] = operand;
    expr->u.child[1] = 0;
    return expr;
}


static const char *opStr[] = {
    "[val]",
    "[sym]",
    "[unary] -",
    "@w0",
    "@w1",
    "@w2",
    "@w3",
    "+",
    "-",
    "*",
    "/",
    "%",
};

static void
printExprWithIndent(const struct Expr *expr, size_t indent)
{
    for (size_t i=0; i<indent; ++i) {
	printf(" ");
    }
    printf("%s ", opStr[expr->op]);
    indent += 4;
    if (expr->op == VAL) {
	printf("%s\n", expr->u.lit.repr);
    } else if (expr->op == NEG || (expr->op >= OP_W0 && expr->op <= OP_W3)) {
	printf("\n");
	printExprWithIndent(expr->u.child[0], indent);
    } else {
	printf("\n");
	printExprWithIndent(expr->u.child[0], indent);
	printExprWithIndent(expr->u.child[1], indent);
    }
}

void
printExprTree(const struct Expr *expr)
{
    printExprWithIndent(expr, 0);
}

static void
printExpr_(const struct Expr *expr, bool isFactor)
{
    if (expr->op == SYM) {
	printf("%s", expr->u.sym);
    } else if (expr->op == VAL) {
	printf("%s", expr->u.lit.repr);
    } else if (expr->op == OP_W0) {
	printf("@w0(");
	printExpr_(expr->u.child[0], true);
	printf(")");
    } else if (expr->op == OP_W1) {
	printf("@w1(");
	printExpr_(expr->u.child[0], true);
	printf(")");
    } else if (expr->op == OP_W2) {
	printf("@w2(");
	printExpr_(expr->u.child[0], true);
	printf(")");
    } else if (expr->op == OP_W3) {
	printf("@w3(");
	printExpr_(expr->u.child[0], true);
	printf(")");
    } else if (expr->op == NEG) {
	printf("-");
	printExpr_(expr->u.child[0], true);
    } else if (expr->op == ADD || expr->op == SUB) {
	if (isFactor) {
	    printf("(");
	}
	printExpr_(expr->u.child[0], false);
	printf(" %s ", opStr[expr->op]);
	printExpr_(expr->u.child[1], false);
	if (isFactor) {
	    printf(")");
	}
    } else {
	printExpr_(expr->u.child[0], true);
	printf(" %s ", opStr[expr->op]);
	printExpr_(expr->u.child[1], true);
    }
}

void
printExpr(const struct Expr *expr)
{
    printExpr_(expr, false);
}

size_t
fixExpressionType(struct Expr *expr)
{
    if (expr->type != UNKNOWN) {
	return 0;
    }

    // fix types
    if (expr->op == SYM) {
	expr->type = getSymType(expr->u.sym);
    } else if (expr->op == NEG || (expr->op >= OP_W0 && expr->op <= OP_W3)) {
	fixExpressionType(expr->u.child[0]);
	expr->type = expr->u.child[0]->type;
    } else if (expr->op != VAL) {
	fixExpressionType(expr->u.child[0]);
	fixExpressionType(expr->u.child[1]);
	expr->type = combineExprTypes(expr->op, expr->u.child[0],
				      expr->u.child[1]);
    }
    if (expr->type == UNKNOWN) {
	return 0;
    }
    return 1;
}

static uint64_t
evalExpr(const struct Expr *expr, const struct Expr *root)
{
    if (root == expr) {
	errorMsg(&root->loc, "reference cycle in expression");
	stop(1);
    }
    if (! root) {
	root = expr;
    }

    uint64_t result = 0;
    if (expr->op == SYM) {
	if (!getSym(expr->u.sym)) {
	    printf("internal error");
	    stop(2);
	}
	result = evalExpr(getSym(expr->u.sym), root);
	if (expr->type < ABSOLUTE) {
	    result += getSegmentOffset(expr->type);
	}
    } else if (expr->op == VAL) {
	result = expr->u.lit.val;
    } else if (expr->op == OP_W0) {
	result = evalExpr(expr->u.child[0], root) %  0xFFFF;
    } else if (expr->op == OP_W1) {
	result = (evalExpr(expr->u.child[0], root) / 0xFFFF) % 0xFFFF;
    } else if (expr->op == OP_W2) {
	result = (evalExpr(expr->u.child[0], root) / 0xFFFFFFFF) % 0xFFFF;
    } else if (expr->op == OP_W3) {
	result = (evalExpr(expr->u.child[0], root) / 0xFFFFFFFFFFFF) % 0xFFFF;
    } else if (expr->op == NEG) {
	result = -evalExpr(expr->u.child[0], root);
    } else if (expr->op == ADD) {
	result = evalExpr(expr->u.child[0], root);
	result += evalExpr(expr->u.child[1], root);
    } else if (expr->op == SUB) {
	result = evalExpr(expr->u.child[0], root);
	result -= evalExpr(expr->u.child[1], root);
    } else if (expr->op == MUL) {
	result = evalExpr(expr->u.child[0], root);
	result *= evalExpr(expr->u.child[1], root);
    } else if (expr->op == DIV) {
	result = evalExpr(expr->u.child[0], root);
	result /= evalExpr(expr->u.child[1], root);
    } else if (expr->op == MOD) {
	result = evalExpr(expr->u.child[0], root);
	result %= evalExpr(expr->u.child[1], root);
    }
    return result;
}

uint64_t
evalExpression(const struct Expr *expr)
{
    return evalExpr(expr, 0);
}
