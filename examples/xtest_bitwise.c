#include <stdio.h>

int
main()
{
    unsigned a = 5;
    unsigned b = 3;
    unsigned c;

    printf("%u | %d = %u\n", a, b, a | b);
    printf("%u & %u = %u\n", a, b, a & b);
    printf("%d ^ %d = %d\n", a, b, a ^ b);
    printf("~%u = %u\n", a, ~a);
    printf("~%u | %u = %u\n", a, b, ~a | b);
    printf("%u << %u = %u\n", a, b, a << b);
    printf("%u >> %u = %u\n", a, b, a >> b);

    printf("signed int: %d >> %d = %d\n", -3, 1, -3 >> 1);

    c = a;
    printf("c = %u, b = %u, c |= b -> c = %u\n", c, b, c |= b);

    c = a;
    printf("c = %u, b = %u, c &= b -> c = %u\n", c, b, c &= b);
}
