#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

bool
read_uint64(uint64_t *v)
{
    uint64_t val = 0;
    int ch = getchar();

    while (ch != EOF && isspace(ch)) {
	ch = getchar();
    }
    if (ch == EOF || ch < '0' || ch > '9') {
	return false;
    }
    while (ch >= '0' && ch <= '9') {
	val = 10 * val + ch - '0';
	ch = getchar();
    }
    *v = val;
    return true;
}

bool
read_uint32(void *v)
{
    uint64_t tmp;
    bool ok = read_uint64(&tmp);
    *(uint32_t *)v = tmp;
    return ok;
}

uint64_t
factorial(uint64_t n)
{
    if (n <= 1) {
	return 1;
    }
    return n * factorial(n - 1);
}

int
main()
{
    uint64_t n = 0;

    while (true) {
	printf("Enter: n = ");
	if (read_uint64(&n)) {
	    break;
	}
	while (getchar() != '\n') {
	}
    }
    printf("%llu! = %llu\n", n, factorial(n));
}
