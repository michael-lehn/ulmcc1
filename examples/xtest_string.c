#include <stdio.h>
#include <string.h>

char buf[100];

void
compare(const char *s1, const char *s2, int cmp(const char*, const char*))
{
    printf("cmp(%s, %s) = %d\n", s1, s2, cmp(s1, s2));
}

int
main()
{
    strcpy(buf, "hello, world!");
    printf("buf = '%s' (len = %zu)\n", buf, strlen(buf));
    strcat(buf, ", Hallo Welt!");
    printf("buf = '%s' (len = %zu)\n", buf, strlen(buf));
    const char *s1 = "Hallo";
    const char *s2 = "Hi";
    const char *s3 = "Boah";
    printf("strcmp(%s, %s) = %d\n", s1, s2, strcmp(s1, s2));
    printf("strcmp(%s, %s) = %d\n", s1, s3, strcmp(s1, s3));
    printf("strcmp(%s, %s) = %d\n", s2, s3, strcmp(s2, s3));
    printf("strcmp(%s, %s) = %d\n", s1, s1, strcmp(s1, s1));

    compare("Hallo", "Hallo", strcmp);
    compare(s1, s1, strcmp);
    compare(s1, s2, strcmp);
    compare(s2, s1, strcmp);
    compare(s1, s3, strcmp);
    compare(s3, s1, strcmp);
}
