#ifndef ULMCLIB_STRING_H
#define ULMCLIB_STRING_H

#include <stddef.h>

size_t strlen(const char *s);
int strcmp(const char *s1, const char *s2);
char *strcpy(char *dst, const char *src);
char *strcat(char *s1, const char *s2);

#endif // ULMCLIB_STRING_H
