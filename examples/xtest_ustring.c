#include <stdio.h>
#include "ustring.h"

int
main()
{
    const char *full = addString("/usr/stdio.h");

    printf("fullpath: %s (%p)\n", full, (void *) full);
    printf("file: %s (%p)\n", filename(full), (void *) filename(full));
    printf("path: %s (%p)\n", path(full), (void *) path(full));
    printf("base: %s (%p)\n", basename(full), (void *) basename(full));
    printf("file: %s (%p)\n", filename(full), (void *) filename(full));
    printf("ext: %s (%p)\n", extension(full), (void *) extension(full));
    printf("path + base + '.c': %s (%p) \n",
	   stringCat(stringCat(path(full), basename(full)), addString(".c")),
	   (void *) stringCat(stringCat(path(full), basename(full)),
			      addString(".c")));
    printf("\n");

    full = addString("/usr/local/include/");
    printf("fullpath: %s (%p)\n", full, (void *) full);
    printf("path: %s (%p)\n", path(full), (void *) path(full));
    printf("base: %s (%p)\n", basename(full), (void *) basename(full));
    printf("file: %s (%p)\n", filename(full), (void *) filename(full));
    printf("ext: %s (%p)\n", extension(full), (void *) extension(full));
    printf("path + base + '.c': %s (%p) \n",
	   stringCat(stringCat(path(full), basename(full)), addString(".c")),
	   (void *) stringCat(stringCat(path(full), basename(full)),
			      addString(".c")));
    printf("\n");

    full = addString("stdio.h");
    printf("fullpath: %s (%p)\n", full, (void *) full);
    printf("path: %s (%p)\n", path(full), (void *) path(full));
    printf("base: %s (%p)\n", basename(full), (void *) basename(full));
    printf("file: %s (%p)\n", filename(full), (void *) filename(full));
    printf("ext: %s (%p)\n", extension(full), (void *) extension(full));
    printf("path + base + '.c': %s (%p) \n",
	   stringCat(stringCat(path(full), basename(full)), addString(".c")),
	   (void *) stringCat(stringCat(path(full), basename(full)),
			      addString(".c")));
    printf("\n");

    full = addString("../stdio.h");
    printf("fullpath: %s (%p)\n", full, (void *) full);
    printf("path: %s (%p)\n", path(full), (void *) path(full));
    printf("base: %s (%p)\n", basename(full), (void *) basename(full));
    printf("file: %s (%p)\n", filename(full), (void *) filename(full));
    printf("ext: %s (%p)\n", extension(full), (void *) extension(full));
    printf("path + base + '.c': %s (%p) \n",
	   stringCat(stringCat(path(full), basename(full)), addString(".c")),
	   (void *) stringCat(stringCat(path(full), basename(full)),
			      addString(".c")));
    printf("\n");

    printStringPool();
}
