#include <stdlib.h>
#include <stdio.h>

#include "codegen.h"
#include "fixups.h"
#include "instr.h"
#include "lexer.h"
#include "parser.h"
#include "string.h"
#include "stop.h"
#include "symtab.h"

int
main()
{
    char *inputfile = 0;
    char *outputfile = 0;

    if (parse()) {
	codegen();
    }
}
