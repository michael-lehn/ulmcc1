#include <stdio.h>

struct Foo
{
    char	ch;
    long long	l;
};

struct Bar
{
    char	ch;
    struct Foo	foo;
};

char ch;
long long l;

void
initBar(struct Bar *bar)
{
    bar->ch = ch;
    bar->foo.ch = ch;
    bar->foo.l = l;
}

int
main()
{
    char a;
    struct Foo foo;
    struct Bar bar;

    foo.ch = ch;
    foo.l = l;

    bar.ch = ch;
    bar.foo.ch = ch;
    bar.foo.l = l;
    initBar(&bar);
}
