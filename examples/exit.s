	.equ	 ZERO,	 0
	.equ	 FP,	 1
	.equ	 SP,	 2
	.equ	 RET,	 3


	.text
/*
 * function exit
 **/
	.globl 	exit
exit:
	// function prologue
	movq	 %RET,	 (%SP)
	movq	 %FP,	 8(%SP)
	addq	 0,	 %SP,	 %FP
	subq	 0,	 %SP,	 %SP

	// begin of the function body
	#0 {...
	movzwq	 18(%FP),   %4
	halt	 %4
	#0 ...}
	// end of the function body

	// function epilogue
leave.L0:
	addq	 0,	 %FP,	 %SP
	movq	 8(%SP),	 %FP
	movq	 (%SP),	 %RET
	jmp	 %RET,	 %0

