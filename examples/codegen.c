#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "expr.h"
#include "fixups.h"
#include "instr.h"
#include "lexer.h"
#include "msg.h"
#include "stop.h"
#include "symtab.h"

static size_t seg_;
static uint64_t addr_;

enum ErrorCode {
    U8_OUT_OF_RANGE,
    S8_OUT_OF_RANGE,
    U16_OUT_OF_RANGE,
    S16_OUT_OF_RANGE,
    S24_REL_JMP_OUT_OF_RANGE,
    EXPECTED_MULT_OF_4,
    EXPECTED_ABS_EXPR,
};

static const char *errorStr[] = {
    // U8_OUT_OF_RANGE,
    "%llu out of range [0, 2^8-1]",

    // S8_OUT_OF_RANGE,
    "%lld out of range [-2^7, 2^7-1]",

    // U16_OUT_OF_RANGE,
    "%llu out of range [0, 2^16-1]",

    // S16_OUT_OF_RANGE,
    "%lld out of range [-2^15, 2^15-1]",

    // S24_REL_JMP_OUT_OF_RANGE,
    "offset '%lld' for relative jump is out of range [-2^25, -2^25-1]",

    // EXPECTED_MULT_OF_4,
    "offset '%lld' for relative jump is not a multiple of 4",

    // EXPECTED_ABS_EXPR
    "absolute expression expected",
};

static void
codegenError(const struct TokenLoc *loc, enum ErrorCode errorCode, uint64_t v)
{
    errorMsg(loc, errorStr[errorCode], v);
    stop(1);
}

enum Key {
    I1 = 0,
    R1 = 0,
    R2 = 1,
    R3 = 2,

    DIS = 0,
    BAS = 1, 
    IDX = 2,
    SRC = 4, 
    DST = 4, 
};

static uint32_t
imm8u(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    uint64_t v = extractValue(seg_, expr, addr_, offset, 1, FIXUP_ABSOLUTE);
    if (v >= 256) {
	codegenError(&instr->operand->u.expr[key]->loc, U8_OUT_OF_RANGE, v);
	stop(1);
    }
    return v % 0x100;
}

static uint32_t
imm8s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 1, FIXUP_ABSOLUTE);
    if (v < -128 || v >= 128) {
	codegenError(&instr->operand->u.expr[key]->loc, S8_OUT_OF_RANGE,
		     (uint64_t) v);
	stop(1);
    }
    return v % 0x100;
}

static uint32_t
imm16u(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    uint64_t v = extractValue(seg_, expr, addr_, offset, 2, FIXUP_ABSOLUTE);
    if (v >= 65536) {
	codegenError(&instr->operand->u.expr[key]->loc, U16_OUT_OF_RANGE, v);
	stop(1);
    }
    return v % 0x10000;
}

static uint32_t
imm16s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 2, FIXUP_ABSOLUTE);
    if (v < -32768 || v >= 32768) {
	codegenError(&instr->operand->u.expr[key]->loc, S16_OUT_OF_RANGE,
		     (uint64_t) v);
	stop(1);
    }
    return v % 0x10000;
}

static uint32_t
relJmp24s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 3, FIXUP_RELATIVE);
    if (expr->type != UNKNOWN && expr->type != ABSOLUTE) {
	v = v - instr->addr;
    }

    if (expr->type != UNKNOWN && v % 4) {
	codegenError(&instr->operand->u.expr[key]->loc, EXPECTED_MULT_OF_4, v);
	stop(1);
    }

    v /= 4;

    if (v < -8388608 || v >= 8388608) {
	codegenError(&instr->operand->u.expr[key]->loc,
		     S24_REL_JMP_OUT_OF_RANGE, (uint64_t) v*4);
	stop(1);
    }
    uint64_t *v_ = &v;
    *v_ = *v_ % 0x1000000;
    return v;
}

static uint32_t
encodeInstr(struct Instr *instr)
{
    enum TokenKind mnem = instr->mnemonic;
    enum InstrFmt fmt = instr->operand ? instr->operand->fmt : FMT_;

    uint32_t code = 0;

    if (fmt == FMT_) {
	if (mnem == NOP) {
		code = 0xFF;
		code *= 16777216;
	}
    } else if (fmt == FMT_I) {
	if (mnem == HALT) {
	    code = 0x09;
	    code = code * 256 + imm8u(1, instr, I1);
	    code *= 65536;
	} else if (mnem == JA || mnem == JNBE) {
	    code = 0x4B;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JAE || mnem == JNB) {
	    code = 0x49;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JNAE || mnem == JB) {
	    code = 0x48;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JNA || mnem == JBE) {
	    code = 0x4A;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JE || mnem == JZ) {
	    code = 0x42;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JG || mnem == JNLE) {
	    code = 0x47;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JGE || mnem == JNL) {
	    code = 0x45;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JL || mnem == JNGE) {
	    code = 0x44;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JLE || mnem == JNG) {
	    code = 0x46;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JMP) {
	    code = 0x41;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == JNE || mnem == JNZ) {
	    code = 0x43;
	    code = code * 16777216 +  relJmp24s(1, instr, I1);
	} else if (mnem == PUTC) {
	    code = 0x69;
	    code = code * 256 + imm8u(1, instr, I1);
	    code *= 65536;
	}
    } else if (fmt == FMT_R) {
	if (mnem == GETC) {
	    code = 0x60;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == HALT) {
	    code = 0x01;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == PUTC) {
	    code = 0x61;
	    code = code * 256 + imm8u(1, instr, I1);
	}
	code *= 65536;
    } else if (fmt == FMT_IR) {
	if (mnem == LDSWQ) {
	    code = 0x57;
	    code = code * 65536 + imm16s(1, instr, I1);
	    code = code * 256 + imm8u(3, instr, R2);
	} else if (mnem == LDZWQ) {
	    code = 0x56;
	    code = code * 65536 + imm16u(1, instr, I1);
	    code = code * 256 + imm8u(3, instr, R2);
	} else if (mnem == SHLDWQ) {
	    code = 0x5D;
	    code = code * 65536 + imm16u(1, instr, I1);
	    code = code * 256 + imm8u(3, instr, R2);
	}
    } else if (fmt == FMT_RR) {
	if (mnem == JMP) {
	    code = 0x40;
	} else if (mnem == NOTQ) {
	    code = 0x5E;
	}
	code = code * 256 + imm8u(1, instr, R1);
	code = code * 256 + imm8u(2, instr, R2);
	code *= 256;
    } else if (fmt == FMT_RRR) {
	if (mnem == ADDQ) {
	    code = 0x30;
	} else if (mnem == ANDQ) {
	    code = 0x51;
	} else if (mnem == DIVQ) {
	    code = 0x33;
	} else if (mnem == IDIVQ) {
	    code = 0x35;
	} else if (mnem == IMULQ) {
	    code = 0x34;
	} else if (mnem == MULQ) {
	    code = 0x32;
	} else if (mnem == ORQ) {
	    code = 0x50;
	} else if (mnem == SALQ || mnem == SHLQ) {
	    code = 0x52;
	} else if (mnem == SARQ) {
	    code = 0x54;
	} else if (mnem == SHRQ) {
	    code = 0x53;
	} else if (mnem == SUBQ) {
	    code = 0x31;
	} else if (mnem == TRAP) {
	    code = 0x02;
	}
	code = code * 256 + imm8u(1, instr, R1);
	code = code * 256 + imm8u(2, instr, R2);
	code = code * 256 + imm8u(3, instr, R3);
    } else if (fmt == FMT_IRR) {
	if (mnem == ADDQ) {
	    code = 0x38;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == DIVQ) {
	    code = 0x3B;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == IDIVQ) {
	    code = 0x3D;
	    code = code * 256 + imm8s(1, instr, I1);
	} else if (mnem == IMULQ) {
	    code = 0x3C;
	    code = code * 256 + imm8s(1, instr, I1);
	} else if (mnem == MULQ) {
	    code = 0x3A;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == SALQ || mnem == SHLQ) {
	    code = 0x5A;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == SARQ) {
	    code = 0x5C;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == SHRQ) {
	    code = 0x5B;
	    code = code * 256 + imm8u(1, instr, I1);
	} else if (mnem == SUBQ) {
	    code = 0x39;
	    code = code * 256 + imm8u(1, instr, I1);
	}
	code = code * 256 + imm8u(2, instr, R2);
	code = code * 256 + imm8u(3, instr, R3);
    } else if (fmt == FMT_IMR_R) {
	if (mnem == MOVSBQ) {
	    code = 0x1F;
	} else if (mnem == MOVZBQ) {
	    code = 0x1B;
	} else if (mnem == MOVSLQ) {
	    code = 0x1D;
	} else if (mnem == MOVZLQ) {
	    code = 0x19;
	} else if (mnem == MOVSWQ) {
	    code = 0x1E;
	} else if (mnem == MOVZWQ) {
	    code = 0x1A;
	} else if (mnem == MOVQ) {
	    code = 0x18;
	}
	code = code * 256 + imm8s(1, instr, DIS);
	code = code * 256 + imm8u(2, instr, BAS);
	code = code * 256 + imm8u(3, instr, DST);
    } else if (fmt == FMT_MRR1_R) {
	if (mnem == MOVSBQ) {
	    code = 0x17;
	} else if (mnem == MOVZBQ) {
	    code = 0x13;
	} else if (mnem == MOVSLQ) {
	    code = 0x15;
	} else if (mnem == MOVZLQ) {
	    code = 0x11;
	} else if (mnem == MOVSWQ) {
	    code = 0x16;
	} else if (mnem == MOVZWQ) {
	    code = 0x12;
	} else if (mnem == MOVQ) {
	    code = 0x10;
	}
	code = code * 256 + imm8u(1, instr, BAS);
	code = code * 256 + imm8u(2, instr, IDX);
	code = code * 256 + imm8u(3, instr, DST);
    } else if (fmt == FMT_MRR2_R) {
	if (mnem == MOVSBQ) {
	    code = 0x87;
	} else if (mnem == MOVZBQ) {
	    code = 0x83;
	} else if (mnem == MOVSLQ) {
	    code = 0x85;
	} else if (mnem == MOVZLQ) {
	    code = 0x81;
	} else if (mnem == MOVSWQ) {
	    code = 0x86;
	} else if (mnem == MOVZWQ) {
	    code = 0x82;
	} else if (mnem == MOVQ) {
	    code = 0x80;
	}
	code = code * 256 + imm8u(1, instr, BAS);
	code = code * 256 + imm8u(2, instr, IDX);
	code = code * 256 + imm8u(3, instr, DST);
    } else if (fmt == FMT_MRR4_R) {
	if (mnem == MOVSBQ) {
	    code = 0xA7;
	} else if (mnem == MOVZBQ) {
	    code = 0xA3;
	} else if (mnem == MOVSLQ) {
	    code = 0xA5;
	} else if (mnem == MOVZLQ) {
	    code = 0xA1;
	} else if (mnem == MOVSWQ) {
	    code = 0xA6;
	} else if (mnem == MOVZWQ) {
	    code = 0xA2;
	} else if (mnem == MOVQ) {
	    code = 0xA0;
	}
	code = code * 256 + imm8u(1, instr, BAS);
	code = code * 256 + imm8u(2, instr, IDX);
	code = code * 256 + imm8u(3, instr, DST);
    } else if (fmt == FMT_MRR8_R) {
	if (mnem == MOVSBQ) {
	    code = 0xC7;
	} else if (mnem == MOVZBQ) {
	    code = 0xC3;
	} else if (mnem == MOVSLQ) {
	    code = 0xC5;
	} else if (mnem == MOVZLQ) {
	    code = 0xC1;
	} else if (mnem == MOVSWQ) {
	    code = 0xC6;
	} else if (mnem == MOVZWQ) {
	    code = 0xC2;
	} else if (mnem == MOVQ) {
	    code = 0xC0;
	}
	code = code * 256 + imm8u(1, instr, BAS);
	code = code * 256 + imm8u(2, instr, IDX);
	code = code * 256 + imm8u(3, instr, DST);
    } else if (fmt == FMT_R_IMR) {
	if (mnem == MOVB) {
	    code = 0x2B;
	} else if (mnem == MOVL) {
	    code = 0x29;
	} else if (mnem == MOVW) {
	    code = 0x2A;
	} else if (mnem == MOVQ) {
	    code = 0x28;
	}
	code = code * 256 + imm8u(1, instr, SRC);
	code = code * 256 + imm8s(2, instr, DIS);
	code = code * 256 + imm8u(3, instr, BAS);
    } else if (fmt == FMT_R_MRR1) {
	if (mnem == MOVB) {
	    code = 0x23;
	} else if (mnem == MOVL) {
	    code = 0x21;
	} else if (mnem == MOVW) {
	    code = 0x22;
	} else if (mnem == MOVQ) {
	    code = 0x20;
	}
	code = code * 256 + imm8u(1, instr, SRC);
	code = code * 256 + imm8u(2, instr, BAS);
	code = code * 256 + imm8u(3, instr, IDX);
    } else if (fmt == FMT_R_MRR2) {
	if (mnem == MOVB) {
	    code = 0x93;
	} else if (mnem == MOVL) {
	    code = 0x91;
	} else if (mnem == MOVW) {
	    code = 0x92;
	} else if (mnem == MOVQ) {
	    code = 0x90;
	}
	code = code * 256 + imm8u(1, instr, SRC);
	code = code * 256 + imm8u(2, instr, BAS);
	code = code * 256 + imm8u(3, instr, IDX);
    } else if (fmt == FMT_R_MRR4) {
	if (mnem == MOVB) {
	    code = 0xB3;
	} else if (mnem == MOVL) {
	    code = 0xB1;
	} else if (mnem == MOVW) {
	    code = 0xB2;
	} else if (mnem == MOVQ) {
	    code = 0xB0;
	}
	code = code * 256 + imm8u(1, instr, SRC);
	code = code * 256 + imm8u(2, instr, BAS);
	code = code * 256 + imm8u(3, instr, IDX);
    } else if (fmt == FMT_R_MRR8) {
	if (mnem == MOVB) {
	    code = 0xD3;
	} else if (mnem == MOVL) {
	    code = 0xD1;
	} else if (mnem == MOVW) {
	    code = 0xD2;
	} else if (mnem == MOVQ) {
	    code = 0xD0;
	}
	code = code * 256 + imm8u(1, instr, SRC);
	code = code * 256 + imm8u(2, instr, BAS);
	code = code * 256 + imm8u(3, instr, IDX);
    } else {
	printf("internal error: format not handled\n");
	printf("mnemonic: %s\n", tokenKindStr(mnem));
	printf("instruction format: %d\n", fmt);
	stop(2);
    }
    if (code / 16777216 == 0) {
	printf("internal error: opcode not handled\n");
	printf("mnemonic: %s\n", tokenKindStr(mnem));
	printf("instruction format: %d\n", fmt);
	stop(2);
    }
    return code;
}


static void
encode(size_t seg, uint64_t addrOffset, struct Instr *instr)
{
    seg_ = seg;
    addr_ = addrOffset + instr->addr;

    printf("0x%016x:", addrOffset + instr->addr);
    if (instr->mnemonic == DOT_STRING) {
	const char *str = instr->operand->u.str[0];
	do {
	    printf(" %02x", *str % 0x100);
	} while (*str++);
    } else if (instr->mnemonic == DOT_SPACE) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	if (expr->type == UNKNOWN) {
	    codegenError(&expr->loc, EXPECTED_ABS_EXPR, 0);
	}
	uint64_t v = evalExpression(expr);
	while (v--) {
	    printf(" 00");
	}
    } else if (instr->mnemonic == DOT_BYTE) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 1, FIXUP_ABSOLUTE);
	printf(" %02x" , v % 0x100);
    } else if (instr->mnemonic == DOT_WORD) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(instr->operand->u.expr[0]);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 2, FIXUP_ABSOLUTE);
	printf(" %04x" , v % 0x10000);
    } else if (instr->mnemonic == DOT_LONG) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 4, FIXUP_ABSOLUTE);
	printf(" %08x", v % 0x100000000);
    } else if (instr->mnemonic == DOT_QUAD) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 8, FIXUP_ABSOLUTE);
	printf(" %016x", v);
    } else {
    	union {
    	    uint32_t all;
    	    unsigned char byte[4];
    	} code;

    	code.all = encodeInstr(instr);
    	printf(" %02x %02x %02x %02x", code.byte[0], code.byte[1],
    	       code.byte[2], code.byte[3]);
    }
    printf(" # \t");
    printInstr(instr);
    printf("\n");
}

static const char *segStr[] = {
    "TEXT",
    "DATA",
    "BSS",
};

void
codegen()
{
    fixSymtab();
    for (size_t seg = 0; seg < 2; ++seg) {
	if (! getSegmentSize(seg)) {
	    continue;
	}
	printf("#%s %lu\n", segStr[seg], getSegmentAlignment(seg));

	struct Instr *instr = segment[seg];

	while (instr) {
	    encode(seg, getSegmentOffset(seg), instr);
	    instr = instr->next;
	}
    }
    if (getSegmentSize(2)) {
	printf("#BSS %lu %lu\n", getSegmentAlignment(2), getSegmentSize(2));
    }
    printSymTab();
    printFixupTab();
}
