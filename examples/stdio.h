#ifndef ULMCLIB_STDIO_H
#define ULMCLIB_STDIO_H

enum { EOF = 255 };

int getchar();
int putchar(char c); 
int puts(const char *s);
int printf(const char *fmt, ...);
int vprintf(const char *fmt, const char **argp);

#endif // ULMCLIB_STDIO_H
