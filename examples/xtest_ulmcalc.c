#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

enum TokenKind
{
    EOI = 0,
    DECIMAL_LITERAL = 1,
    NEWLINE = 2,
    PLUS = 3,
    MINUS = 4,
    ASTERISK = 5,
    SLASH = 6,
    LPAREN = 7,
    RPAREN = 8,
    BAD_TOKEN = 9,
};

const char *tokenStr[] = {
    "EOI",
    "DECIMAL_LITERAL",
    "NEWLINE",
    "PLUS",
    "MINUS",
    "ASTERISK",
    "SLASH",
    "LPAREN",
    "RPAREN",
    "BAD_TOKEN",
};

struct TokenPos
{
    unsigned long long line, col;
};

struct TokenLoc
{
    struct TokenPos begin, end;
};

static enum TokenKind tokenKind;
static char tokenValue[];
static struct TokenLoc tokenLoc;

static enum TokenKind getToken();

enum {
    LEXER_BUFFER_SIZE = 16,
    PARSER_BUFFER_SIZE = 16,
};

static enum TokenKind tokenKind;
static char tokenValue[LEXER_BUFFER_SIZE];
static struct TokenLoc tokenLoc;

static struct TokenPos pos = {1, 1};
static int ch;
static char *cp, *end = tokenValue + LEXER_BUFFER_SIZE;

static void
tokenValue_clear()
{
    cp = tokenValue;
}

static enum TokenKind getToken();
static struct TokenPos getPos();

static void
tokenValue_append()
{
    *cp++ = ch;
    if (cp == end) {
	printf("limit of %d bytes for tokenValue exeeded.\n",
		LEXER_BUFFER_SIZE);
	exit(1);
    }
    *cp = 0;
}
static struct TokenPos getPos();

static void
nextch()
{
    tokenLoc.end = pos;
    ch = getchar();
    if (ch == '\n') {
	pos.col = 1;
	++pos.line;
    } else if (ch == '\t') {
	pos.col += 8 - pos.col % 8;
    } else {
	++pos.col;
    }
}

static bool
is_space(char ch)
{
    return ch == ' ' || ch == '\r' || ch == '\f' || ch == '\v' || ch == '\t';
}

static bool
is_digit(char ch)
{
    return ch >= '0' && ch <= '9';
}

static enum TokenKind
getToken_()
{
    tokenKind = BAD_TOKEN;
    // skip whitespace
    while (is_space(ch) || ch == 0) {
	nextch();
    }

    tokenValue_clear();
    tokenLoc.begin = tokenLoc.end;

    // check for decimal literals
    if (ch == '0') {
	tokenValue_append();
	nextch();
	tokenKind = DECIMAL_LITERAL;
	return tokenKind;
    } else if (is_digit(ch)) {
	do {
	    tokenValue_append();
	    nextch();
	} while (is_digit(ch));
	tokenKind = DECIMAL_LITERAL;
	return tokenKind;
    }

    // check for punctuators
    if (ch == '\n') {
	nextch();
	tokenKind = NEWLINE;
	return tokenKind;
    } else if (ch == '+') {
	nextch();
	tokenKind = PLUS;
	return tokenKind;
    } else if (ch == '-') {
	nextch();
	tokenKind = MINUS;
	return tokenKind;
    } else if (ch == '*') {
	nextch();
	tokenKind = ASTERISK;
	return tokenKind;
    } else if (ch == '/') {
	nextch();
	tokenKind = SLASH;
	return tokenKind;
    } else if (ch == '(') {
	nextch();
	tokenKind = LPAREN;
	return tokenKind;
    } else if (ch == ')') {
	nextch();
	tokenKind = RPAREN;
	return tokenKind;
    }

    // if we get here no legal token was found or end of file was reached
    if (ch != EOF) {
	nextch();
	tokenKind = BAD_TOKEN;
	return tokenKind;
    }
    tokenKind = EOI;
    return tokenKind;
}

static enum TokenKind
getToken();

static enum TokenKind
getToken()
{
    getToken_();
    // printf("getToken: %d\n", tokenKind);
    return tokenKind;
}

static bool error;
static unsigned long long errorCount;
static long long result;

static long long stackBuffer[PARSER_BUFFER_SIZE];
static long long *sp = stackBuffer + PARSER_BUFFER_SIZE;

static void
push(long long v)
{
    if (sp + 1 == stackBuffer) {
        printf("limit of %d bytes for stackBuffer exeeded.\n",
               PARSER_BUFFER_SIZE);
        exit(1);
    }
    *--sp = v;
}

static long long
pop()
{
    if (sp == stackBuffer + PARSER_BUFFER_SIZE) {
        printf("pop: stack is empty\n");
        exit(2);
    }
    return *sp++;
}

static long long
decToLongLong(const char *str)
{
    long long v = 0;
    while (*str) {
        v *= 10;
        v += *str++ - '0';
    }
    return v;
}

static void
stopLexerAfterNextNewline()
{
    // will also stop when Eof is reached
    while (tokenKind != NEWLINE) {
        getToken();
        if (tokenKind == EOI) {
            return;
        }
    }
    getToken();
}

static void
parseError(const char *msg, struct TokenLoc loc)
{
    if (! error) {
        printf("%lu.%lu-%lu.%lu: syntax error: %s\n", loc.begin.line,
               loc.begin.col, loc.end.line, loc.end.col, msg);
        stopLexerAfterNextNewline();
        error = true;
        ++errorCount;
    }
}

static bool parseExpression();
static bool parseTerm();
static bool parseFactor();

static bool
parseExpressionStatement()
{
    result = 0;
    if (parseExpression()) {
        if (tokenKind != NEWLINE) {
            parseError("newline expected", tokenLoc);
            return false;
        }
        getToken();
	result = pop();
        return true;
    } else if (tokenKind == NEWLINE) {
        getToken();
        return true;
    }
    return false;
}

static bool
parseExpression()
{
    if (parseTerm()) {
        if (tokenKind == PLUS || tokenKind == MINUS) {
            long long operand = pop();
            enum TokenKind op;
	    op = tokenKind;
            getToken();
            struct TokenLoc loc = tokenLoc;
            if (! parseExpression()) {
                parseError("expression expected", loc);
                return false;
            }
            if (op == PLUS) {
                operand += pop();
            } else if (op == PLUS) {
                operand -= pop();
            }
            push(operand);
        }
        return true;
    }
    return false;
}

static bool
parseTerm()
{
    if (parseFactor()) {
        if (tokenKind == ASTERISK || tokenKind == SLASH) {
            long long operand = pop();
            enum TokenKind op = tokenKind;
            getToken();
            struct TokenLoc loc = tokenLoc;
            if (! parseTerm()) {
                parseError("term expected", loc);
                return false;
            }
            if (op == ASTERISK) {
                operand *= pop();
            } else if (op == SLASH) {
                operand /= pop();
            }
	    push(operand);
        }
        return true;
    }
    return false;
}

static bool
parseFactor()
{
    if (tokenKind == DECIMAL_LITERAL) {
	push(decToLongLong(tokenValue));
        getToken();
        return true;
    } else if (tokenKind == LPAREN) {
        getToken();
        struct TokenLoc loc = tokenLoc;
        if (! parseExpression()) {
            parseError("expression expected", loc);
            return false;
        }
        if (tokenKind != RPAREN) {
            parseError("')' expected", tokenLoc);
            return false;
        }
        getToken();
        return true;
    }
    return false;
}

int
main()
{
    getToken();
    while (tokenKind != EOI) {
        error = false;
        if (parseExpressionStatement()) {
            printf("= %lld\n", result);
        } else {
	    break;
	}
    }
}
