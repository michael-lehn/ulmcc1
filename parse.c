#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "expr.h"
#include "msg.h"
#include "parse.h"
#include "stmt.h"
#include "string.h"
#include "symtab.h"
#include "type.h"

static bool parseGlobalDeclaration();
static bool parseLocalDeclaration(struct Stmt **stmt);
static bool parseFieldDeclaration();

enum DeclKind {
    GLOBAL_DECL,
    LOCAL_DECL,
    FIELD_DECL,
};

static bool parseDeclaration(enum DeclKind kind, struct Stmt **stmt);

static const struct Type *parseDeclarationSpecifiers(enum StorageClass *sClass);
static const struct Type *parseDeclarator(struct TokenLoc *identLoc,
					  const char **ident,
					  const struct Type *base);
static struct Stmt *parseCompound();

bool
expected(enum TokenKind t)
{
    if (tokenKind != t) {
	errorMsg(&tokenLoc, "expected '%s' got '%s'", keywordStr(t),
		 keywordStr(tokenKind));
	exit(1);
	return false;
    }
    getToken();
    return true;
}

bool
parse(const char *filename, FILE *in)
{
    setLexerIn(filename, in);
    getToken();
    while (tokenKind != EOI) {
	if (! parseGlobalDeclaration()) {
	    return false;
	}
    }
    return true;
}

static bool
parseGlobalDeclaration()
{
    return parseDeclaration(GLOBAL_DECL, 0);
}

static bool
parseLocalDeclaration(struct Stmt **stmt)
{
    return parseDeclaration(LOCAL_DECL, stmt);
}

static bool
parseFieldDeclaration()
{
    return parseDeclaration(FIELD_DECL, 0);
}

static void
fixArrayDim(const struct Type *array_, size_t dim)
{
    struct Type *array = (struct Type *) array_;
    array->p.dim = dim;
    array->size = array->u.ref->size * dim;
}

const struct Type *
parseAbstractDeclaration()
{
    const struct Type *type = parseDeclarationSpecifiers(0);
    if (tokenKind == ASTERISK || tokenKind == LPAREN || tokenKind == LBRACKET) {
	type = parseDeclarator(0, 0, type);
    }
    return type;
}

bool
foundTypeToken()
{
    // TODO: use a lookup table for this
    if (tokenKind == CHAR || tokenKind == SHORT || tokenKind == INT
	|| tokenKind == UNSIGNED || tokenKind == SIGNED || tokenKind == VOID
	|| tokenKind == STRUCT || tokenKind == UNION || tokenKind == ENUM
	|| tokenKind == LONG || tokenKind == CONST)
    {
	return true;
    }
    if (tokenKind == IDENT) {
	const struct Symbol *sym = getSymbol(tokenValue);
	return sym && sym->storageClass == SC_TYPEDEF;
    }
    return false;
}

static bool
parseDeclaration(enum DeclKind kind, struct Stmt **stmt)
{
    bool globalDecl = kind == GLOBAL_DECL;
    bool localDecl = kind == LOCAL_DECL;
    bool fieldDecl = kind == FIELD_DECL;

    enum StorageClass storageClass = fieldDecl ? SC_STACK
				   : globalDecl ? SC_NONE
				   : SC_STACK;
    enum StorageClass *storageClass_ = fieldDecl ? 0 : &storageClass;

    if (fieldDecl  && !foundTypeToken()) {
	errorMsg(&tokenLoc, "unknown type name '%s'", tokenValue);
	exit(1);
	return 0;
    }

    const struct Type *base = parseDeclarationSpecifiers(storageClass_);

    if (! base) {
	if (globalDecl) {
	    errorMsg(&tokenLoc, "unknown type name '%s'", tokenValue);
	    exit(1);
	}

	return false;
    }

    bool first = true;
    while (true) {
	const char *ident = 0;
	struct TokenLoc identLoc;
	const struct Type *type = parseDeclarator(&identLoc, &ident, base);

	if (! type) {
	    return false;
	}
	if (! ident) {
	    if (first && (type->kind == TK_STRUCT || type->kind == TK_UNION)) {
		if (! type->u.tag->ident) {
		    warningMsg(&tokenLoc, "unnamed struct/union does not"
			       " declare anything");
		}
		// found a struct or union forward declaration
		break;
	    } else if (first && type->kind == TK_ENUM) {
		break;
	    }
	    errorMsg(&tokenLoc, "identifier expected");
	    return false;
	}
	const struct Symbol *sym = addSymbol(identLoc, ident, storageClass,
					     type);
	if (! sym) {
	    printf("addSymbol failed\n");
	    exit(1);
	    return false;
	}

	const struct Expr *init = 0;
	if (tokenKind == EQUAL) {
	    if (! globalDecl && !localDecl) {
		errorMsg(&tokenLoc, "initializer only allowed in global or"
			 " local declarations");
		exit(1);
	    }
	    getToken();
	    init = parseInitializerList(type);
	    if (! init) {
		errorMsg(&tokenLoc, "initializer expected");
		exit(1);
	    }
	}

	if (init && type->kind == ARRAY && type->size == 0) {
	    if (init->op == EO_LIST) {
		fixArrayDim(type, init->u.l.len);
	    } else if (init->op == EO_LIT && init->type->kind == ARRAY) {
		fixArrayDim(type, getStringLen(init->u.lit.s.str) + 1);
	    }
	}

	if (init) {
	    init = initSymbol(ident, init);
	    if (localDecl && storageClass != SC_STATIC) {
		struct TokenLoc loc = tokenLoc;
		*stmt = appendStmt(*stmt, makeExprStmt(loc, STMT_ASSIGN, init));
	    } else {
		appendNode(makeVarDef(storageClass != SC_STATIC,
				      sym->memLoc.name, type, init));
	    }
	}

	if (globalDecl && first && tokenKind == LBRACE) {
	    reopenPreviousScope();
	    const struct Stmt *funcBody = parseCompound();
	    size_t stackFrame = closeScope(BLOCK);

	    appendNode(makeFuncDef(storageClass != SC_STATIC,
				   type->u.ref, ident, stackFrame, funcBody));
	    return true;
	}
	if (tokenKind != COMMA) {
	    break;
	}
	getToken();
	first = false;
    }
    expected(SEMICOLON);
    return true;
}

static const struct Type *
parseArrayDim(const struct Type *base)
{
    if (tokenKind != LBRACKET) {
	return base;
    }
    getToken();
    const struct Expr *dimExpr = parseLogicalOrExpression();
    if (dimExpr) {
	if (dimExpr->op != EO_LIT) {
	    errorMsg(&tokenLoc, "dimension has to be a compile-time constant");
	    exit(1);
	    return 0;
	}
	if (dimExpr->type->kind != INTEGER) {
	    errorMsg(&tokenLoc, "dimension has to be an integer constant");
	    exit(1);
	    return 0;
	}
	if (dimExpr->type->u.is_signed && dimExpr->u.lit.int64 <= 0) {
	    errorMsg(&tokenLoc, "dimension is not positive");
	    exit(1);
	    return 0;
	}
    }
    size_t dim = dimExpr ? dimExpr->u.lit.uint64 : 0;
    if (tokenKind != RBRACKET) {
	errorMsg(&tokenLoc, "expected ']'");
	exit(1);
	return 0;
    }
    getToken();
    return makeArrayType(base, dim);
}

static const struct Type *
parseParameterList(const struct Type *base)
{
    if (tokenKind != LPAREN) {
	return base;
    }
    getToken();

    openScope(PARAM_LIST);
    if (tokenKind != RPAREN) {
	bool first = true;
	for (; ; first = false) {
	    struct TokenLoc identLoc = tokenLoc;
	    const char *ident = 0;
	    const struct Type *type = parseDeclarationSpecifiers(0);
	    if (! type && tokenKind == DOT3) {
		if (first) {
		    errorMsg(&tokenLoc, "named argument required before '...'");
		    exit(1);
		}
		getToken();
		addParam(identLoc, ident, makeVoidType());
		break;
	    }
	    if (! type) {
		errorMsg(&tokenLoc, "paramter declaration expected");
		type = 0;
		exit(1);
		break;
	    }

	    type = parseDeclarator(&identLoc, &ident, type);

	    if (type->kind == TK_VOID) {
		break;
	    } else if (type->kind == FUNCTION) {
		type = makePointerType(type);
	    } else if (type->kind == ARRAY) {
		type = makePointerType(type->u.ref);
	    }

	    if (! addParam(identLoc, ident, type)) {
		type = 0;
		break;
	    }

	    if (tokenKind != COMMA) {
		break;
	    }
	    getToken();
	}
    }
    expected(RPAREN);
    base = makeFunctionType(base, getParamArray());
    closeScope(PARAM_LIST);

    return base;
}

static const struct Type *
parseInvertedDeclarator(struct TokenLoc *identLoc, const char **ident,
			const struct Type *base)
{
    const struct Type *type = base;
    switch (tokenKind) {
	case ASTERISK:
	    getToken();
	    while (tokenKind == CONST) {
		getToken();
		type = parseInvertedDeclarator(identLoc, ident, type);
		type = makeConstType(type);
	    }
	    type = parseInvertedDeclarator(identLoc, ident, type);
	    type = makePointerType(type);
	    break;

	case LPAREN:
	    getToken();
	    type = parseInvertedDeclarator(identLoc, ident, type);
	    if (tokenKind != RPAREN) {
		errorMsg(&tokenLoc, "expected ')'");
		return 0;
	    }
	    getToken();
	    break;


	case IDENT:
	    if (! ident) {
		errorMsg(&tokenLoc, "type-id cannot have a name");
		exit(1);
		return 0;
	    }
	    if (*ident) {
		errorMsg(&tokenLoc, "unexpected identifier");
		exit(1);
		return 0;
	    }
	    *identLoc = tokenLoc;
	    *ident = tokenValue;
	    getToken();
	    break;

	default:
	    ;
    }

    while (tokenKind == LBRACKET || tokenKind == LPAREN) {
	if (tokenKind == LBRACKET) {
	    type = parseArrayDim(type);
	} else if (tokenKind == LPAREN) {
	    type = parseParameterList(type);
	}
    }
    return type;
}

static const struct Type *
parseDeclarator(struct TokenLoc *identLoc, const char **ident,
		const struct Type *base)
{
    const struct Type *inverted = parseInvertedDeclarator(identLoc, ident,
							  makeDummyType(base));
    const struct Type *type = base;
    for (; inverted->kind != TK_DUMMY; inverted = inverted->u.ref) {
	switch (inverted->kind) {
	    case CONST:
		type = makeConstType(type);
		break;

	    case POINTER:
		type = makePointerType(type);
		break;

	    case ARRAY:
		type = makeArrayType(type, inverted->p.dim);
		break;

	    case FUNCTION:
		if (type->kind == ARRAY) {
		    errorMsg(&tokenLoc, "declared as function returning an"
			     " array");
		    return 0;
		}
		    
		type = makeFunctionType(type, inverted->p.param);
		break;

	    default:
		;
	}
    }
    if (type->kind == FUNCTION) {
	finishParamList(type->u.ref);
    }
    return type;
}

static void
parseFields()
{
    while (tokenKind != RBRACE) {
	if (! parseFieldDeclaration()) {
	    errorMsg(&tokenLoc, "field decalration expected");
	    exit(1);
	}
    }
}

static const struct Type *
parseStruct(struct TokenLoc loc, enum TokenKind structOrUnion)
{
    if (tokenKind != UNION && tokenKind != STRUCT) {
	fprintf(stderr, "internal error: 'parseStruct' called with current"
		" token '%d' (neither 'struct' %d nor 'union' %d)\n",
		structOrUnion, UNION, STRUCT);
	exit(2);
    }

    const char *ident = 0;
    getToken();
    if (tokenKind == IDENT) {
	ident = tokenValue;
	getToken();
    }
    enum TypeKind kind = (enum TypeKind) structOrUnion;

    if (tokenKind == LBRACE || tokenKind == SEMICOLON) {
	// explicit forward declaration or definition of tagged type
	struct Symbol *symbol = addTaggedType(loc, kind, ident);
	if (! symbol) {
	    return 0;
	}
	if (tokenKind == LBRACE) {
	    // definition of tagged type
	    getToken();
	    beginMemberDef(symbol);
	    parseFields();
	    endMemberDef(symbol);
	    expected(RBRACE);
	} else if (! ident) {
	    // anonymous struct declaration
	    errorMsg(&tokenLoc, "declaration of anonymous struct"
		     " must be a definition");
	    return 0;
	}
	return symbol->type;
    }

    const struct Symbol *symbol = getTaggedType(loc, kind, ident);
    if (symbol) {
	return symbol->type;
    }

    // implicit forward declaration of tagged type
    struct Symbol *newSymbol = addTaggedType(loc, (enum TypeKind) structOrUnion,
					     ident);
    return newSymbol->type;
}

static const struct Type *
parseEnum(struct TokenLoc loc)
{
    expected(ENUM);

    const char *ident = 0;
    if (tokenKind == IDENT) {
	ident = tokenValue;
	getToken();
    }

    if (tokenKind == LBRACE) {
	const struct Symbol *enumSym = addTaggedType(loc, TK_ENUM, ident);
	getToken();
	
	if (tokenKind != IDENT) {
	    errorMsg(&tokenLoc, "empty enum is invalid");
	    exit(1);
	    return 0;
	}
	int64_t value = 0;
	while (tokenKind == IDENT) {
	    const char *ident = addString(tokenValue);
	    const struct Symbol *sym = getSymbol(ident);
	    if (sym) {
		noteMsg(&tokenLoc, "'%s' redeclared as different kind of"
			" symbol", ident);
		errorMsg(&sym->loc, "previous declaration of '%s' was here",
			 ident);
		exit(1);
	    }
	    getToken();
	    if (tokenKind == EQUAL) {
		getToken();
		const struct Expr *init = parseConditionalExpression();
		if (! init->is_const) {
		    errorMsg(&init->loc, "not an integer constant");
		    exit(1);
		    return 0;
		}
		if (init->op != EO_LIT) {
		    fprintf(stderr, "internal error: 'parseEnum' constant"
			    " folding failed\n");
		    exit(2);
		}
		value = init->u.lit.int64;
	    }
	    addSignedLiteral(value, ident);

	    if (tokenKind != COMMA) {
		break;
	    }
	    getToken();
	    ++value;
	}
	expected(RBRACE);
	return enumSym->type;
    }
    if (! ident) {
	expected(LBRACE);
    }

    const struct Symbol *symbol = getTaggedType(loc, TK_ENUM, ident);
    if (symbol) {
	return symbol->type;
    }

    // implicit forward declaration of tagged type
    struct Symbol *newSymbol = addTaggedType(loc, TK_ENUM, ident);
    return newSymbol->type;
}

static const struct Type *
parseDeclarationSpecifiers(enum StorageClass *storageClass)
{
    enum TokenKind const_, size_, size2_, signed_, storage_, type_;
    const struct Type *type = 0;
    const char *typeIdent = 0;

    const_ = size_ = size2_ = signed_ = storage_ = type_ = 0;
    bool first = true;
    for (; true; first = false) {
	enum TokenKind *t = 0;
	switch (tokenKind) {
	    case AUTO:
	    case STATIC:
	    case EXTERN:
	    case TYPEDEF:
		if (! storageClass) {
		    errorMsg(&tokenLoc, "'%s' not allowed here",
			     keywordStr(tokenKind));
		    return 0; 
		} else {
		    t = &storage_;
		}
		break;
	    case IDENT:
		{
		    const struct Symbol *sym = getSymbol(tokenValue);
		    if (sym && sym->storageClass == SC_TYPEDEF && !type_
			&& !signed_ && !size_ )
		    {
			t = &type_;
			type = sym->type;
			typeIdent = tokenValue;
		    } else {
			t = 0;
		    }
		}
		break;

	    case CONST:
		t = &const_;
		break;

	    case ENUM:
		t = &type_;
		type_ = ENUM;
		type = parseEnum(tokenLoc);
		continue;

	    case STRUCT:
	    case UNION:
		t = &type_;
		type_ = STRUCT;
		type = parseStruct(tokenLoc, tokenKind);
		continue;

	    case UNDERSCORE_BOOL:
	    case CHAR:
	    case INT:
	    case VOID:
		t = &type_;
		break;

	    case SHORT:
		t = &size_;
		break;

	    case SIGNED:
	    case UNSIGNED:
		t = &signed_;
		break;

	    case LONG:
		t = size_ == LONG ? &size2_ : &size_;
		break;

	    default:
		t = 0;
	}
	if (! t) {
	    if (first) {
		return 0;
	    }
	    break;
	}
	if (*t) {
	    errorMsg(&tokenLoc, "can not combine '%s' with '%s'",
		     keywordStr(tokenKind),
		     typeIdent ? typeIdent : keywordStr(*t));
	    return 0; 
	}
	*t = tokenKind;
	getToken();
    }
    if (! type_) {
	type_ = INT;
    }

    if ((size_ == SHORT || size_ == LONG) && type_ != INT) {
	errorMsg(&tokenLoc, "can not combine '%s' with '%s'",
		 keywordStr(size_),
		 typeIdent ? typeIdent : keywordStr(type_));
	return 0;
    }
    if (signed_ && type_ != INT && type_ != CHAR) {
	errorMsg(&tokenLoc, "can not combine '%s' with '%s'",
		 keywordStr(signed_),
		 typeIdent ? typeIdent : keywordStr(type_));
	return 0;
    }

    if (type_ == CHAR) {
	type = signed_ == UNSIGNED
	    ? makeUnsignedIntegerType(1)
	    : makeSignedIntegerType(1);
    } else if (type_ == UNDERSCORE_BOOL) {
	type = makeUnsignedIntegerType(1);
    } else if (type_ == VOID) {
	type = makeVoidType();
    } else if (type_ == INT) {
	if (size_ == SHORT || ! size_) {
	    type = signed_ == UNSIGNED
		? makeUnsignedIntegerType(sizeOfShort)
		: makeSignedIntegerType(sizeOfShort);
	} else if (size_ == LONG && ! size2_) {
	    type = signed_ == UNSIGNED
		? makeUnsignedIntegerType(sizeOfLong)
		: makeSignedIntegerType(sizeOfLong);
	} else if (size_ == LONG && size2_) {
	    type = signed_ == UNSIGNED
		? makeUnsignedIntegerType(sizeOfLongLong)
		: makeSignedIntegerType(sizeOfLongLong);
	} else {
	    fprintf(stderr, "internal error unhandled int type\n");
	    exit(2);
	}
    }

    if (const_) {
	type = makeConstType(type);
    }
    if (storageClass && storage_) {
	*storageClass = (enum StorageClass) storage_;
    } else if (storage_) {
	errorMsg(&tokenLoc, "illegal storage specifier");
	return 0;
    }
    return type;
}

static struct Stmt *
parseAssignmentStatement()
{
    const struct Expr *expr = parseExpr();
    if (! expected(SEMICOLON)) {
	return 0;
    }
    struct TokenLoc loc = expr ? expr->loc : tokenLoc;
    return makeExprStmt(loc, STMT_ASSIGN, expr);
}

static struct Stmt *
parseReturnStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(RETURN);
    const struct Expr *expr = parseExpr();
    if (! expected(SEMICOLON)) {
	return 0;
    }
    return makeExprStmt(loc, STMT_RETURN, expr);
}

static struct Stmt *parseStatement();

static struct Stmt *
parseBreakStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(BREAK);
    expected(SEMICOLON);
    return makeStmt(loc, STMT_BREAK);
}

static struct Stmt *
parseContinueStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(CONTINUE);
    expected(SEMICOLON);
    return makeStmt(loc, STMT_CONTINUE);
}

static struct Stmt *
parseIfStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(IF);
    if (! expected(LPAREN)) {
	return 0;
    }
    const struct Expr *cond = parseExpr();
    if (! cond) {
	errorMsg(&tokenLoc, "expression expected");
	return 0;
    }
    if (! expected(RPAREN)) {
	return 0;
    }
    const struct Stmt *thenStmt = parseStatement();
    if (! thenStmt) {
	return 0;
    }
    const struct Stmt *elseStmt = 0;
    if (tokenKind == ELSE) {
	getToken();
	elseStmt = parseStatement();
    }
    return makeIfStmt(loc, cond, thenStmt, elseStmt);
}

static struct Stmt *
parseWhileStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(WHILE);
    if (! expected(LPAREN)) {
	return 0;
    }
    const struct Expr *cond = parseExpr();
    if (! cond) {
	errorMsg(&tokenLoc, "expression expected");
	return 0;
    }
    if (! expected(RPAREN)) {
	return 0;
    }
    const struct Stmt *stmt = parseStatement();
    if (! stmt) {
	errorMsg(&tokenLoc, "statement expected");
	exit(1);
	return 0;
    }
    return makeLoopStmt(loc, STMT_WHILE, cond, stmt);
}

static struct Stmt *
parseForStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(FOR);
    expected(LPAREN);
    openScope(BLOCK);
    struct Stmt *init = makeStmt(loc, STMT_BEGIN);
    struct Stmt *initStmt = init;
    if (! parseLocalDeclaration(&initStmt)) {
	init = makeExprStmt(loc, STMT_ASSIGN, parseExpr());
	expected(SEMICOLON);
    }
    const struct Expr *cond = parseExpr();
    expected(SEMICOLON);
    const struct Expr *incr = parseExpr();
    expected(RPAREN);
    const struct Stmt *stmt = tokenKind == LBRACE
	?  parseCompound()
	:  parseStatement();
    closeScope(BLOCK);
    return makeForLoopStmt(loc, init, cond, incr, stmt);

}

static struct Stmt *
parseDoWhileStatement()
{
    struct TokenLoc loc = tokenLoc;
    expected(DO);
    const struct Stmt *stmt = parseStatement();
    if (! stmt) {
	return 0;
    }
    if (! expected(WHILE)) {
	return 0;
    }
    if (! expected(LPAREN)) {
	return 0;
    }
    const struct Expr *cond = parseExpr();
    if (! cond) {
	errorMsg(&tokenLoc, "expression expected");
	return 0;
    }
    if (! expected(RPAREN)) {
	return 0;
    }
    if (! expected(SEMICOLON)) {
	return 0;
    }
    return makeLoopStmt(loc, STMT_DO, cond, stmt);
}

static struct Stmt *
parseStatement()
{
    struct Stmt *stmt = 0;
    switch (tokenKind) {
	case LBRACE:
	    openScope(BLOCK);
	    stmt = parseCompound();
	    closeScope(BLOCK);
	    break;

	case RETURN:
	    stmt = parseReturnStatement();
	    break;

	case FOR:
	    stmt = parseForStatement();
	    break;

	case IF:
	    stmt = parseIfStatement();
	    break;
	
	case BREAK:
	    stmt = parseBreakStatement();
	    break;

	case CONTINUE:
	    stmt = parseContinueStatement();
	    break;

	case WHILE:
	    stmt = parseWhileStatement();
	    break;

	case DO:
	    stmt = parseDoWhileStatement();
	    break;

	default:
	    stmt = parseAssignmentStatement();
    }
    return stmt;
}

static struct Stmt *
parseCompound()
{
    if (tokenKind != LBRACE) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Stmt *body = makeStmt(loc, STMT_BEGIN);
    struct Stmt *stmt = body;
    getToken();

    while (tokenKind != RBRACE) {
	// parse locale declarations
	if (! parseLocalDeclaration(&stmt)) {
	    stmt = appendStmt(stmt, parseStatement());
	}
    }
    getToken();
    return makeCompoundStmt(loc, body);
} 
