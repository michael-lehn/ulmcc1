#include <stdio.h>

#include "lexer.h"
#include "string.h"

int
main(int argc, char **argv)
{
    if (argc == 2) {
	FILE *in = fopen(argv[1], "r");
	setLexerIn(argv[1], in);
    } else {
	setLexerIn("<stdin>", stdin);
    }
    while ( (tokenKind = getToken()) != EOI) {
	printf("%s ", tokenKindStr(tokenKind));
	printf(" '%s'", tokenValue);
	if (tokenKind == STRING_LITERAL) {
	    printf(" processed as '%s'", processedTokenValue);
	}
	printf(" at %s:%lu.%lu-%lu.%lu\n", tokenLoc.filename,
	       tokenLoc.begin.line, tokenLoc.begin.col, tokenLoc.end.line,
	       tokenLoc.end.col);
    }
    printf("%s", tokenKindStr(tokenKind));
    printf(" at %lu.%lu-%lu.%lu\n", tokenLoc.begin.line,
	   tokenLoc.begin.col, tokenLoc.end.line, tokenLoc.end.col);


    printf("string pool:\n");
    printStringPool();
}
