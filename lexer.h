#ifndef ULMCC_LEXER_H
#define ULMCC_LEXER_H 1

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>

enum TokenKind {
    EOI = 0,
    BAD_TOKEN,

    // literals, identifier
    CHARACTER_LITERAL,
    DECIMAL_LITERAL,
    HEXADECIMAL_LITERAL,
    IDENT,
    OCTAL_LITERAL,
    STRING_LITERAL,

    // punctuators
    AMPERSAND,
    AMPERSAND_EQ,
    AMPERSAND2,
    ARROW,
    ASTERISK,
    ASTERISK_EQ,
    CARET,
    CARET_EQ,
    COLON,
    COMMA,
    DOT,
    DOT3,
    EQUAL,
    EQUAL2,
    EXCLAMATION,
    GE,
    GT,
    GT2,
    GT2_EQ,
    LBRACE,
    LBRACKET,
    LE,
    LPAREN,
    LT,
    LT2,
    LT2_EQ,
    MINUS,
    MINUS2,
    MINUS_EQ,
    NE,
    PERCENT,
    PERCENT_EQ,
    PLUS,
    PLUS2,
    PLUS_EQ,
    QMARK,
    RBRACE,
    RBRACKET,
    RPAREN,
    SEMICOLON,
    SLASH,
    SLASH_EQ,
    TILDE,
    TILDE_EQ,
    VBAR,
    VBAR_EQ,
    VBAR2,

    // keywords
    AUTO,
    BREAK,
    CASE,
    CHAR,
    CONST,
    CONTINUE,
    DEFAULT,
    DO,
    DOUBLE,
    ELSE,
    ENUM,
    EXTERN,
    FLOAT,
    FOR,
    GOTO,
    IF,
    INLINE,
    INT,
    LONG,
    REGISTER,
    RESTRICT,
    RETURN,
    SHORT,
    SIGNED,
    SIZEOF,
    STATIC,
    STRUCT,
    SWITCH,
    TYPEDEF,
    UNION,
    UNSIGNED,
    VOID,
    VOLATILE,
    WHILE,
    UNDERSCORE_BOOL,
    UNDERSCORE_COMPLEX,
    UNDERSCORE_IMAGINARY,
    TOKEN_KIND_LAST,
};

struct TokenPos
{
    size_t line, col;
};

struct TokenLoc
{
    const char *filename;
    struct TokenPos begin, end;
};

extern enum TokenKind tokenKind;
extern struct TokenLoc tokenLoc;
extern const char *tokenValue, *processedTokenValue;
extern union LiteralValue {
    uint64_t uint64;
    int64_t int64;
    struct {
	const char *str;
	const char *escStr;
    } s;
} literalValue;
extern const struct Type *literalType;

bool undefMacro(const char *ident);
bool defineMacro(const char *ident);
bool is_defined(const char *ident);

void setLexerIn(const char *filename, FILE *in);
enum TokenKind getToken();
const char *tokenKindStr(enum TokenKind tokenKind);
const char *keywordStr(enum TokenKind tokenKind);

const char * currentInputLine();
size_t currentInputLineNumber();
const char * previousInputLine();
size_t previousInputLineNumber();

// for debugging
void printCh();

#endif // ULMAS_LEXER_H
