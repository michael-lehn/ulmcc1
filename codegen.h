#ifndef ULMCC_CODEGEN_H
#define ULMCC_CODEGEN_H 1

#include <stddef.h>

#include "expr.h"
#include "stmt.h"
#include "symtab.h"

extern size_t sizeOfShort;
extern size_t sizeOfInt;
extern size_t sizeOfLong;
extern size_t sizeOfLongLong;
extern size_t sizeOfSizeT;

enum NodeKind {
    NODE_BEGIN,
    VARDEF,
    FUNCDEF,
    STRLIT,
};

struct Node {
    enum NodeKind kind;
    bool global;
    const struct Type *type;
    const char *name;
    union {
	struct FuncDef {
	    size_t stackFrame;
	    const struct Stmt *body;
	} f;
	struct VarDef {
	    const struct Expr *init;
	} v;
	struct {
	    const char *str;
	    const char *escStr;
	} s;
    } u;
    struct Node *next;
};

size_t maxAlign(size_t align1, size_t align2);
size_t roundUp(ptrdiff_t a, size_t m);
size_t roundDown(ptrdiff_t a, size_t m);

struct Node *makeNode(enum NodeKind kind);
struct Node *makeFuncDef(bool global, const struct Type *retType,
			 const char *name, size_t stackFrame,
			 const struct Stmt *body);
struct Node *makeVarDef(bool global, const char *name, const struct Type *type,
			const struct Expr *init);
struct Node *makeStrLit(const char *name, const struct Type *type,
			const char *str, const char *escStr);

void appendNode(struct Node *node);
void gencode();

#endif // ULMCC_CODEGEN_H
