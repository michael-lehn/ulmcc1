#ifndef ULMCC_SYMTAB_H
#define ULMCC_SYMTAB_H

#include <stdint.h>

#include "lexer.h"
#include "type.h"

enum StorageClass {
    SC_NONE = 0,
    SC_STACK = AUTO,
    SC_EXTERN = EXTERN,
    SC_STATIC = STATIC,
    SC_TYPEDEF = TYPEDEF,
    SC_LITERAL,
};

struct Stmt;
struct Symtab;

struct Symbol {
    struct TokenLoc loc;
    const char *ident;
    const struct Type *type;
    enum StorageClass storageClass;
    const struct Expr *init;

    union {
	const struct Symtab *members;
	const struct Symbol *enumIdent;
	union LiteralValue value;
    } u;

    struct MemLocation {
	const char *name;
	ptrdiff_t offset;
    } memLoc;
};

enum ScopeKind {
    GLOBAL = 0,
    BLOCK,
    PARAM_LIST,
    STRUCT_FIELD_LIST,
    UNION_FIELD_LIST,
};

struct Symtab {
    enum ScopeKind kind;
    struct Symtab *up;
    size_t len;
    size_t maxAlign;
    ptrdiff_t offset0, offset, minOffset;
    struct SymtabEntry {
	struct SymtabEntry *next;
	struct Symbol symbol;
    } *first, *last;
};


void openScope(enum ScopeKind kind);
void reopenPreviousScope();
size_t closeScope(enum ScopeKind kind);

const struct Symbol *addSignedLiteral(int64_t val, const char *ident);

const struct Symbol *getStringLiteral(const char *str);
const struct Symbol *addStringLiteral(const char *str, const char *escStr);
const struct Symbol *addSymbol(struct TokenLoc loc, const char *ident,
			       enum StorageClass storageClass,
			       const struct Type *type);
const struct Symbol *getSymbol(const char *ident);
const struct Expr *initSymbol(const char *ident, const struct Expr *init);

struct Symbol *addTaggedType(struct TokenLoc loc, enum TypeKind kind,
			     const char *ident);
const struct Symbol *getTaggedType(struct TokenLoc loc, enum TypeKind kind,
				   const char *ident);

bool addParam(struct TokenLoc loc, const char *ident, const struct Type *type);
void finishParamList(const struct Type *retType);
const struct Type **getParamArray();

bool addLocal(struct TokenLoc loc, const char *ident, const struct Type *type);

void beginMemberDef(struct Symbol *taggedType);
void endMemberDef(struct Symbol *taggedType);
const struct Symbol *getMemberSymbol(const struct Symbol *taggedType,
				     const char *member);

void createGlobalSymtab();
void printCurrentSymtab();

#endif // ULMCC_SYMTAB_H
