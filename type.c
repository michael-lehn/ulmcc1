#include <stdarg.h>
#include <stdlib.h>

#include "memregion.h"
#include "msg.h"
#include "symtab.h"
#include "type.h"

// check type identity
ptrdiff_t
cmpType(const struct Type *t1, const struct Type *t2)
{
    if (t2->kind != t1->kind) {
	return t2->kind - t1->kind;
    } else if (t1->kind == INTEGER) {
	if (t2->size != t1->size) {
	    return t2->size - t1->size;
	}
	return t2->u.is_signed - t1->u.is_signed;
    } else if (t1->kind == TK_STRUCT || t1->kind == TK_UNION
	       || t1->kind == TK_ENUM)
    {
	return t2->u.tag - t1->u.tag;
    } else if (t1->kind == ARRAY) {
	if (t2->p.dim - t1->p.dim) {
	    return t2->p.dim - t1->p.dim;
	}
	return cmpType(t1->u.ref, t2->u.ref);
    } else if (t1->kind == TK_CONST) {
	return cmpType(t1->u.ref, t2->u.ref);
    } else if (t1->kind == POINTER) {
	return cmpType(t1->u.ref, t2->u.ref);
    } else if (t1->kind == FUNCTION) {
	ptrdiff_t diff = 0;
	if ((diff = cmpType(t1->u.ref, t2->u.ref))) {
	    return diff;
	}
	size_t i;
	for (i = 0; t1->p.param[i] && t2->p.param[i]; ++i) {
	    if ((diff = cmpType(t1->p.param[i], t2->p.param[i]))) {
		return diff;
	    }
	}
	if (t1->p.param[i]) {
	    return -1;
	}
	if (t2->p.param[i]) {
	    return 1;
	}
	return 0;
    }
    return 0;
}

struct TypeSetEntry {
    struct TypeSetEntry *next;
    struct Type type;
};

struct TypeSet
{
    struct TypeSet *up;
    struct TypeSetEntry *first;
};

static struct TypeSet global, *currentTypeSet = &global, *prevTypeSet;

static struct Type *
findType(const struct Type *type)
{
    struct TypeSet *typeSet = type->tagged ? currentTypeSet : &global;

    for (struct TypeSetEntry *l = typeSet->first; l; l = l->next) {
	if (! cmpType(&l->type, type)) {
	    return &l->type;
	}
    }
    return 0;
}

void
openTypeScope()
{
    struct TypeSet *typeSet = alloc(TYPE, sizeof(*typeSet));
    typeSet->up = currentTypeSet;
    typeSet->first = 0;

    prevTypeSet = 0;
    currentTypeSet = typeSet;
}

void
reopenPreviousTypeScope()
{
    if (! prevTypeSet) {
	fprintf(stderr, "internal error: can not reopen previous type set"
		" scope\n");
	exit(2);
    }
    currentTypeSet = prevTypeSet;
    prevTypeSet = 0;
}

void
closeTypeScope()
{
    prevTypeSet = currentTypeSet;
    currentTypeSet = currentTypeSet->up;
}

static const struct Type *
addNewEntry(struct Type *newType)
{
    struct TypeSet *typeSet = newType->tagged ? currentTypeSet : &global;

    struct TypeSetEntry *newEntry = alloc(TYPE, sizeof(*newEntry));
    newEntry->next = typeSet->first;
    newEntry->type = *newType;
    typeSet->first = newEntry;

    return &newEntry->type;
}

const struct Type *
unqualType(const struct Type *type)
{
    if (! type) {
	return 0;
    }
    while (type->kind == TK_CONST) {
	type = type->u.ref;
    }
    return type;
}

const struct Type *
makeDummyType(const struct Type *type)
{
    struct Type newType;
    newType.kind = TK_DUMMY;
    newType.u.ref = type;
    newType.size = newType.align = 0;
    newType.tagged = false;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeVoidType()
{
    struct Type newType;
    newType.kind = TK_VOID;
    newType.size = 0;
    newType.align = 8;
    newType.tagged = false;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeUnsignedIntegerType(size_t size)
{
    struct Type newType;
    newType.kind = INTEGER;
    newType.u.is_signed = false;
    newType.size = newType.align = size;
    newType.tagged = false;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeSignedIntegerType(size_t size)
{
    struct Type newType;
    newType.kind = INTEGER;
    newType.u.is_signed = true;
    newType.size = newType.align = size;
    newType.tagged = false;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeUnsignedIntegerTypeFromLiteral(uint64_t literal)
{
    if (literal <= 0xFFFFu) {
	return makeUnsignedIntegerType(2);
    } else if (literal <= 0xFFFFFFFFu) {
	return makeUnsignedIntegerType(4);
    }
    return  makeUnsignedIntegerType(8);
}

const struct Type *
makeSignedIntegerTypeFromLiteral(int64_t literal_)
{
    uint64_t literal = literal_;
    if (literal < 0x8FFFu) {
	return makeSignedIntegerType(2);
    } else if (literal < 0x8FFFFFFFu) {
	return makeSignedIntegerType(4);
    }
    return  makeSignedIntegerType(8);
}

const struct Type *
makeConstType(const struct Type *type)
{
    if (type->kind == TK_CONST) {
	return type;
    }

    struct Type newType;
    newType.kind = TK_CONST;
    newType.u.ref = type;
    newType.tagged = type->tagged;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makePointerType(const struct Type *ref)
{
    struct Type newType;
    newType.kind = POINTER;
    newType.u.ref = ref;
    newType.size = newType.align = 8;
    newType.tagged = ref->tagged;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeArrayType(const struct Type *elemType, size_t dim)
{
    struct Type newType;
    newType.kind = ARRAY;
    newType.u.ref = elemType;
    newType.p.dim = dim;
    newType.size = elemType->size * dim;
    newType.align = elemType->align;
    newType.tagged = elemType->tagged;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type **
allocParamArray(size_t len)
{
    const struct Type **param = alloc(TYPE, (len + 1) * sizeof(struct Type *));
    param[len] = 0;
    return param;
}

const struct Type *
makeFunctionType(const struct Type *ret, const struct Type **param)
{
    if (ret->kind == ARRAY) {
	fprintf(stderr, "error: return type of function can not be an array\n");
	return 0;
    }

    bool tagged = ret->tagged;
    for (size_t i = 0; param[i]; ++i) {
	if (param[i]->kind == TK_CONST) {
	    param[i] = param[i]->u.ref;
	}
	tagged = tagged || param[i]->tagged;
    }

    struct Type newType;
    newType.kind = FUNCTION;
    newType.u.ref = ret;
    newType.p.param = param;
    newType.tagged = tagged;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeIncompleteStructType(const struct Symbol *tag)
{
    struct Type newType;
    newType.kind = TK_STRUCT;
    newType.u.tag = tag;
    newType.size = 0;
    newType.align = 1;
    newType.tagged = true;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeIncompleteEnumType(const struct Symbol *tag)
{
    struct Type newType;
    newType.kind = TK_ENUM;
    newType.u.tag = tag;
    newType.size = 0;
    newType.align = 8;
    newType.tagged = true;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
makeIncompleteUnionType(const struct Symbol *tag)
{
    struct Type newType;
    newType.kind = TK_UNION;
    newType.u.tag = tag;
    newType.size = 0;
    newType.align = 1;
    newType.tagged = true;

    const struct Type *found = findType(&newType);
    if (found) {
	return found;
    }
    return addNewEntry(&newType);
}

const struct Type *
completeTaggedType(const struct Symbol *tag, size_t size, size_t align)
{
    struct Type *found = findType(tag->type);
    if (! found) {
	return 0;
    }
    if (found->kind == TK_ENUM) {
	found->p.enumRep = makeSignedIntegerType(size);
	found->size = found->p.enumRep->size;
	found->align = found->p.enumRep->align;
    } else {
	found->size = size;
	found->align = align;
    }
    return found;
}

#ifndef TYPE_BUFFER_SIZE
#define TYPE_BUFFER_SIZE 256
#endif

static char typeStr_[TYPE_BUFFER_SIZE];
static char *typeStrPos = typeStr_;

static void
typeStr_reset()
{
    typeStrPos = typeStr_;
    *typeStrPos = 0;
}

static void
typeStr_append(const char *s)
{
    while (typeStrPos - typeStr_ < TYPE_BUFFER_SIZE) {
	if (! (*typeStrPos++ = *s++)) {
	    --typeStrPos;
	    break;
	}
    }
    if (typeStrPos - typeStr_ >= TYPE_BUFFER_SIZE) {
	fprintf(stderr, "typeStr_append: limit of %d bytes for line buffer"
		" exeeded.\n",
		TYPE_BUFFER_SIZE);
	exit(2);
    }
}

static void
vtypeStr_vappend(const char *fmt, va_list argp)
{
    size_t size = TYPE_BUFFER_SIZE - (typeStrPos - typeStr_);
    typeStrPos += vsnprintf(typeStrPos, size, fmt, argp);
}

static void
typeStr_vappend(const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vtypeStr_vappend(fmt, argp);
    va_end(argp);
}

void
makeTypeStr_(const struct Type *type)
{
    switch (type->kind) {
	case TK_VOID:
	    typeStr_append("void");
	    break;
	case INTEGER:
	    typeStr_vappend("%s%zu_t", type->u.is_signed ? "int" : "uint",
			    type->size*8);
	    break;
	case TK_CONST:
	    typeStr_append("const ");
	    makeTypeStr_(type->u.ref);
	    break;
	case POINTER:
	    typeStr_append("pointer to ");
	    makeTypeStr_(type->u.ref);
	    break;
	case ARRAY:
	    typeStr_vappend("array with dim [%zu] with elements of type ",
			    type->p.dim);
	    makeTypeStr_(type->u.ref);
	    break;
	case FUNCTION:
	    makeTypeStr_(type->u.ref);
	    typeStr_append("(");
	    for (size_t i = 0; type->p.param[i]; ++i) {
		makeTypeStr_(type->p.param[i]);
		if (type->p.param[i + 1]) {
		    typeStr_append(",");
		}
	    }
	    break;
	case TK_STRUCT:
	    typeStr_vappend("struct %s", type->u.tag->ident);
	    break;
	case TK_UNION:
	    typeStr_vappend("union %s", type->u.tag->ident);
	    break;
	case TK_ENUM:
	    typeStr_vappend("enum %s", type->u.tag->ident);
	    break;
	default:
	    fprintf(stderr, "internal error: type %d not handled in"
		    " 'makeTypeStr_'\n", type->kind);
	    exit(2);
    }
}

const char *
typeStr(const struct Type *type)
{
    typeStr_reset();
    makeTypeStr_(type);
    return typeStr_;
}

void
printType(const struct Type *type)
{
    switch (type->kind) {
	case TK_VOID:
	    printf("void");
	    break;
	case TK_DUMMY:
	    printf("<DUMMY for ");
	    printType(type->u.ref);
	    printf(">");
	    break;
	case INTEGER:
	    printf("%s%zu_t", type->u.is_signed ? "int" : "uint", type->size*8);
	    break;
	case TK_CONST:
	    printf("const ");
	    printType(type->u.ref);
	    break;
	case POINTER:
	    printf("<POINTER to ");
	    printType(type->u.ref);
	    printf(">");
	    break;
	case ARRAY:
	    printf("<ARRAY [%zu] OF ", type->p.dim);
	    printType(type->u.ref);
	    printf(">");
	    break;
	case FUNCTION:
	    printf("<FUNCTION ");
	    printType(type->u.ref);
	    printf("(");
	    for (size_t i = 0; type->p.param[i]; ++i) {
		printType(type->p.param[i]);
		printf(" <%p>", (void *)type->p.param[i]);
		if (type->p.param[i + 1]) {
		    printf(", ");
		}
	    }
	    printf(") >");
	    break;
	case TK_STRUCT:
	    printf("<struct %s (%p)>", type->u.tag->ident, (void *) type);
	    break;
	case TK_UNION:
	    printf("<union %s (%p)>", type->u.tag->ident, (void *) type);
	    break;
	case TK_ENUM:
	    printf("<enum %s (%p)>", type->u.tag->ident, (void *) type);
	    break;
	default:
	    fprintf(stderr, "internal error: type %d not handled in"
		    " 'printType'\n", type->kind);
	    exit(2);
    }
}

void
printTypeSet()
{
    size_t count = 0;
    for (struct TypeSetEntry *l = global.first; l; l = l->next) {
	printf("%zu: ", ++count);
	printType(&l->type);
	printf("\n");
    }
}
