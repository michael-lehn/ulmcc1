#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "instr_ulm.h"
#include "memregion.h"
#include "msg.h"
#include "string.h"

static void saveExpr(Reg src, const struct Expr *expr);

static const struct Type *retType;
static const char *leaveLabel, *continueLabel, *breakLabel;
static size_t compoundLevel;

static size_t loadExpr(const struct Expr *expr, Reg *dest_);
static void loadCond(const struct Expr *expr, const char *trueLabel,
		     const char *falseLabel);

Reg
irrInstr(enum InstrOp op, const struct Expr *litExpr, Reg reg1, Reg *reg2)
{
    if (litExpr->type->u.is_signed) {
	return srrInstr(op, litExpr->u.lit.int64, reg1, reg2);
    }
    return urrInstr(op, litExpr->u.lit.uint64, reg1, reg2);
}

static Reg
loadStructAddress(const struct Expr *expr, Reg *dest_)
{
    if (unqualType(expr->type)->kind != TK_STRUCT) {
	fprintf(stderr, "internal error: 'loadStructAddress'\n");
	printf("expr:\n");
	printExprTree(expr);
	printf("with type\n");
	printType(expr->type);
	exit(2);
    }
    if (expr->op == EO_VAR) {
	return loadAddress(expr->u.v.memLoc.name,
			   expr->u.v.memLoc.offset, dest_);
    } else if (expr->op == EO_DEREF) {
	return loadExpr(expr->u.child[0], dest_);
    }
    fprintf(stderr, "internal error: struct is neither var nor reference\n");
    printExprTree(expr);
    exit(2);
    return 0;
}

static void
copyStruct(size_t size, size_t align, Reg src, Reg dst)
{
    size_t offset = 0;
    size_t i;
    for (i = 0; i < size / align * align; i += align) {
	Reg tmp = fetch(align, false, offset, src, 0, 1, 0);
	store(align, tmp, offset, dst, 0, 1);
	if (i + align < size) {
	    offset += align;
	}
	releaseReg(tmp);
    }
    for (; i < size; ++i) {
	Reg tmp = fetch(1, false, offset, src, 0, 1, 0);
	store(1, tmp, offset, dst, 0, 1);
	if (i + 1 < size) {
	    ++offset;
	}
	releaseReg(tmp);
    }
}

static void
passStruct(const struct Expr *structParam, size_t stackOffset)
{
    printComment("\t#beg: passStruct\n");
    size_t size = structParam->type->size;
    size_t align = structParam->type->align;
    Reg src = loadAddress(structParam->u.v.memLoc.name,
			  structParam->u.v.memLoc.offset, 0);

    size_t structParamOffset = 0;
    size_t i;
    for (i = 0; i < size / align * align; i += align) {
	Reg tmp = fetch(align, false, structParamOffset, src, 0, 1, 0);
	store(align, tmp, stackOffset, SP, 0, 1);
	if (i + align < size) {
	    structParamOffset += align;
	    stackOffset += align;
	}
	releaseReg(tmp);
    }
    for (; i < size; ++i) {
	Reg tmp = fetch(1, false, 0, src, 0, 1, 0);
	store(1, tmp, stackOffset, SP, 0, 1);
	if (i + 1 < size) {
	    ++structParamOffset;
	    ++stackOffset;
	}
	releaseReg(tmp);
    }
    releaseReg(src);
    printComment("\t#end: passStruct\n");
}

static void
returnStruct(const struct Expr *structParam)
{
    printComment("\t#beg: returnStruct\n");
    size_t stackOffset = 16;
    size_t size = structParam->type->size;
    size_t align = structParam->type->align;
    Reg src = loadAddress(structParam->u.v.memLoc.name,
			  structParam->u.v.memLoc.offset, 0);

    size_t structParamOffset = 0;
    size_t i;
    for (i = 0; i < size / align * align; i += align) {
	Reg tmp = fetch(align, false, structParamOffset, src, 0, 1, 0);
	store(align, tmp, stackOffset, FP, 0, 1);
	if (i + align < size) {
	    structParamOffset += align;
	    stackOffset += align;
	}
	releaseReg(tmp);
    }
    for (; i < size; ++i) {
	Reg tmp = fetch(1, false, 0, src, 0, 1, 0);
	store(1, tmp, stackOffset, FP, 0, 1);
	if (i + 1 < size) {
	    ++structParamOffset;
	    ++stackOffset;
	}
	releaseReg(tmp);
    }
    releaseReg(src);
    printComment("\t#end: returnStruct\n");
}

static size_t
funcCall(const struct Expr *expr, Reg *dest_)
{
    const struct Type *funcDecl = expr->u.child[0]->type;
    size_t stackFrame = 16 + funcDecl->u.ref->size;
    size_t argPos = stackFrame;
    size_t numArgs = 0;
    const struct ExprListItem *arg = expr->u.child[1]->u.l.first;
    for (size_t i = 0; funcDecl->p.param[i]; ++i, ++numArgs, arg = arg->next) {
	const struct Type *t = funcDecl->p.param[i];
	stackFrame = roundUp(stackFrame, t->align);
	stackFrame += t->size;
	if (t->kind == TK_VOID) {
	    for (; arg; arg = arg->next) {
		t = arg->expr->type;
		if (t->kind == ARRAY) {
		    t = makePointerType(t->u.ref);
		}
		size_t size = arg->expr->type->size < 8 ? 8 : t->size;
		size_t align = arg->expr->type->align < 8 ? 8 : t->align;
		stackFrame = roundUp(stackFrame, align);
		stackFrame += size;
	    }
	    break;
	}
    }
    stackFrame = roundUp(stackFrame, 8);
    size_t regSaveOffset = stackFrame;

    stackFrame += requiredRegSaveSpace();
    urrInstr(SUB_INSTR, stackFrame, SP, &SP);
    saveUsedRegs(regSaveOffset);

    arg = expr->u.child[1]->u.l.first;
    for (size_t i = 0; arg; ++i, arg = arg->next) {
	const struct Type *t = i < numArgs
	    ? funcDecl->p.param[i]
	    : arg->expr->type;

	if (t->kind == ARRAY) {
	    t = makePointerType(t->u.ref);
	}

	size_t size = i >= numArgs && t->size < 8 ? 8 : t->size;
	size_t align = i >= numArgs && t->align < 8 ? 8 : t->align;

	argPos = roundUp(argPos, align);
	if (t->kind != TK_STRUCT) {
	    Reg tmp = loadExpr(arg->expr, 0);
	    store(size, tmp, argPos, SP, 0, 1);
	    releaseReg(tmp);
	} else {
	    passStruct(arg->expr, argPos);
	}
	argPos += size;
    }
    Reg call = loadExpr(expr->u.child[0], 0);
    /*
    Reg call = loadAddress(expr->u.child[0]->u.v.memLoc.name,
			   expr->u.child[0]->u.v.memLoc.offset, 0);
    */
    callInstr(call);
    releaseReg(call);
    restoreUsedRegs(regSaveOffset);
    Reg dest;
    if (funcDecl->u.ref->size) {
	if (funcDecl->u.ref->kind != TK_STRUCT) { 
    	    dest = fetch(expr->type->size, expr->type->u.is_signed, 16, SP,
			 ZERO, 1, dest_);
    	} else {
    	    dest = urrInstr(ADD_INSTR, 16, SP, dest_);
    	}
    } else {
	dest = 0;
    }
    urrInstr(ADD_INSTR, stackFrame, SP, &SP);

    return dest;
}

static void
loadCond(const struct Expr *expr, const char *trueLabel,
	 const char *falseLabel)
{
    if (! expr) {
	return;
    }
    if (trueLabel && falseLabel) {
	fprintf(stderr, "internal error: 'loadCond' trueLabel and falseLabel"
		" are both defined\n");
	exit(2);
    }
    if (!trueLabel && !falseLabel) {
	fprintf(stderr, "internal error: 'loadCond' neither trueLabel nor"
		" falseLabel defined\n");
	exit(2);
    }
    const char *condLabel = trueLabel ? trueLabel : falseLabel;
    enum InstrOp condOp = trueLabel ? JNZ_INSTR : JZ_INSTR;

    switch (expr->op) {
	case EO_LOGICAL_NOT:
	    loadCond(expr->u.child[0], falseLabel, trueLabel);
	    return;

	case EO_LOGICAL_OR:
	    if (falseLabel) {
		const char *t = makeLabel(0);
		loadCond(expr->u.child[0], t, 0);
		loadCond(expr->u.child[1], 0, falseLabel);
		printLabel(t);
	    } else {
		loadCond(expr->u.child[0], trueLabel, 0);
		loadCond(expr->u.child[1], trueLabel, 0);
	    }
	    return;

	case EO_LOGICAL_AND:
	    if (falseLabel) {
		loadCond(expr->u.child[0], 0, falseLabel);
		loadCond(expr->u.child[1], 0, falseLabel);
	    } else {
		const char *f = makeLabel(0);
		loadCond(expr->u.child[0], 0, f);
		loadCond(expr->u.child[1], trueLabel, 0);
		printLabel(f);
	    }
	    return;

	case EO_GE:
	case EO_GT:
	case EO_LE:
	case EO_LT:
	case EO_LOGICAL_NOT_EQUAL:
	case EO_LOGICAL_EQUAL:
	    {
		Reg left = loadExpr(expr->u.child[0], 0);
		if (expr->u.child[1]->op == EO_LIT) {
		    irrInstr(SUB_INSTR, expr->u.child[1], left, &ZERO);
		} else {
		    Reg right = loadExpr(expr->u.child[1], 0);
		    rrrInstr(SUB_INSTR, right, left, &ZERO);
		    releaseReg(right);
		}
		releaseReg(left);
		switch (expr->op) {
		    case EO_LOGICAL_EQUAL:
			condOp =  trueLabel ? JZ_INSTR : JNZ_INSTR;
			break;
		    case EO_LOGICAL_NOT_EQUAL:
			condOp = trueLabel ? JNZ_INSTR : JZ_INSTR;
			break;
		    case EO_GE:
			condOp = expr->type->u.is_signed
			    ? trueLabel ? GE_SI_INSTR : LT_SI_INSTR
			    : trueLabel ? GE_UI_INSTR : LT_UI_INSTR;
			break;
		    case EO_GT:
			condOp = expr->type->u.is_signed
			    ? trueLabel ? GT_SI_INSTR : LE_SI_INSTR
			    : trueLabel ? GT_UI_INSTR : LE_UI_INSTR;
			break;
		    case EO_LE:
			condOp = expr->type->u.is_signed
			    ? trueLabel ? LE_SI_INSTR : GT_SI_INSTR
			    : trueLabel ? LE_UI_INSTR : GT_UI_INSTR;
			break;
		    case EO_LT:
			condOp = expr->type->u.is_signed
			    ? trueLabel ? LT_SI_INSTR : GE_SI_INSTR
			    : trueLabel ? LT_UI_INSTR : GE_UI_INSTR;
			break;
		    default:
			fprintf(stderr, "internal error: 'loadExpr'\n");
			exit(2);
		}
		condJmpToLabel(condOp, condLabel);
	    }
	    return;

	case EO_POSTFIX_INCR:
	case EO_POSTFIX_DECR:
	case EO_ASSIGN:
	case EO_MOD:
	case EO_DIV:
	case EO_ADD:
	case EO_SUB:
	case EO_MUL:
	case EO_UNARY_MINUS:
	case EO_LIT:
	case EO_ADDR:
	case EO_DEREF:
	case EO_VAR:
	case EO_CALL:
	case EO_LIST:
	case EO_COND:
	    { 
		Reg dest = loadExpr(expr, 0);
		rrrInstr(SUB_INSTR, ZERO, dest, &ZERO);
		condJmpToLabel(condOp, condLabel);
		releaseReg(dest);
	    }
	    return;

	default:
	    fprintf(stderr, "internal error: 'loadCond'\n");
	    exit(2);
    }
}

static size_t
loadExpr(const struct Expr *expr, Reg *dest_)
{
    if (! expr) {
	return 0;
    }
    switch (expr->op) {
	case EO_LIST:
	    {
		const struct ExprListItem *item = expr->u.l.first;
		for (; item->next; item = item->next) {
		    Reg tmp = loadExpr(item->expr, dest_);
		    releaseReg(tmp);
		}
		return loadExpr(item->expr, dest_);
	    }

	case EO_CALL:
	    return funcCall(expr, dest_);

	case EO_VAR:
	    if (expr->type->kind == ARRAY || expr->type->kind == FUNCTION) {
		return loadAddress(expr->u.v.memLoc.name,
				   expr->u.v.memLoc.offset, dest_);
	    }
	    return fetchValue(expr->type->size, expr->type->u.is_signed,
			      expr->u.v.memLoc, dest_);

	case EO_DEREF:
	    {
		Reg dest = loadExpr(expr->u.child[0], dest_);
		if (expr->type->kind != FUNCTION && expr->type->kind != ARRAY) {
		    dest = fetchAddressedValue(expr->type->size,
					       expr->type->u.is_signed,
					       dest, &dest);
		}
		return dest;
	    }

	case EO_ADDR:
	    expr = expr->u.child[0];
	    return loadAddress(expr->u.v.memLoc.name,
			       expr->u.v.memLoc.offset, dest_);

	case EO_LIT:
	    if (expr->type->kind == ARRAY) {
		fprintf(stderr, "internal error: EO_LIT in 'loadExpr'\n");
		exit(2);
	    } else {
		size_t size = expr->type->size;
		return expr->type->u.is_signed
		    ? loadSignedLiteral(size, expr->u.lit.int64, dest_)
		    : loadUnsignedLiteral(size, expr->u.lit.int64, dest_);
	    }

	case EO_UNARY_MINUS:
	    {
		Reg dest = loadExpr(expr->u.child[0], dest_);
		dest = rrrInstr(SUB_INSTR, dest, ZERO, &dest);
		return dest;
	    }

	case EO_POSTFIX_INCR:
	    {
		size_t scale = expr->u.child[0]->type->kind == POINTER
			|| expr->u.child[0]->type->kind == ARRAY
		    ? expr->u.child[0]->type->u.ref->size
		    : 1;
		Reg dest = loadExpr(expr->u.child[0], dest_);
		Reg tmp = urrInstr(ADD_INSTR, scale, dest, 0);
		saveExpr(tmp, expr->u.child[0]);
		releaseReg(tmp);
		return dest;
	    }

	case EO_POSTFIX_DECR:
	    {
		size_t scale = expr->u.child[0]->type->kind == POINTER
			|| expr->u.child[0]->type->kind == ARRAY
		    ? expr->u.child[0]->type->u.ref->size
		    : 1;
		Reg dest = loadExpr(expr->u.child[0], dest_);
		Reg tmp = urrInstr(SUB_INSTR, scale, dest, 0);
		saveExpr(tmp, expr->u.child[0]);
		releaseReg(tmp);
		return dest;
	    }

	case EO_BITWISE_NOT:
	    {
		enum InstrOp op = (enum InstrOp) expr->op;
		Reg dest = loadExpr(expr->u.child[0], 0);
		dest = rrInstr(op, dest, &dest);
		return dest;
	    }

	case EO_BITWISE_OR:
	case EO_BITWISE_XOR:
	case EO_BITWISE_AND:
	    {
		enum InstrOp op = (enum InstrOp) expr->op;
		Reg left = loadExpr(expr->u.child[0], 0);
		Reg right = loadExpr(expr->u.child[1], 0);
		Reg dest = rrrInstr(op, right, left, dest_);
		releaseReg(left);
		releaseReg(right);
		return dest;
	    }

	case EO_BITSHIFT_LEFT:
	case EO_BITSHIFT_RIGHT:
	    {
		enum InstrOp op = expr->type->u.is_signed
		    ? expr->op == EO_BITSHIFT_LEFT
			? SHL_SI_INSTR : SHR_SI_INSTR
		    : expr->op == EO_BITSHIFT_LEFT
			? SHL_UI_INSTR : SHR_UI_INSTR;
		Reg shift = loadExpr(expr->u.child[1], 0);
		Reg val = loadExpr(expr->u.child[0], 0);
		Reg dest = rrrInstr(op, shift, val, dest_);
		releaseReg(val);
		releaseReg(shift);
		return dest;
	    }

	case EO_ADD:
	case EO_SUB:
	    {
		enum InstrOp op = (enum InstrOp) expr->op;
		Reg left = loadExpr(expr->u.child[0], 0);

		size_t scale = expr->u.child[0]->type->kind == POINTER
			|| expr->u.child[0]->type->kind == ARRAY
		    ? expr->u.child[0]->type->u.ref->size
		    : 1;
		Reg dest;
		if (expr->u.child[1]->op == EO_LIT && scale == 1) {
		    dest = irrInstr(op, expr->u.child[1], left, dest_);
		    releaseReg(left);
		} else {
		    Reg right = loadExpr(expr->u.child[1], 0);
		    if (scale != 1) {
			urrInstr(MUL_INSTR, scale, right, &right);
		    }
		    dest = rrrInstr(op, right, left, dest_);
		    releaseReg(left);
		    releaseReg(right);
		}
		return dest;
	    }

	case EO_MUL:
	    {
		Reg left = loadExpr(expr->u.child[0], 0);
		Reg dest;
		if (expr->u.child[1]->op == EO_LIT) {
		    dest = irrInstr(MUL_INSTR, expr->u.child[1], left, dest_);
		    releaseReg(left);
		} else {
		    Reg right = loadExpr(expr->u.child[1], 0);
		    dest = rrrInstr(MUL_INSTR, right, left, dest_);
		    releaseReg(left);
		    releaseReg(right);
		}
		return dest;
	    }

	case EO_DIV:
	    {
		enum InstrOp op = expr->type->u.is_signed
		    ? DIV_SI_INSTR
		    : DIV_UI_INSTR;
		Reg tmpLeft = loadExpr(expr->u.child[0], 0);
		Reg tmpRight = loadExpr(expr->u.child[1], 0);
		Reg dest = rrrInstr(op, tmpRight, tmpLeft, dest_);
		releaseReg(tmpLeft);
		releaseReg(tmpRight);
		return dest;
	    }

	case EO_MOD:
	    {
		enum InstrOp op = expr->type->u.is_signed
		    ? MOD_SI_INSTR
		    : MOD_UI_INSTR;
		Reg tmpLeft = loadExpr(expr->u.child[0], 0);
		Reg tmpRight = loadExpr(expr->u.child[1], 0);
		Reg dest = rrrInstr(op, tmpRight, tmpLeft, dest_);
		releaseReg(tmpLeft);
		releaseReg(tmpRight);
		return dest;
	    }

	case EO_ASSIGN:
	    {
		Reg dest;
		if (expr->u.child[0]->type->kind != TK_STRUCT) {
		    dest = loadExpr(expr->u.child[1], dest_);
		    saveExpr(dest, expr->u.child[0]);
		} else {
		    const struct Expr *left = expr->u.child[0];
		    const struct Expr *right = expr->u.child[1];
		    size_t size = left->type->size;
		    size_t align = left->type->align;

		    dest = loadStructAddress(left, dest_);
		    Reg src;

		    if (right->op != EO_CALL) {
			src = loadStructAddress(right, 0);
			copyStruct(size, align, src, dest);
		    } else {
			src = loadExpr(expr->u.child[1], dest_);
			copyStruct(size, align, src, dest);
		    }
		    releaseReg(src);
		}
		return dest;
	    }

	case EO_LOGICAL_NOT:
	case EO_LOGICAL_OR:
	case EO_LOGICAL_AND:
	case EO_GE:
	case EO_GT:
	case EO_LE:
	case EO_LT:
	case EO_LOGICAL_NOT_EQUAL:
	case EO_LOGICAL_EQUAL:
	    {
		const char *falseLabel = makeLabel(0);
		const char *endLabel = makeLabel(0);
		loadCond(expr, 0, falseLabel);
		Reg dest = loadUnsignedLiteral(size_int, 1, dest_);
		jmpToLabel(endLabel);
		printLabel(falseLabel);
		dest = loadUnsignedLiteral(size_int, 0, &dest);
		printLabel(endLabel);
		return dest;
	    }

	case EO_COND:
	    {
		const char *falseLabel = makeLabel(0);
		const char *endLabel = makeLabel(0);
		loadCond(expr->u.child[0], 0, falseLabel);
		Reg dest = loadExpr(expr->u.child[1], dest_);
		jmpToLabel(endLabel);
		printLabel(falseLabel);
		loadExpr(expr->u.child[2], &dest);
		printLabel(endLabel);
		return dest;
	    }
	
	default:
	    ;
    }
    fprintf(stderr, "internal error: 'loadExpr'\n");
    printf("not handled:\n");
    printExpr(expr);
    printf("\n");
    exit(2);
    return ZERO;
}

static void
saveExpr(Reg src, const struct Expr *expr)
{
    switch (expr->op) {
	case EO_VAR:
	    storeValue(src, expr->type->size, expr->u.v.memLoc);
	    return;

	case EO_DEREF:
	    {
		Reg addr = loadExpr(expr->u.child[0], 0);
		storeAddressedValue(src, expr->type->size, addr);
		releaseReg(addr);
	    }
	    return;

	default:
	    fprintf(stderr, "internal error: 'saveExpr'\n");
	    exit(2);
    }
}

static void
genCodeExpr(const struct Expr *expr)
{
    size_t reg = loadExpr(expr, 0);
    releaseReg(reg);
}

static void
genCodeContinue(const struct Stmt *stmt)
{
    if (! continueLabel) {
	errorMsg(&stmt->loc, "continue statement not within a loop");
	exit(1);
    }
    jmpToLabel(continueLabel);
}

static void
genCodeBreak(const struct Stmt *stmt)
{
    if (! continueLabel) {
	errorMsg(&stmt->loc, "break statement not within a loop");
	exit(1);
    }
    jmpToLabel(breakLabel);
}

static void
genCodeReturn(const struct Stmt *stmt)
{
    const struct Expr *expr = stmt->u.expr;

    if (expr) {
	// TODO: should not be done in the code generator
	if (! assignmentCompatible(expr->loc, true, retType, expr, 0)) {
    	    errorMsg(&expr->loc, "can not convert expression to "
    	    	 " return type");
    	    exit(1);
    	}
	if (expr->type->kind != TK_STRUCT) {
	    size_t reg = loadExpr(expr, 0);
	    store(retType->size, reg, 16, FP, ZERO, 1);
	    releaseReg(reg);
	} else {
	    returnStruct(expr);
	}
    }
    jmpToLabel(leaveLabel);
}

static void genCodeStmt(const struct Stmt *stmt);

static void
genCodeIf(const struct Stmt *stmt)
{
    /*
    const char *endifLabel = makeLabel("endif");
    const char *thenLabel = makeLabel("then");

    loadCond(stmt->u.c.cond, thenLabel, 0);
    if (stmt->u.c.elseStmt) {
	printComment("\t#else\n");
	genCodeStmt(stmt->u.c.elseStmt);
    }
    jmpToLabel(endifLabel);
    printLabel(thenLabel);
    printComment("\t#then\n");
    genCodeStmt(stmt->u.c.thenStmt);
    printLabel(endifLabel);
    printComment("\t#endif\n");
    */

    const char *endifLabel = makeLabel("endif");
    const char *elseLabel = stmt->u.c.elseStmt ? makeLabel("else") : endifLabel;

    loadCond(stmt->u.c.cond, 0, elseLabel);
    printComment("\t#then\n");
    genCodeStmt(stmt->u.c.thenStmt);
    if (stmt->u.c.elseStmt) {
	jmpToLabel(endifLabel);
	printLabel(elseLabel);
	printComment("\t#else\n");
	genCodeStmt(stmt->u.c.elseStmt);
    }
    printLabel(endifLabel);
    printComment("\t#endif\n");
}

static void
genCodeWhile(const struct Stmt *stmt)
{
    const char *oldContinueLabel = continueLabel, *oldBreakLabel = breakLabel;
    continueLabel = makeLabel("check.while.cond");
    breakLabel = makeLabel("while.end");;

    printLabel(continueLabel);
    loadCond(stmt->u.l.cond, 0, breakLabel);

    printComment("\t#while loop body\n");
    genCodeStmt(stmt->u.l.stmt);
    jmpToLabel(continueLabel);
    printComment("\t#end of while loop body\n");
    printLabel(breakLabel);

    continueLabel = oldContinueLabel;
    breakLabel = oldBreakLabel;
}

static void
genCodeFor(const struct Stmt *stmt)
{
    const char *condLabel = makeLabel("check.for.cond");
    const char *oldContinueLabel = continueLabel, *oldBreakLabel = breakLabel;
    continueLabel = makeLabel("for.incr");
    breakLabel = makeLabel("for.end");;

    genCodeStmt(stmt->u.l.init);
    printLabel(condLabel);
    loadCond(stmt->u.l.cond, 0, breakLabel);

    printComment("\t#for loop body\n");
    genCodeStmt(stmt->u.l.stmt);

    printLabel(continueLabel);
    releaseReg(loadExpr(stmt->u.l.incr, 0));
    jmpToLabel(condLabel);
    printComment("\t#end of for loop body\n");
    printLabel(breakLabel);

    continueLabel = oldContinueLabel;
    breakLabel = oldBreakLabel;
}


static void
genCodeDoWhile(const struct Stmt *stmt)
{
    const char *oldContinueLabel = continueLabel, *oldBreakLabel = breakLabel;

    continueLabel = makeLabel("do.while");
    breakLabel = makeLabel("do.while.end");;

    printLabel(continueLabel);
    printComment("\t#do while loop body\n");
    genCodeStmt(stmt->u.l.stmt);
    printComment("\t#end of do while loop body\n");
    loadCond(stmt->u.l.cond, continueLabel, 0);
    printLabel(breakLabel);

    continueLabel = oldContinueLabel;
    breakLabel = oldBreakLabel;
}

static void
genCodeStmt(const struct Stmt *stmt)
{
    if (! stmt) {
	return;
    }

    switch (stmt->kind) {
	case STMT_BEGIN:
	    break;
	case STMT_COMPOUND:
	    printComment("\t#%zu {...\n", compoundLevel++);
	    genCodeStmt(stmt->u.body);
	    printComment("\t#%zu ...}\n", --compoundLevel);
	    break;
	case STMT_ASSIGN:
	    if (stmt->u.expr) {
		printComment("\t# %s;\n", exprStr(stmt->u.expr));
		genCodeExpr(stmt->u.expr);
	    } else {
		printComment("\t# ;\n");
	    }
	    break;
	case STMT_CONTINUE:
	    printComment("\t# continue;\n");
	    genCodeContinue(stmt);
	    break;
	case STMT_BREAK:
	    printComment("\t# break;\n");
	    genCodeBreak(stmt);
	    break;
	case STMT_RETURN:
	    printComment("\t# return %s;\n", exprStr(stmt->u.expr));
	    genCodeReturn(stmt);
	    break;
	case STMT_IF:
	    printComment("\t#if (%s)\n", exprStr(stmt->u.c.cond));
	    genCodeIf(stmt);
	    break;
	case STMT_WHILE:
	    printComment("\t#while (%s)\n", exprStr(stmt->u.l.cond));
	    genCodeWhile(stmt);
	    break;
	case STMT_DO:
	    printComment("\t#do\n");
	    genCodeDoWhile(stmt);
	    printComment("\t#while (%s)\n", exprStr(stmt->u.l.cond));
	    break;
	case STMT_FOR:
	    printComment("\t#for (");
	    for (const struct Stmt *s = stmt->u.l.init; s; s = s->next) {
		if (s->kind == STMT_BEGIN) {
		    continue;
		} else if (s->kind == STMT_COMPOUND) {
		    s = s->u.body;
		} else if (s->kind == STMT_ASSIGN) {
		    printComment("%s", exprStr(s->u.expr));
		    if (s->next) {
			printComment(", ");
		    }
		} else {
		    fprintf(stderr, "internal error: unexpected statement in"
			    " for initializer\n");
		    exit(2);
		}
	    }
	    printComment("; ");
	    printComment("%s; ", exprStr(stmt->u.l.cond));
	    printComment("%s)\n", exprStr(stmt->u.l.incr));
	    genCodeFor(stmt);
	    break;

	default:
	    fprintf(stderr, "internal error: 'genCodeStmt'\n");
	    exit(2);
    }
    genCodeStmt(stmt->next);
}

void
gencodeData(const struct Expr *expr)
{
    if (expr->op == EO_LIST) {
	for (struct ExprListItem *it = expr->u.l.first; it; it = it->next) {
	    gencodeData(it->expr);
	}
	return;
    }
    enum Directive dir = expr->type->size;
    if (expr->op != EO_ASSIGN) {
	fprintf(stderr, "internal error 'gencodeData'\n");
	exit(2);
    }
    printDirective(dir, constExprStr(expr->u.child[1]));
}

enum {
    RET_OFFSET = 0,
    FP_OFFSET = 8,
    RVAL_OFFSET = 16,
};

static void
funcPrologue(bool global, const char *name, size_t stackFrame)
{
    printComment("/*\n");
    printComment(" * function %s\n", name);
    printComment(" **/\n");
    if (global) {
	printDirective(GLOBL, name);
    }
    printLabel(name);
    printComment("\t// function prologue\n");
    store(sizeOfSizeT, RET, RET_OFFSET, SP, 0, 0);
    store(sizeOfSizeT, FP, FP_OFFSET, SP, 0, 0);
    loadReg(SP, &FP);
    urrInstr(SUB_INSTR, stackFrame, SP, &SP);

    printComment("\n\t// begin of the function body\n");
}

static void
funcEpilogue(const char *name)
{
    printComment("\t// end of the function body\n\n");
    printComment("\t// function epilogue\n");
    if (name == addString("main")) {
	printComment("\t// 'main' returns 0 if there is no return statement\n");
    	store(retType->size, ZERO, 16, FP, ZERO, 1);
    }
    printLabel(leaveLabel);
    loadReg(FP, &SP);
    fetch(sizeOfSizeT, false, FP_OFFSET, SP, 0, 0, &FP);
    fetch(sizeOfSizeT, false, RET_OFFSET, SP, 0, 0, &RET);
    returnInstr();
    printComment("\n");
}

static size_t
gcd(size_t a, size_t b)
{
    while (b != 0) {
        size_t r = a % b;
        a = b;
        b = r;
    }
    return a;
}

size_t
maxAlign(size_t align1, size_t align2)
{
    return align1 * align2 / gcd(align1, align2);
}

size_t
roundUp(ptrdiff_t a, size_t m)
{
    return (a + m - 1) / m * m;
}

size_t
roundDown(ptrdiff_t a, size_t m)
{
    if (a >= 0) {
	return a / m * m;
    }
    return -roundUp(-a, m);
}

struct Node *
makeNode(enum NodeKind kind)
{
    struct Node *node = alloc(NODE, sizeof(*node));
    node->kind = kind;
    node->next = 0;
    return node;
}

struct Node *
makeFuncDef(bool global, const struct Type *retType, const char *name,
	    size_t stackFrame, const struct Stmt *body)
{
    struct Node *node = makeNode(FUNCDEF);
    node->global = global;
    node->type = retType;
    node->name = name;
    node->u.f.stackFrame = stackFrame;
    node->u.f.body = body;
    return node;
}

struct Node *
makeVarDef(bool global, const char *name, const struct Type *type,
	   const struct Expr *init)
{
    struct Node *node = makeNode(VARDEF);
    node->global = global;
    node->type = type;
    node->name = name;
    node->u.v.init = init;
    return node;
}

struct Node *
makeStrLit(const char *name, const struct Type *type, const char *str,
	   const char *escStr)
{
    if (! name) {
	fprintf(stderr, "internal error: 'makeStrLit' no name\n");
	exit(2);
	return 0;
    }
    struct Node *node = makeNode(STRLIT);
    node->global = false;
    node->type = type;
    node->name = name;
    node->u.s.str = str;
    node->u.s.escStr = escStr;
    return node;
}

static struct Node *prog, *progEnd;

void
appendNode(struct Node *node)
{
    if (! node) {
	return;
    }
    if (! prog) {
	progEnd = prog = makeNode(NODE_BEGIN);
    }
    progEnd->next = node;
    progEnd = node;
}

void
gencode()
{
    for (struct Node *node = prog; node; node = node->next) {
	switch (node->kind) {
	    case NODE_BEGIN:
		break;

	    case STRLIT:
		printDirective(DATA);
		printLabel(node->name);
		printDirective(D_STRING, node->u.s.escStr);
		break;

	    case VARDEF:
		if (node->u.v.init) {
		    printDirective(DATA);
		} else {
		    printDirective(BSS);
		}
		if (node->global) {
		    printDirective(GLOBL, node->name);
		}
		printDirective(ALIGN, node->type->align);
		printLabel(node->name);
		gencodeData(node->u.v.init);
		break;

	    case FUNCDEF:
		printDirective(TEXT);
		retType = node->type;
		leaveLabel = makeLabel("leave");
		funcPrologue(node->global, node->name, node->u.f.stackFrame);
		genCodeStmt(node->u.f.body);
		funcEpilogue(node->name);

		retType = 0;
		leaveLabel = 0;
		break;
	}
    }
}
