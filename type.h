#ifndef ULMCC_TYPE_H
#define ULMCC_TYPE_H

#include <stdbool.h>
#include <stddef.h>

#include "lexer.h"

struct Symbol;

enum TypeKind {
    TK_CONST = CONST,
    TK_ENUM = ENUM,
    TK_STRUCT = STRUCT,
    TK_UNION = UNION,
    TK_VOID = VOID,
    TK_DUMMY,
    INTEGER,
    POINTER,
    ARRAY,
    FUNCTION,
};

struct Type {
    enum TypeKind kind;

    union {
	const struct Type *ref;
	const struct Symbol *tag;
	bool is_signed;
    } u;
    
    union {
	size_t dim;
	const struct Type **param;
	const struct Type *enumRep;
    } p;

    bool tagged;
    size_t size, align;
};

void openTypeScope();
void reopenPreviousTypeScope();
void closeTypeScope();

const struct Type *unqualType(const struct Type *type);

const struct Type *makeDummyType(const struct Type *base);
const struct Type *makeVoidType();
const struct Type *makeUnsignedIntegerType(size_t size);
const struct Type *makeSignedIntegerType(size_t size);
const struct Type *makeUnsignedIntegerTypeFromLiteral(uint64_t literal);
const struct Type *makeSignedIntegerTypeFromLiteral(int64_t literal);
const struct Type *makeConstType(const struct Type *type);
const struct Type *makePointerType(const struct Type *ref);
const struct Type *makeArrayType(const struct Type *elemType, size_t dim);

const struct Type **allocParamArray(size_t len);
const struct Type *makeFunctionType(const struct Type *ret,
				    const struct Type **param);

const struct Type *makeIncompleteEnumType(const struct Symbol *tag);
const struct Type *makeIncompleteStructType(const struct Symbol *tag);
const struct Type *makeIncompleteUnionType(const struct Symbol *tag);
const struct Type *completeTaggedType(const struct Symbol *tag, size_t size,
				      size_t align);

const char *typeStr(const struct Type *type);
void printType(const struct Type *type);
void printTypeSet();

#endif // ULMCC_TYPE_H
