#ifndef ULMCC_EXPR_H
#define ULMCC_EXPR_H

#include <stdint.h>

#include "lexer.h"
#include "symtab.h"
#include "type.h"

enum ExprOp {
    // expressions without any child nodes
    EO_VAR = IDENT,
    EO_LIT = TOKEN_KIND_LAST,
    EO_LIST = TOKEN_KIND_LAST + 1,

    // expressions with two child nodes
    EO_ADD = PLUS,
    EO_ASSIGN = EQUAL,
    EO_CALL = TOKEN_KIND_LAST + 2,
    EO_DIV = SLASH,
    EO_GE = GE,
    EO_GT = GT,
    EO_LE = LE,
    EO_LT = LT,
    EO_LOGICAL_AND = AMPERSAND2,
    EO_LOGICAL_EQUAL = EQUAL2,
    EO_LOGICAL_NOT = EXCLAMATION,
    EO_LOGICAL_NOT_EQUAL = NE,
    EO_LOGICAL_OR = VBAR2,

    EO_BITWISE_AND = AMPERSAND,
    EO_BITWISE_NOT = TILDE,
    EO_BITWISE_OR = VBAR,
    EO_BITWISE_XOR = CARET,
    EO_BITSHIFT_LEFT = LT2,
    EO_BITSHIFT_RIGHT = GT2,

    EO_MEMBER = DOT,
    EO_MOD = PERCENT,
    EO_MUL = ASTERISK,
    EO_PREFIX_DECR = MINUS_EQ,
    EO_PREFIX_INCR = PLUS_EQ,
    EO_SUB = MINUS,

    // expressions with three child nodes
    EO_COND = QMARK,

    // expressions with one child node
    EO_SIZEOF = SIZEOF,
    EO_ADDR = TOKEN_KIND_LAST + 3,
    EO_UNARY_MINUS = TOKEN_KIND_LAST + 4,
    EO_UNARY_PLUS = TOKEN_KIND_LAST + 5,
    EO_DEREF = TOKEN_KIND_LAST + 6,
    EO_POSTFIX_INCR = TOKEN_KIND_LAST + 7,
    EO_POSTFIX_DECR = TOKEN_KIND_LAST + 8,
    EO_LAST,
};

struct Expr
{
    const struct Expr *unfolded;
    struct TokenLoc loc;
    enum ExprOp op;

    union {
        const struct Expr *child[3];
	struct {
	    struct MemLocation memLoc;
	    const char *ident;
	} v;
	struct {
	    struct ExprListItem {
		const struct Expr *expr;
		struct ExprListItem *next;
	    } *first, *last;
	    size_t len;
	} l;
	union LiteralValue lit;
    } u;

    const struct Type *type;
    bool is_const;
    bool is_lvalue;
};

struct Expr *makeExprList(struct TokenLoc loc);
void exprListAppend(struct Expr *list, const struct Expr *expr);
struct Expr *makeRefExpr(struct TokenLoc loc, const char *ident,
			 const char *name, ptrdiff_t offset,
			 const struct Type *type);
struct Expr *makeVarExpr(struct TokenLoc loc,
			 const struct Symbol *symbol);
struct Expr *makeLitExpr(struct TokenLoc loc, const struct Type *type,
			 union LiteralValue literalValue);
struct Expr *makeSIntExpr(struct TokenLoc loc, size_t size,
			  int64_t literal);
struct Expr *makeUIntExpr(struct TokenLoc loc, size_t size,
			  int64_t literal);

struct Expr *makeUnaryExpr(struct TokenLoc opLoc, enum ExprOp op,
			   const struct Expr *first);
struct Expr *makeBinaryExpr(struct TokenLoc opLoc, enum ExprOp op,
			    const struct Expr *first,
			    const struct Expr *second);
struct Expr *makeTernaryExpr(struct TokenLoc opLoc, enum ExprOp op,
			     const struct Expr *first,
			     const struct Expr *second,
			     const struct Expr *third);

bool assignmentCompatible(struct TokenLoc loc, bool init,
			  const struct Type *lType,
			  const struct Expr *rExpr,
			  const struct Type *rType);

const struct Expr *parseExpr();
const struct Expr *parseConditionalExpression();
const struct Expr *parseInitializerList();
const struct Expr *parseLogicalOrExpression();

void printExprTree(const struct Expr *expr);
void printExpr(const struct Expr *expr);
const char *exprStr(const struct Expr *expr);
const char *constExprStr(const struct Expr *expr);

#endif // ULMCC_EXPR_H

