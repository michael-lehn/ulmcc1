#ifndef ULMCC_STMT_H
#define ULMCC_STMT_H 1

#include "expr.h"

enum StmtKind {
    STMT_BEGIN,
    STMT_BREAK,
    STMT_CONTINUE,
    STMT_RETURN,
    STMT_COMPOUND,
    STMT_ASSIGN,
    STMT_IF,
    STMT_WHILE,
    STMT_DO,
    STMT_FOR,
};

struct Stmt {
    enum StmtKind kind;
    struct TokenLoc loc;

    union {
	const struct Expr *expr;
	const struct Stmt *body;
	struct {
	    const struct Stmt *init;
	    const struct Expr *cond;
	    const struct Expr *incr;
	    const struct Stmt *stmt;
	} l;
	struct {
	    const struct Expr *cond;
	    const struct Stmt *thenStmt;
	    const struct Stmt *elseStmt;
	} c;
    } u;

    struct Stmt *next;
};

struct Stmt *appendStmt(struct Stmt *last, struct Stmt *next);
struct Stmt *makeStmt(struct TokenLoc loc, enum StmtKind kind);
struct Stmt *makeCompoundStmt(struct TokenLoc loc, const struct Stmt *body);
struct Stmt *makeExprStmt(struct TokenLoc loc, enum StmtKind kind,
			  const struct Expr *expr);
struct Stmt *makeLoopStmt(struct TokenLoc loc, enum StmtKind kind,
			  const struct Expr *cond, const struct Stmt *body);
struct Stmt *makeForLoopStmt(struct TokenLoc loc, const struct Stmt *init,
			     const struct Expr *cond, const struct Expr *incr,
			     const struct Stmt *body);
struct Stmt *makeIfStmt(struct TokenLoc loc, const struct Expr *cond,
			const struct Stmt *thenStmt,
			const struct Stmt *elseStmt);

void printStmt(const struct Stmt *stmt);

#endif // ULMCC_STMT_H


