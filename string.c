#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include "memregion.h"
#include "string.h"

static size_t labelCount;

static ptrdiff_t
strcmp_(const char *s1, const char *s2)
{
    for (; *s1 && *s1 == *s2; ++s1, ++s2) {
    }
    return *s1 - *s2;
}

static void
strcpy_(char *dst, const char *src)
{
    while (src && (*dst++ = *src++)) {
    }
}

static size_t
strlen_(const char *s)
{
    const char *cp;
    for (cp = s; cp && *cp; ++cp) {
    }
    return cp - s;
}

struct Entry;

struct EntryHeader
{
    struct Entry *next;
    size_t len;
};

struct Entry
{
    struct EntryHeader h;
    char str[];
};

static struct Entry *stringList;

static struct Entry *
newStringEntry(size_t len)
{
    struct Entry *entry = alloc(STRING, len + sizeof(struct EntryHeader) + 1);
    entry->h.len = len;
    entry->h.next = stringList;
    stringList = entry;
    return entry;
}

const char *
addString(const char *s)
{
    size_t len = strlen_(s);

    // search
    for (struct Entry *entry = stringList; entry; entry = entry->h.next) {
	if (len == entry->h.len && !strcmp_(s, entry->str)) {
	    return entry->str;
	}
    }

    // insert if not found
    struct Entry *entry = newStringEntry(len);
    strcpy_(entry->str, s);

    return entry->str;
}

size_t
getStringLen(const char *str)
{
    return ((struct Entry *)(str - sizeof(struct EntryHeader)))->h.len;
}

const char *
makeLabel(const char *prefix)
{
    const char *labelPrefix = ".L";
    size_t lenPrefix = strlen_(prefix);
    size_t lenLabelPrefix = strlen_(labelPrefix);

    size_t lenLabelId = 0;
    size_t i = labelCount;
    do {
	++lenLabelId;
	i /= 10;
    } while (i);

    size_t len = lenPrefix + lenLabelPrefix + lenLabelId;
    struct Entry *entry = newStringEntry(len);
    strcpy_(entry->str, prefix);
    strcpy_(entry->str + lenPrefix, labelPrefix);

    char *s = entry->str + len; 
    i = labelCount;
    *(s--) = 0;
    do {
	*(s--) = i % 10 + '0';
	i /= 10;
    } while (i);

    ++labelCount;
    return entry->str;
}

static size_t bufLen;
static char *buf;

const char *
stringCat(const char *str1, const char *str2)
{
    if (! str1 || ! str2) {
	return 0;
    }

    size_t len1 = getStringLen(str1);
    size_t len2 = getStringLen(str2);

    if (! len1) {
	return str2;
    } else if (! len2) {
	return str1;
    }

    if (bufLen < len1 + len2) {
	bufLen = 1;
	while (bufLen < len1 + len2) {
	    bufLen *= 2;
	}
	free(buf);
	buf = malloc(bufLen);
    }
    strcpy_(buf, str1);
    strcpy_(buf + len1, str2);
    return addString(buf);
}

static const char *
findLast(char c, const char *str)
{
    const char *p;
    for (p = str + getStringLen(str); p != str && *p != c; --p) {
    }
    return *p == c ? p : 0;
}

const char *
path(const char *filepath)
{
    const char *p = findLast('/', filepath);
    if (! p) {
	return addString("./");
    }
    ++p;
    char sav = *p;
    *(char *)p = 0;
    filepath = addString(filepath);
    *(char *)p = sav;
    return filepath;
}

const char *
filename(const char *filepath)
{
    const char *p = findLast('/', filepath);
    if (! p) {
	return filepath;
    }
    if (! *(p + 1)) {
	return 0;
    }
    return addString(p + 1);
}

const char *
extension(const char *filepath)
{
    const char *p = findLast('.', filepath);
    if (! p) {
	return 0;
    }
    return addString(p + 1);
}

const char *
basename(const char *filepath)
{
    filepath = filename(filepath);
    if (! filepath) {
	return 0;
    }
    const char *p = findLast('.', filepath);
    if (! p) {
	return filepath;
    }
    char sav = *p;
    *(char *)p = 0;
    filepath = addString(filepath);
    *(char *)p = sav;
    return filepath;
}

void
printStringPool()
{
    for (struct Entry *entry = stringList; entry; entry = entry->h.next) {
	printf("'%s' len = %zu at %p\n", entry->str, getStringLen(entry->str),
	       entry->str);
    }
}
