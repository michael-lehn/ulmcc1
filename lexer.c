#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"
#include "string.h"
#include "type.h"

#ifndef LEXER_TOKEN_BUFFER_SIZE
#define LEXER_TOKEN_BUFFER_SIZE 256
#endif

#ifndef LEXER_LINE_BUFFER_SIZE
#define LEXER_LINE_BUFFER_SIZE 256
#endif

enum ErrorCode
{
    OP_NOT_IMPLEMENTED,
    SINGLE_QUOTE_AS_CHARACTER_LITERAL,
    NEWLINE_AS_CHARACTER_LITERAL,
    EXPECTED_HEXDIGIT,
    INVALID_CHARACTER_LITERAL,
    EOF_IN_CHARACTER_LITERAL,
    NEWLINE_IN_STRING_LITERAL,
    EOF_IN_STRING_LITERAL,
    INVALID_HEXADECIMAL_LITERAL,
    EOF_IN_DELIMITED_COMMENT,
    FOUND_BAD_TOKEN,
    UNSUPPORTED,
};

static const char* includePath[] = {
    "./",
    "/Users/lehn/work/ulmcc1/examples/",
    "/home/numerik/pub/ulmcc/include/",
};

static const char* errorStr[] = {
    // OP_NOT_IMPLEMENTED
    "not your fault. Operation is currently not implemented",

    // SINGLE_QUOTE_AS_CHARACTER_LITERAL
    "single quote charater is not permitted as character constant",

    // NEWLINE_AS_CHARACTER_LITERAL,
    "newline not permitted in character constant",

    // EXPECTED_HEXDIGIT
    "hexadecimal digit expected",

    // INVALID_CHARACTER_LITERAL
    "invalid character constant",

    // EOF_IN_CHARACTER_LITERAL
    "end of file in character constant",

    // NEWLINE_IN_STRING_LITERAL
    "newline not permitted in string constant",

    // EOF_IN_STRING_LITERAL
    "end of file in string constant",

    // INVALID_HEXADECIMAL_LITERAL
    "invalid hexadecimal constant",

    // EOF_IN_DELIMITED_COMMENT
    "unexpected eof in delimited comment",

    // FOUND_BAD_TOKEN
    "bad token",

    // UNSUPPORTED
    "(currently) not supported",
};

static const char* tokenKindStr_[] = {
    "EOI",
    "BAD_TOKEN",

    // identifier
    "CHARACTER_LITERAL",
    "DECIMAL_LITERAL",
    "HEXADECIMAL_LITERAL",
    "IDENT",
    "OCTAL_LITERAL",
    "STRING_LITERAL",

    // punctuators
    "AMPERSAND",
    "AMPERSAND_EQ",
    "AMPERSAND2",
    "ARROW",
    "ASTERISK",
    "ASTERISK_EQ",
    "CARET",
    "CARET_EQ",
    "COLON",
    "COMMA",
    "DOT",
    "DOT3",
    "EQUAL",
    "EQUAL2",
    "EXCLAMATION",
    "GE",
    "GT",
    "GT2",
    "GT2_EQ",
    "LBRACE",
    "LBRACKET",
    "LE",
    "LPAREN",
    "LT",
    "LT2",
    "LT2_EQ",
    "MINUS",
    "MINUS2",
    "MINUS_EQ",
    "NE",
    "PERCENT",
    "PERCENT_EQ",
    "PLUS",
    "PLUS2",
    "PLUS_EQ",
    "QMARK",
    "RBRACE",
    "RBRACKET",
    "RPAREN",
    "SEMICOLON",
    "SLASH",
    "SLASH_EQ",
    "TILDE",
    "TILDE_EQ",
    "VBAR",
    "VBAR_EQ",
    "VBAR2",

    // keywords
    "AUTO",
    "BREAK",
    "CASE",
    "CHAR",
    "CONST",
    "CONTINUE",
    "DEFAULT",
    "DO",
    "DOUBLE",
    "ELSE",
    "ENUM",
    "EXTERN",
    "FLOAT",
    "FOR",
    "GOTO",
    "IF",
    "INLINE",
    "INT",
    "LONG",
    "REGISTER",
    "RESTRICT",
    "RETURN",
    "SHORT",
    "SIGNED",
    "SIZEOF",
    "STATIC",
    "STRUCT",
    "SWITCH",
    "TYPEDEF",
    "UNION",
    "UNSIGNED",
    "VOID",
    "VOLATILE",
    "WHILE",
    "UNDERSCORE_BOOL",
    "UNDERSCORE_COMPLEX",
    "UNDERSCORE_IMAGINARY",
};

static const char* keywordStr_[] = {
    "<EOI>",
    "<BAD_TOKEN>",

    // literals, identifier
    "<CHARACTER_LITERAL>",
    "<DECIMAL_LITERAL>",
    "<HEXADECIMAL_LITERAL>",
    "<IDENT>",
    "<OCTAL_LITERAL>",
    "<STRING_LITERAL>",

    // punctuators
    "&",
    "&=",
    "&&",
    "->",
    "*",
    "*=",
    "^",
    "^=",
    ":",
    ",",
    ".",
    "...",
    "=",
    "==",
    "!",
    ">=",
    ">",
    ">>",
    ">>=",
    "{",
    "[",
    "<=",
    "(",
    "<",
    "<<",
    "<<=",
    "-",
    "--",
    "-=",
    "!=",
    "%",
    "%=",
    "+",
    "++",
    "+=",
    "?",
    "}",
    "]",
    ")",
    ";",
    "/",
    "/=",
    "~",
    "~=",
    "|",
    "|=",
    "||",

    // keywords
    "auto",
    "break",
    "case",
    "char",
    "const",
    "continue",
    "default",
    "do",
    "double",
    "else",
    "enum",
    "extern",
    "float",
    "for",
    "goto",
    "if",
    "inline",
    "int",
    "long",
    "register",
    "restrict",
    "return",
    "short",
    "signed",
    "sizeof",
    "static",
    "struct",
    "switch",
    "typedef",
    "union",
    "unsigned",
    "void",
    "volatile",
    "while",
    "_Bool",
    "_Complex",
    "_Imaginary",
};

enum TokenKind tokenKind;
struct TokenLoc tokenLoc;
const char *tokenValue, *processedTokenValue;
union LiteralValue literalValue;
const struct Type *literalType;

struct TokenValue
{
    char buf[LEXER_TOKEN_BUFFER_SIZE];
    char* cp;
};

struct InputLine
{
    char buffer[LEXER_LINE_BUFFER_SIZE];
    char *cp, *end;
    size_t lineNumber;
    bool eof;
};

struct OpenLexFile
{
    FILE* in;
    const char* filename;
    struct TokenPos pos;
    struct OpenLexFile* next;
};

static const char* currentFilename;
static struct TokenPos pos = { 1, 1 };
static int ch;
static FILE* in_;
static struct TokenValue tokenValue_, processedTokenValue_;
static struct InputLine inputLine_[2], *inputLine = &inputLine_[0];
static struct OpenLexFile* openLexFile;

static void
octLiteral()
{
    bool is_signed = true;
    const char* s = tokenValue + 1;

    literalValue.uint64 = 0;
    while (*s) {
	int d = *s++ - '0';
	literalValue.uint64 *= 8;
	literalValue.uint64 += d;
	if (*s == 'u') {
	    // is_signed = false;
	    break;
	}
    }
    literalType = is_signed
		    ? makeSignedIntegerTypeFromLiteral(literalValue.uint64)
		    : makeUnsignedIntegerTypeFromLiteral(literalValue.uint64);
}

static void
decLiteral()
{
    bool is_signed = true;
    const char* s = tokenValue;

    literalValue.uint64 = 0;
    while (*s) {
	int d = *s++ - '0';
	literalValue.uint64 *= 10;
	literalValue.uint64 += d;
	if (*s == 'u') {
	    is_signed = false;
	    break;
	}
    }
    literalType = is_signed
		    ? makeSignedIntegerTypeFromLiteral(literalValue.uint64)
		    : makeUnsignedIntegerTypeFromLiteral(literalValue.uint64);
}

static void
hexLiteral()
{
    bool is_signed = true;
    const char* s = tokenValue + 2;

    literalValue.uint64 = 0;
    while (*s) {
	int d = *s++;
	if (d >= 'a' && d <= 'f') {
	    d = 10 + d - 'a';
	} else if (d >= 'A' && d <= 'F') {
	    d = 10 + d - 'A';
	} else {
	    d -= '0';
	}
	literalValue.uint64 *= 16;
	literalValue.uint64 += d;
	if (*s == 'u') {
	    is_signed = false;
	    break;
	}
    }
    literalType = is_signed
		    ? makeSignedIntegerTypeFromLiteral(literalValue.uint64)
		    : makeUnsignedIntegerTypeFromLiteral(literalValue.uint64);
}

static void
tokenValue_disable(struct TokenValue* tokenValue)
{
    tokenValue->cp = 0;
}

static void
tokenValue_clear(struct TokenValue* tokenValue)
{
    tokenLoc.filename = currentFilename;
    tokenValue->cp = tokenValue->buf;
    *(tokenValue->cp) = 0;
}

static void
tokenValue_append(struct TokenValue* tokenValue, int ch)
{
    if (!tokenValue->cp) {
	return;
    }
    *(tokenValue->cp++) = ch;
    if (tokenValue->cp - tokenValue->buf >= LEXER_TOKEN_BUFFER_SIZE) {
	fprintf(stderr,
		"limit of %d bytes for tokenValue exeeded.\n",
		LEXER_TOKEN_BUFFER_SIZE);
	fprintf(stderr, "last character appended was '%c'\n", ch);
	stop(2);
    }
    *(tokenValue->cp) = 0;
}

static bool resumeLexerIn();
static bool popIfClause();

static void
fillLineBuffer()
{
    size_t nextLineNumber = inputLine->lineNumber + 1;
    ptrdiff_t i = inputLine - inputLine_;

    inputLine = &inputLine_[1 - i];

    inputLine->cp = inputLine->end = inputLine->buffer;
    inputLine->lineNumber = nextLineNumber;

    int ch;
    do {
	if (inputLine->end - inputLine->buffer >= LEXER_LINE_BUFFER_SIZE) {
	    fprintf(stderr,
		    "limit of %d bytes for line buffer exeeded.\n",
		    LEXER_LINE_BUFFER_SIZE);
	    stop(2);
	}
	ch = fgetc(in_);
	if (ch == EOF) {
	    inputLine->eof = true;
	    break;
	}
	*inputLine->end++ = ch;
    } while (ch != '\n');

    *inputLine->end = 0;
    if (inputLine->eof) {
	if (resumeLexerIn()) {
	    inputLine->eof = false;
	    fillLineBuffer();
	} else {
	    if (popIfClause()) {
		errorMsg(0, "unterminated conditional directive\n");
		stop(1);
	    }
	}
    }
}

const char*
currentInputLine()
{
    return inputLine->buffer;
}

size_t
currentInputLineNumber()
{
    return inputLine->lineNumber;
}

const char*
previousInputLine()
{
    ptrdiff_t i = inputLine - inputLine_;
    const struct InputLine* prev = &inputLine_[1 - i];
    return prev->lineNumber ? prev->buffer : 0;
}

size_t
previousInputLineNumber()
{
    ptrdiff_t i = inputLine - inputLine_;
    const struct InputLine* prev = &inputLine_[1 - i];
    return prev->lineNumber;
}

const char*
tokenKindStr(enum TokenKind tokenKind)
{
    return tokenKindStr_[tokenKind];
}

const char*
keywordStr(enum TokenKind tokenKind)
{
    return keywordStr_[tokenKind];
}

static bool
tokenFixIdentToKeyword()
{
    static bool first = true;

    if (first) {
	first = false;
	for (size_t i = AUTO; i < TOKEN_KIND_LAST; ++i) {
	    keywordStr_[i] = addString(keywordStr_[i]);
	}
    }

    if (tokenKind != IDENT) {
	return false;
    }
    for (size_t i = AUTO; i < TOKEN_KIND_LAST; ++i) {
	if (tokenValue == keywordStr_[i]) {
	    tokenKind = i;
	    return true;
	}
    }
    return false;
}

static void
lexError(enum ErrorCode errorCode)
{
    errorMsg(&tokenLoc, errorStr[errorCode]);
    stop(1);
}

bool
is_space(char ch)
{
    return ch == ' ' || ch == '\r' || ch == '\f' || ch == '\v' || ch == '\t' ||
	   ch == '\n';
}

bool
is_letter(char ch)
{
    return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || ch == '_';
}

bool
is_digit(char ch)
{
    return ch >= '0' && ch <= '9';
}

bool
is_octaldigit(char ch)
{
    return ch >= '0' && ch <= '7';
}

bool
is_hexdigit(char ch)
{
    return (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') ||
	   (ch >= 'A' && ch <= 'F');
}

unsigned
hex_to_val(char ch)
{
    if (ch >= '0' && ch <= '9') {
	return ch - '0';
    } else if (ch >= 'a' && ch <= 'f') {
	return 10 + ch - 'a';
    } else {
	return 10 + ch - 'A';
    }
}

static void
nextch()
{
    tokenLoc.end = pos;
    tokenValue_append(&tokenValue_, ch);

    if (!inputLine->cp || inputLine->cp == inputLine->end) {
	fillLineBuffer();
    }

    if (inputLine->cp == inputLine->end) {
	if (inputLine->eof) {
	    ch = EOF;
	    return;
	}
    }

    ch = *inputLine->cp++;

    if (ch == '\n') {
	pos.col = 1;
	++pos.line;
    } else if (ch == '\t') {
	pos.col += 8 - pos.col % 8;
    } else {
	++pos.col;
    }
}

void
parseCharacterLiteral()
{
    tokenValue_clear(&processedTokenValue_);
    // ch == '\''
    nextch();
    if (ch == '\'') {
	lexError(SINGLE_QUOTE_AS_CHARACTER_LITERAL);
    }
    do {
	if (ch == '\n') {
	    lexError(NEWLINE_AS_CHARACTER_LITERAL);
	} else if (ch == '\\') {
	    nextch();
	    if (is_octaldigit(ch)) {
		unsigned octalval = ch - '0';
		nextch();
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		tokenValue_append(&processedTokenValue_, octalval);
	    } else {
		switch (ch) {
		    /* simple-escape-sequence */
		    case '\'':
			tokenValue_append(&processedTokenValue_, '\'');
			nextch();
			break;
		    case '"':
			tokenValue_append(&processedTokenValue_, '\"');
			nextch();
			break;
		    case '?':
			tokenValue_append(&processedTokenValue_, '\?');
			nextch();
			break;
		    case '\\':
			tokenValue_append(&processedTokenValue_, '\\');
			nextch();
			break;
		    case 'a':
			tokenValue_append(&processedTokenValue_, '\a');
			nextch();
			break;
		    case 'b':
			tokenValue_append(&processedTokenValue_, '\b');
			nextch();
			break;
		    case 'f':
			tokenValue_append(&processedTokenValue_, '\f');
			nextch();
			break;
		    case 'n':
			tokenValue_append(&processedTokenValue_, '\n');
			nextch();
			break;
		    case 'r':
			tokenValue_append(&processedTokenValue_, '\r');
			nextch();
			break;
		    case 't':
			tokenValue_append(&processedTokenValue_, '\t');
			nextch();
			break;
		    case 'v':
			tokenValue_append(&processedTokenValue_, '\v');
			nextch();
			break;
		    case 'x': {
			nextch();
			if (!is_hexdigit(ch)) {
			    lexError(EXPECTED_HEXDIGIT);
			}
			unsigned hexval = hex_to_val(ch);
			nextch();
			while (is_hexdigit(ch)) {
			    hexval = hexval * 16 + hex_to_val(ch);
			    nextch();
			}
			tokenValue_append(&processedTokenValue_, hexval);
			break;
		    }
		    default:
			lexError(INVALID_CHARACTER_LITERAL);
		}
	    }
	} else if (ch == EOF) {
	    lexError(EOF_IN_CHARACTER_LITERAL);
	} else {
	    tokenValue_append(&processedTokenValue_, ch);
	    nextch();
	}
    } while (ch != '\'');
    nextch();
    processedTokenValue = processedTokenValue_.buf;
}

void
parseStringLiteral()
{
    tokenValue_clear(&processedTokenValue_);
    // ch == '"'
    nextch();
    while (ch != '"') {
	if (ch == '\n') {
	    lexError(NEWLINE_IN_STRING_LITERAL);
	} else if (ch == '\\') {
	    nextch();
	    if (is_octaldigit(ch)) {
		unsigned octalval = ch - '0';
		nextch();
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		ch = octalval;
		tokenValue_append(&processedTokenValue_, ch);
	    } else {
		switch (ch) {
		    /* simple-escape-sequence */
		    case '\'':
			tokenValue_append(&processedTokenValue_, '\'');
			nextch();
			break;
		    case '"':
			tokenValue_append(&processedTokenValue_, '\"');
			nextch();
			break;
		    case '?':
			tokenValue_append(&processedTokenValue_, '\?');
			nextch();
			break;
		    case '\\':
			tokenValue_append(&processedTokenValue_, '\\');
			nextch();
			break;
		    case 'a':
			tokenValue_append(&processedTokenValue_, '\a');
			nextch();
			break;
		    case 'b':
			tokenValue_append(&processedTokenValue_, '\b');
			nextch();
			break;
		    case 'f':
			tokenValue_append(&processedTokenValue_, '\f');
			nextch();
			break;
		    case 'n':
			tokenValue_append(&processedTokenValue_, '\n');
			nextch();
			break;
		    case 'r':
			tokenValue_append(&processedTokenValue_, '\r');
			nextch();
			break;
		    case 't':
			tokenValue_append(&processedTokenValue_, '\t');
			nextch();
			break;
		    case 'v':
			tokenValue_append(&processedTokenValue_, '\v');
			nextch();
			break;
		    case 'x': {
			nextch();
			if (!is_hexdigit(ch)) {
			    lexError(EXPECTED_HEXDIGIT);
			}
			unsigned hexval = hex_to_val(ch);
			nextch();
			while (is_hexdigit(ch)) {
			    hexval = hexval * 16 + hex_to_val(ch);
			    nextch();
			}
			tokenValue_append(&processedTokenValue_, hexval);
			break;
		    }
		    default:
			lexError(INVALID_CHARACTER_LITERAL);
		}
	    }
	} else if (ch == EOF) {
	    lexError(EOF_IN_STRING_LITERAL);
	} else {
	    tokenValue_append(&processedTokenValue_, ch);
	    nextch();
	}
    }
    nextch();
    processedTokenValue = processedTokenValue_.buf;
}

static void
processIncludeDirective()
{
    tokenLoc.begin = tokenLoc.end;
    tokenValue_disable(&tokenValue_);
    while (is_space(ch) || ch == 0) {
	nextch();
    }
    bool curDir = ch == '"';
    if (ch != '"' && ch != '<') {
	errorMsg(&tokenLoc, "expected '\"' or '<'");
	stop(1);
    }
    nextch();
    tokenValue_clear(&tokenValue_);
    tokenValue = tokenValue_.buf;
    tokenLoc.begin = tokenLoc.end;
    while (is_letter(ch) || is_digit(ch) || ch == '.' || ch == '/') {
	nextch();
    }
    const char* file = addString(tokenValue);
    if (!curDir) {
	bool found = false;
	for (size_t i = 0; i < sizeof(includePath) / sizeof(includePath[0]);
	     ++i)
	{
	    const char* path = addString(includePath[i]);
	    path = stringCat(path, file);

	    FILE* test = fopen(path, "r");
	    if (test) {
		fclose(test);
		file = path;
		found = true;
		break;
	    }
	}
	if (! found) {
	    errorMsg(0, "'%s' not found in any include path\n", file);
	    fprintf(stderr, "list of include paths:\n");
	    for (size_t i = 0; i < sizeof(includePath) / sizeof(includePath[0]);
		 ++i)
	    {
		fprintf(stderr, "  '%s'\n", includePath[i]);
	    }
	    stop(1);
	}
    }
    if (curDir && ch != '"') {
	errorMsg(&tokenLoc, "expected '\"'");
	stop(1);
    } else if (!curDir && ch != '>') {
	errorMsg(&tokenLoc, "expected '>'");
	stop(1);
    }
    nextch();
    tokenValue_disable(&tokenValue_);
    tokenValue_clear(&tokenValue_);

    FILE* in = fopen(file, "r");
    if (!in) {
	fprintf(stderr, "%s: can not open for reading\n", file);
	stop(1);
    }
    setLexerIn(file, in);
}

static void
skipCppWhitespace()
{
    // skip whitespace
    tokenValue_disable(&tokenValue_);
    while ((is_space(ch) && ch != '\n') || ch == 0) {
	nextch();
	if (ch == '\\') {
	    nextch();
	    if (ch != '\n') {
		printf("ch = '%d'\n", ch);
		lexError(FOUND_BAD_TOKEN);
	    }
	}
    }
}

static bool skipToken();
static enum TokenKind getToken0();

static void
processDefineDirective()
{
    getToken0();
    if (tokenKind != IDENT) {
	errorMsg(&tokenLoc, "identifier for macro expected");
	stop(1);
    }
    if (!skipToken() && !defineMacro(tokenValue)) {
	errorMsg(&tokenLoc, "macro '%s' already defined", tokenValue);
	stop(1);
    }
    skipCppWhitespace();
    if (ch != '\n') {
	errorMsg(&tokenLoc, "macros with values not supported");
	stop(1);
    }
}

static void
processUndefDirective()
{
    getToken0();
    if (tokenKind != IDENT) {
	errorMsg(&tokenLoc, "identifier for macro expected");
	stop(1);
    }
    if (!skipToken() && !undefMacro(tokenValue)) {
	errorMsg(&tokenLoc, "macro '%s' not defined", tokenValue);
	stop(1);
    }
    skipCppWhitespace();
    if (ch != '\n') {
	errorMsg(&tokenLoc, "extra tokens at end of #undef directive");
	stop(1);
    }
}

static void processIfdefDirective(bool onCond);
static void processElseDirective();
static void processEndifDirective();

void
parseDirective()
{
    static bool first = true;

    static const char* dir_include;
    static const char* dir_ifndef;
    static const char* dir_ifdef;
    static const char* dir_endif;
    static const char* dir_else;
    static const char* dir_define;
    static const char* dir_undef;

    if (first) {
	first = false;
	dir_include = addString("#include");
	dir_ifndef = addString("#ifndef");
	dir_ifdef = addString("#ifdef");
	dir_endif = addString("#endif");
	dir_else = addString("#else");
	dir_define = addString("#define");
	dir_undef = addString("#undef");
    }

    if (is_letter(ch)) {
	do {
	    nextch();
	} while (is_letter(ch) || is_digit(ch));
	tokenValue = addString(tokenValue);

	if (tokenValue == dir_include) {
	    processIncludeDirective();
	} else if (tokenValue == dir_ifndef) {
	    processIfdefDirective(false);
	} else if (tokenValue == dir_ifdef) {
	    processIfdefDirective(true);
	} else if (tokenValue == dir_endif) {
	    processEndifDirective();
	} else if (tokenValue == dir_else) {
	    processElseDirective();
	} else if (tokenValue == dir_define) {
	    processDefineDirective();
	} else if (tokenValue == dir_undef) {
	    processUndefDirective();
	} else {
	    printf("found unkown or not supported directive '%s'\n",
		   tokenValue);
	    tokenValue_disable(&tokenValue_);
	    while (ch != '\n') {
		nextch();
	    }
	}
    } else {
	fprintf(stderr, "error: skipping directive\n");
	while (ch != EOF && ch != '\n') {
	    fprintf(stderr, "%c", ch);
	    nextch();
	}
	fprintf(stderr, "\n");
    }
}

static enum TokenKind
getToken_()
{
    if (!ch) {
	nextch();
	tokenLoc.begin = tokenLoc.end;
    }

    // skip whitespace
    tokenValue_disable(&tokenValue_);
    while (is_space(ch) || ch == 0) {
	nextch();
	if (ch == '\\') {
	    nextch();
	    if (ch != '\n') {
		printf("ch = '%d'\n", ch);
		lexError(FOUND_BAD_TOKEN);
		return 0;
	    }
	}
    }
    tokenValue_clear(&tokenValue_);
    tokenValue_clear(&processedTokenValue_);
    tokenLoc.begin = tokenLoc.end;

    // check for identifiers
    if (is_letter(ch)) {
	do {
	    nextch();
	} while (is_letter(ch) || is_digit(ch));
	tokenKind = IDENT;
	return tokenKind;
    }
    // check for numeral literals
    else if (is_digit(ch)) {
	if (ch == '0') {
	    // representation?
	    nextch();
	    if (ch == 'x' || ch == 'X') {
		tokenKind = HEXADECIMAL_LITERAL;
		nextch();
		if (!is_hexdigit(ch)) {
		    lexError(INVALID_HEXADECIMAL_LITERAL);
		}
		do {
		    nextch();
		} while (is_hexdigit(ch));
		if (ch == 'u') {
		    nextch();
		}
		hexLiteral();
	    } else {
		tokenKind = OCTAL_LITERAL;
		while (is_octaldigit(ch)) {
		    nextch();
		}
		if (ch == 'u') {
		    nextch();
		}
		octLiteral();
	    }
	} else {
	    tokenKind = DECIMAL_LITERAL;
	    do {
		nextch();
	    } while (is_digit(ch));
	    if (ch == 'u') {
		nextch();
	    }
	    decLiteral();
	}
	return tokenKind;
    }
    // check for character literals
    else if (ch == '\'') {
	parseCharacterLiteral();
	tokenKind = CHARACTER_LITERAL;
	return tokenKind;
    }
    // check for string literals
    else if (ch == '"') {
	parseStringLiteral();
	tokenKind = STRING_LITERAL;
	return tokenKind;
	// check for punctuators
    } else if (ch == '&') {
	nextch();
	tokenKind = AMPERSAND;
	if (ch == '=') {
	    nextch();
	    tokenKind = AMPERSAND_EQ;
	} else if (ch == '&') {
	    nextch();
	    tokenKind = AMPERSAND2;
	}
	return tokenKind;
    } else if (ch == '*') {
	nextch();
	tokenKind = ASTERISK;
	if (ch == '=') {
	    nextch();
	    tokenKind = ASTERISK_EQ;
	}
	return tokenKind;
    } else if (ch == ':') {
	nextch();
	tokenKind = COLON;
	return tokenKind;
    } else if (ch == ',') {
	nextch();
	tokenKind = COMMA;
	return tokenKind;
    } else if (ch == '.') {
	nextch();
	if (ch == '.') {
	    nextch();
	    if (ch == '.') {
		nextch();
		tokenKind = DOT3;
		return tokenKind;
	    } else {
		tokenKind = BAD_TOKEN;
		return tokenKind;
	    }
	}
	tokenKind = DOT;
	return tokenKind;
    } else if (ch == '=') {
	nextch();
	tokenKind = EQUAL;
	if (ch == '=') {
	    nextch();
	    tokenKind = EQUAL2;
	}
	return tokenKind;
    } else if (ch == '!') {
	nextch();
	tokenKind = EXCLAMATION;
	if (ch == '=') {
	    nextch();
	    tokenKind = NE;
	}
	return tokenKind;
    } else if (ch == '>') {
	nextch();
	tokenKind = GT;
	if (ch == '=') {
	    nextch();
	    tokenKind = GE;
	} else if (ch == '>') {
	    nextch();
	    tokenKind = GT2;
	    if (ch == '=') {
		nextch();
		tokenKind = GT2_EQ;
	    }
	}
	return tokenKind;
    } else if (ch == '{') {
	nextch();
	tokenKind = LBRACE;
	return tokenKind;
    } else if (ch == '[') {
	nextch();
	tokenKind = LBRACKET;
	return tokenKind;
    } else if (ch == '<') {
	nextch();
	tokenKind = LT;
	if (ch == '=') {
	    nextch();
	    tokenKind = LE;
	} else if (ch == '<') {
	    nextch();
	    tokenKind = LT2;
	    if (ch == '=') {
		nextch();
		tokenKind = LT2_EQ;
	    }
	}
	return tokenKind;
    } else if (ch == '(') {
	nextch();
	tokenKind = LPAREN;
	return tokenKind;
    } else if (ch == '-') {
	nextch();
	tokenKind = MINUS;
	if (ch == '-') {
	    nextch();
	    tokenKind = MINUS2;
	} else if (ch == '=') {
	    nextch();
	    tokenKind = MINUS_EQ;
	} else if (ch == '>') {
	    nextch();
	    tokenKind = ARROW;
	}
	return tokenKind;
    } else if (ch == '%') {
	nextch();
	tokenKind = PERCENT;
	if (ch == '=') {
	    nextch();
	    tokenKind = PERCENT_EQ;
	}
	return tokenKind;
    } else if (ch == '+') {
	nextch();
	tokenKind = PLUS;
	if (ch == '+') {
	    nextch();
	    tokenKind = PLUS2;
	} else if (ch == '=') {
	    nextch();
	    tokenKind = PLUS_EQ;
	}
	return tokenKind;
    } else if (ch == '?') {
	nextch();
	tokenKind = QMARK;
	return tokenKind;
    } else if (ch == '}') {
	nextch();
	tokenKind = RBRACE;
	return tokenKind;
    } else if (ch == ']') {
	nextch();
	tokenKind = RBRACKET;
	return tokenKind;
    } else if (ch == ')') {
	nextch();
	tokenKind = RPAREN;
	return tokenKind;
    } else if (ch == ';') {
	nextch();
	tokenKind = SEMICOLON;
	return tokenKind;
    } else if (ch == '^') {
	nextch();
	tokenKind = CARET;
	if (ch == '=') {
	    nextch();
	    tokenKind = CARET_EQ;
	}
	return tokenKind;
    } else if (ch == '~') {
	nextch();
	tokenKind = TILDE;
	if (ch == '=') {
	    nextch();
	    tokenKind = TILDE_EQ;
	}
	return tokenKind;
    } else if (ch == '|') {
	nextch();
	tokenKind = VBAR;
	if (ch == '=') {
	    nextch();
	    tokenKind = VBAR_EQ;
	} else if (ch == '|') {
	    nextch();
	    tokenKind = VBAR2;
	}
	return tokenKind;
    } else if (ch == '#') {
	nextch();
	parseDirective();
	return getToken0();
    } else if (ch == '/') {
	nextch();
	tokenKind = SLASH;
	if (ch == '=') {
	    nextch();
	    tokenKind = SLASH_EQ;
	} else if (ch == '/') {
	    // ignore single line comment
	    tokenValue_disable(&tokenValue_);
	    do {
		nextch();
	    } while (ch != EOF && ch != '\n');
	    return getToken_();
	} else if (ch == '*') {
	    tokenValue_disable(&tokenValue_);
	    nextch();
	    bool star = false;
	    while (ch != EOF && (!star || ch != '/')) {
		star = ch == '*';
		nextch();
	    }
	    if (ch == EOF) {
		lexError(EOF_IN_DELIMITED_COMMENT);
	    }
	    nextch();
	    return getToken_();
	}
	return tokenKind;
    }
    // if we get here no legal token was found or end of file was reached
    else if (ch != EOF) {
	nextch();
	tokenKind = EOI;
	lexError(FOUND_BAD_TOKEN);
	return tokenKind;
    }
    tokenKind = EOI;
    return tokenKind;
}

static enum TokenKind
getToken0()
{
    tokenValue = tokenValue_.buf;
    getToken_();

    if (tokenKind == IDENT) {
	tokenValue = addString(tokenValue);
    }

    if (tokenKind != EOI) {
	tokenFixIdentToKeyword();
    }

    // error on unsupported keywords
    switch (tokenKind) {
	case REGISTER:
	case VOLATILE:
	    lexError(UNSUPPORTED);
	    stop(1);
	    break;

	default:;
    }
    return tokenKind;
}

enum TokenKind
getToken()
{
    do {
	getToken0();
    } while (skipToken());
    return tokenKind;
}

void
setLexerIn(const char* filename, FILE* in)
{
    if (in_) {
	struct OpenLexFile* item = alloc(LEX, sizeof(*item));
	item->next = openLexFile;
	item->in = in_;
	item->filename = currentFilename;
	item->pos = pos;
	openLexFile = item;
    }
    currentFilename = addString(filename);
    in_ = in;
}

static bool
resumeLexerIn()
{
    if (!openLexFile) {
	return false;
    }
    fclose(in_);
    pos = openLexFile->pos;
    currentFilename = openLexFile->filename;
    in_ = openLexFile->in;
    openLexFile = openLexFile->next;
    return true;
}

struct Macro
{
    const char* ident;
    struct Macro* next;
};

static struct Macro* macro;

bool
defineMacro(const char* ident)
{
    if (is_defined(ident)) {
	return false;
    }

    struct Macro* item = alloc(LEX, sizeof(*item));
    item->ident = ident;
    item->next = macro;
    macro = item;
    return true;
}

bool
undefMacro(const char* ident)
{
    for (struct Macro** item = &macro; *item; item = &(*item)->next) {
	if ((*item)->ident == ident) {
	    *item = (*item)->next;
	    return true;
	}
    }
    return false;
}

bool
is_defined(const char* ident)
{
    for (const struct Macro* item = macro; item; item = item->next) {
	if (item->ident == ident) {
	    return true;
	}
    }
    return false;
}

struct IfClause
{
    bool is_active;
    bool is_true;
    bool elseProcessed;
    struct IfClause* next;
};

static struct IfClause* ifClause;

static bool
skipToken()
{
    if (!ifClause) {
	return false;
    }
    if (ifClause->is_active && ifClause->is_true) {
	return false;
    }
    return true;
}

static void
pushIfClause(bool is_true)
{
    struct IfClause* item = alloc(LEX, sizeof(*item));
    item->is_true = is_true;
    item->elseProcessed = false;
    item->is_active = !ifClause || ifClause->is_true;
    item->next = ifClause;
    ifClause = item;
}

static bool
popIfClause()
{
    if (!ifClause) {
	return false;
    }
    ifClause = ifClause->next;
    return true;
}

static void
processIfdefDirective(bool onCond)
{
    getToken0();
    if (tokenKind != IDENT) {
	errorMsg(&tokenLoc, "identifier for macro expected");
	stop(1);
    }
    bool cond = is_defined(tokenValue);
    pushIfClause(onCond ? cond : !cond);
}

static void
processElseDirective()
{
    if (!ifClause) {
	errorMsg(&tokenLoc, "#else without #ifdef or #ifndef");
	stop(1);
    }
    if (ifClause->elseProcessed) {
	errorMsg(&tokenLoc, "#else after #else");
	stop(1);
    }
    ifClause->elseProcessed = true;
    ifClause->is_true = !ifClause->is_true;
}

static void
processEndifDirective()
{
    if (!popIfClause()) {
	errorMsg(&tokenLoc, "#endif without #ifdef or #ifndef");
	stop(1);
    }
}
