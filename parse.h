#ifndef ULMCC_PARSE_H
#define ULMCC_PARSE_H

#include <stdbool.h>
#include <stdio.h>

#include "codegen.h"

bool parse(const char *filename, FILE *in);
bool expected(enum TokenKind t);
const struct Type *parseAbstractDeclaration();
bool foundTypeToken();

#endif // ULMCC_PARSE_H
