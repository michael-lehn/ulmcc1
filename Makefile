CC = gcc
CFLAGS = -Wall -Wextra -pedantic

TARGETS_SRC = $(wildcard xtest_*.c) ulmcc1.c
TARGETS_OBJ = $(patsubst %.c,%.o,$(TARGETS_SRC))
TARGETS     = $(patsubst %.c,%,$(TARGETS_SRC))

SOURCE_FILES = $(filter-out $(TARGETS_SRC), $(wildcard *.c))
OBJECT_FILES = $(patsubst %.c,%.o,$(SOURCE_FILES))

SOURCES = $(TARGETS_SRC) $(SOURCE_FILES)
DEPEND_FILES = $(patsubst %.c,%.d,$(SOURCES))


.PHONY: all clean
all: $(TARGETS)

clean:
	$(RM) $(DEPEND_FILES)
	$(RM) $(OBJECT_FILES) $(TARGETS_OBJ)
	$(RM) $(TARGETS)

$(TARGETS): % : %.o $(OBJECT_FILES)
	$(CC) $(CFLAGS) -o $@ $^

%.d: %.c | $(DEPS_DIR)
	@echo "Updating dependencies for $<"
	@set -e; $(RM) -f $@; \
	$(CC) -M $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	$(RM) -f $@.$$$$

ifneq ($(MAKECMDGOALS), clean)
-include $(patsubst %,%,$(SOURCES:.c=.d))
endif



