#include <inttypes.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "instr_ulm.h"

size_t sizeOfShort = 2;
size_t sizeOfInt = 2;
size_t sizeOfLong = 4;
size_t sizeOfLongLong = 8;

size_t sizeOfSizeT = 8;

static Reg regUsed[256], regGroup[256];
static size_t numRegsUsed;

enum {
    ZERO_ID = 0,
    FP_ID = 1,
    SP_ID = 2,
    RET_ID = 3,
};

Reg ZERO = ZERO_ID;
Reg FP = FP_ID;
Reg SP = SP_ID;
Reg RET = RET_ID;

static FILE *out;

static void printInstr(const char *instr);
static void printInstrEnd();
static void printSymArg(const char *sym, ptrdiff_t offset);
static void printSymWordOpArg(const char *sym, int w, ptrdiff_t offset);
static void printSImmArg(int64_t imm);
static void printUImmArg(uint64_t imm);

void
setOut(FILE *out_)
{
    out = out_;

    printInstr(".equ");
    printSymArg("ZERO", 0);
    printUImmArg(0);
    printInstrEnd();
    
    printInstr(".equ");
    printSymArg("FP", 0);
    printUImmArg(1);
    printInstrEnd();
    
    printInstr(".equ");
    printSymArg("SP", 0);
    printUImmArg(2);
    printInstrEnd();
    
    printInstr(".equ");
    printSymArg("RET", 0);
    printUImmArg(3);
    printInstrEnd();

    printInstrEnd();
}

static Reg
allocateReg(size_t size)
{
    bool found;
    for (Reg i = RET + 1; i < 256 - size; ++i) {
        found = true;
        for (size_t n = 0; n < size; ++n) {
            if (regUsed[i + n]) {
                found = false;
                break;
            }
        }
        if (found) {
            for (size_t n = 0; n < size; ++n) {
                regUsed[i + n] = size - n;
		regGroup[i + n] = i;
            }
	    numRegsUsed += size;
            return i;
        }
    }
    fprintf(stderr, "internal error: out of free registers\n");
    exit(2);
    return 0;
}

static Reg
reallocateRegId(Reg id)
{
    if (regUsed[id]) {
	fprintf(stderr, "internal error: reallocating used register\n");
	exit(2);
    }
    regUsed[id] = 1;
    regGroup[id] = id;
    ++numRegsUsed;
    return id;
}

void
releaseReg(Reg regId)
{
    if (regId <= RET) {
	return;
    }
    regId = regGroup[regId];
    if (! regUsed[regId]) {
	fprintf(stderr, "internal error: Releasing unused register '%zu'\n",
		regId);
	exit(2);
    }
    for (size_t n = regUsed[regId]; n; --n) {
	regUsed[regId + n - 1] = 0;
	if (! numRegsUsed) {
	    fprintf(stderr, "internal error: 'releaseReg'\n");
	    exit(2);
	}
	--numRegsUsed;
    }
}

size_t
requiredRegSaveSpace()
{
    return numRegsUsed * 8;
}

void
saveUsedRegs(size_t offset)
{
    for (Reg i = RET + 1; i < 256; ++i) {
	if (regUsed[i]) {
	    fprintf(out, "\t# save register %%%zu\n", i);
	    store(8, i, offset, SP, ZERO, 1);
	    offset += 8;
	}
    }
}

void
restoreUsedRegs(size_t offset)
{
    for (Reg i = RET + 1; i < 256; ++i) {
	if (regUsed[i]) {
	    fprintf(out, "\t# restore register %%%zu\n", i);
	    fetch(8, false, offset, SP, ZERO, 1, &i);
	    offset += 8;
	}
    }
}

static size_t argPos;

void
printDirective(enum Directive directive, ...)
{
    va_list argp;
    va_start(argp, directive);
    switch (directive) {
	case ALIGN:
	    fprintf(out, "\t.align \t%zu\n", va_arg(argp, size_t));
	    break;
	case BSS:
	    printInstrEnd();
	    printInstr(".bss");
	    printInstrEnd();
	    break;
	case DATA:
	    printInstrEnd();
	    printInstr(".data");
	    printInstrEnd();
	    break;
	case GLOBL:
	    fprintf(out, "\t.globl \t%s\n", va_arg(argp, char *));
	    break;
	case SPACE:
	    fprintf(out, "\t.space \t%zu\n", va_arg(argp, size_t));
	    break;
	case D_STRING:
	    fprintf(out, "\t.string \t%s\n", va_arg(argp, char *));
	    break;

	case TEXT:
	    printInstrEnd();
	    printInstr(".text");
	    printInstrEnd();
	    break;
	case D_BYTE:
	    fprintf(out, "\t.byte \t%s\n", va_arg(argp, char *));
	    break;
	case D_WORD:
	    fprintf(out, "\t.word \t%s\n", va_arg(argp, char *));
	    break;
	case D_LONG:
	    fprintf(out, "\t.long \t%s\n", va_arg(argp, char *));
	    break;
	case D_QUAD:
	    fprintf(out, "\t.quad \t%s\n", va_arg(argp, char *));
	    break;
    }
    va_end(argp);
}

static void
vprintComment(const char *fmt, va_list argp)
{
    vfprintf(out, fmt, argp);
}

void
printComment(const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vprintComment(fmt, argp);
    va_end(argp);
}

void
printLabel(const char *name)
{
    fprintf(out, "%s:\n", name);
}

static void
printInstr(const char *instr)
{
    fprintf(out, "\t%s", instr);
}

static void
printFetchInstr(size_t numBytes, bool is_signed)
{
    switch (numBytes) {
	case 1:
	    is_signed ? printInstr("movsbq") : printInstr("movzbq");
	    break;
	case 2:
	    is_signed ? printInstr("movswq") : printInstr("movzwq");
	    break;
	case 4:
	    is_signed ? printInstr("movslq") : printInstr("movzlq");
	    break;
	case 8:
	    printInstr("movq");
	    break;
	default:
	    fprintf(stderr, "internal error: 'printFetchInstr'\n");
	    fprintf(stderr, "numBytes = %zu\n", numBytes);
	    exit(2);
    }
}

static void
printStoreInstr(size_t numBytes)
{
    switch (numBytes) {
	case 1:
	    printInstr("movb");
	    break;
	case 2:
	    printInstr("movw");
	    break;
	case 4:
	    printInstr("movl");
	    break;
	case 8:
	    printInstr("movq");
	    break;
	default:
	    printInstr("movXXX");
	    fprintf(stderr, "internal error: 'printStoreInstr'"
		    " numBytes = %zu\n", numBytes);
	    //exit(2);
    }
}

static void
printReg(Reg reg)
{
    if (reg == FP) {
	fprintf(out, "%%FP");
    } else if (reg == SP) {
	fprintf(out, "%%SP");
    } else if (reg == RET) {
	fprintf(out, "%%RET");
    } else {
	fprintf(out, "%%%zu", reg);
    }
}

static void
printMemArg(ptrdiff_t offset, Reg base, Reg index, size_t scale)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    fprintf(out, "\t ");
    if (offset) {
	fprintf(out, "%td", offset);
    }
    fprintf(out, "(");
    printReg(base);
    if (index) {
	fprintf(out, ",\t ");
	printReg(index);
    }
    if (scale > 1) {
	fprintf(out, ",\t %zu", scale);
    }
    fprintf(out, ")");
}

static void
printSymArg(const char *sym, ptrdiff_t offset)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    if (sym) {
	if (offset) {
	    if (offset >= 0) {
		fprintf(out, "\t %s + %td", sym, offset);
	    } else {
		fprintf(out, "\t %s - %td", sym, -offset);
	    }
	} else {
	    fprintf(out, "\t %s", sym);
	}
    } else {
	fprintf(out, "\t %td", offset);
    }
}

static void
printSymWordOpArg(const char *sym, int w, ptrdiff_t offset)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    if (sym) {
	if (offset) {
	    if (offset >= 0) {
		fprintf(out, "\t @w%d(%s + %td)", w, sym, offset);
	    } else {
		fprintf(out, "\t @w%d(%s - %td)", w, sym, -offset);
	    }
	} else {
	    fprintf(out, "\t @w%d(%s)", w, sym);
	}
    } else {
	fprintf(out, "\t  @w%d(%td)", w, offset);
    }
}

static void
printSImmArg(int64_t imm)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    fprintf(out, "\t %" PRId64, imm);
}

static void
printUImmArg(uint64_t imm)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    fprintf(out, "\t %" PRIu64, imm);
}

static void
printRegArg(Reg reg)
{
    if (argPos) {
	fprintf(out, ",");
    }
    ++argPos;
    fprintf(out, "\t ");
    printReg(reg);
}

static void
printInstrEnd()
{
    argPos = 0;
    fprintf(out, "\n");
}

Reg
loadAddress(const char *name, ptrdiff_t offset, Reg *dest_)
{
    Reg dest = dest_ ? *dest_ : allocateReg(1);
    if (name) {
	printInstr("ldzwq");
	printSymWordOpArg(name, 3, offset);
	printRegArg(dest);
	printInstrEnd();
	printInstr("shldwq");
	printSymWordOpArg(name, 2, offset);
	printRegArg(dest);
	printInstrEnd();
	printInstr("shldwq");
	printSymWordOpArg(name, 1, offset);
	printRegArg(dest);
	printInstrEnd();
	printInstr("shldwq");
	printSymWordOpArg(name, 0, offset);
	printRegArg(dest);
	printInstrEnd();
    } else {
	dest = srrInstr(ADD_INSTR, offset, FP, &dest);
    }
    return dest;
}

Reg
loadSignedLiteral(size_t size, int64_t imm, Reg *dest_)
{
    if (imm >= 0) {
	return loadUnsignedLiteral(size, imm, dest_);
    }
    Reg dest = dest_ ? *dest_ : allocateReg(1);


    if (size > 2) {
	loadSignedLiteral(size - 2, -((-imm + 0x10000 - 1) / 0x10000), &dest);
	printInstr("shldwq");
	printSImmArg(imm & 0xFFFF);
	printRegArg(dest);
	printInstrEnd();
	return dest;
    }

    printInstr("ldswq");
    printSImmArg(imm);
    printRegArg(dest);
    printInstrEnd();

    return dest;
}

Reg
loadUnsignedLiteral(size_t size, uint64_t imm, Reg *dest_)
{
    Reg dest = dest_ ? *dest_ : allocateReg(1);

    if (size > 2) {
	loadUnsignedLiteral(size - 2, imm >> 16, &dest);
	printInstr("shldwq");
	printUImmArg(imm & 0xFFFF);
	printRegArg(dest);
	printInstrEnd();
	return dest;
    }
    printInstr("ldzwq");
    printUImmArg(imm);
    printRegArg(dest);
    printInstrEnd();

    return dest;
}

Reg
loadReg(Reg src, Reg *dest_)
{
    Reg dest = dest_ ? *dest_ : allocateReg(1);

    if (dest == src) {
	return dest;
    }

    printInstr("addq");
    printUImmArg(0);
    printRegArg(src);
    printRegArg(dest);
    printInstrEnd();

    return dest;
}

Reg
fetch(size_t numBytes, bool is_signed, ptrdiff_t offset, Reg addr,
      Reg index, size_t scale, Reg *dest_)
{
    if (! numBytes) {
	printf("internal error: 'fetch' called with numBytes = 0\n");
	return 0;
    }
    Reg dest = dest_ ? *dest_ : allocateReg(1);

    bool allocatedAddr = false;
    if (offset >= 128 || (offset > 0 && index)) {
	addr = srrInstr(ADD_INSTR, offset, addr, 0);
	offset = 0;
	allocatedAddr = true;
    } else if (offset < -128 || (offset < 0 && index)) {
	addr = srrInstr(SUB_INSTR, offset, addr, 0);
	offset = 0;
	allocatedAddr = true;
    }
    printFetchInstr(numBytes, is_signed);
    printMemArg(offset, addr, index, scale);
    printRegArg(dest);
    printInstrEnd();
    if (allocatedAddr) {
	releaseReg(addr);
    }

    return dest;
}

void
store(size_t numBytes, Reg src, ptrdiff_t offset, Reg addr, Reg index,
      size_t scale)
{
    if (! numBytes) {
	return;
    }
    bool allocatedAddr = false;
    if (offset >= 128 || (offset > 0 && index)) {
	addr = srrInstr(ADD_INSTR, offset, addr, 0);
	offset = 0;
	allocatedAddr = true;
    } else if (offset < -128 || (offset < 0 && index)) {
	addr = srrInstr(SUB_INSTR, offset, addr, 0);
	offset = 0;
	allocatedAddr = true;
    }
    printStoreInstr(numBytes);
    printRegArg(src);
    printMemArg(offset, addr, index, scale);
    printInstrEnd();
    if (allocatedAddr) {
	releaseReg(addr);
    }
}

Reg
fetchValue(size_t numBytes, bool is_signed, struct MemLocation memLoc,
	   Reg *dest_)
{
    Reg addr = memLoc.name ? loadAddress(memLoc.name, 0, 0) : FP;
    releaseReg(addr);
    Reg dest = fetch(numBytes, is_signed, memLoc.offset, addr, 0, 1, dest_);
    return dest;
}

void
storeValue(Reg src, size_t numBytes, struct MemLocation memLoc)
{
    size_t dest = memLoc.name ? loadAddress(memLoc.name, 0, 0) : FP;
    store(numBytes, src, memLoc.offset, dest, 0, 1);
    releaseReg(dest);
}

Reg
fetchAddressedValue(size_t numBytes, bool is_signed, Reg addr, Reg *dest_)
{
    return fetch(numBytes, is_signed, 0, addr, ZERO, 1, dest_);
}

void
storeAddressedValue(Reg src, size_t numBytes, Reg addr)
{
    store(numBytes, src, 0, addr, ZERO, 1);
}


void
callInstr(Reg call)
{
    printInstr("jmp");
    printRegArg(call);
    printRegArg(RET);
    printInstrEnd();
}

void
returnInstr()
{
    printInstr("jmp");
    printRegArg(RET);
    printRegArg(ZERO);
    printInstrEnd();
}

void
jmpToLabel(const char *label)
{
    printInstr("jmp");
    printSymArg(label, 0);
    printInstrEnd();
}

void
condJmpToLabel(enum InstrOp op, const char *label)
{
    switch (op) {
	case JZ_INSTR:
	    printInstr("jz");
	    break;
	case JNZ_INSTR:
	    printInstr("jnz");
	    break;
	case GE_SI_INSTR:
	    printInstr("jge");
	    break;
	case GE_UI_INSTR:
	    printInstr("jae");
	    break;
	case GT_SI_INSTR:
	    printInstr("jg");
	    break;
	case GT_UI_INSTR:
	    printInstr("ja");
	    break;
	case LE_SI_INSTR:
	    printInstr("jle");
	    break;
	case LE_UI_INSTR:
	    printInstr("jbe");
	    break;
	case LT_SI_INSTR:
	    printInstr("jl");
	    break;
	case LT_UI_INSTR:
	    printInstr("jb");
	    break;
	default:
	    fprintf(stderr, "internal error: 'condJmpToLabel'\n");
	    exit(2);
    }
    printSymArg(label, 0);
    printInstrEnd();
}

Reg
urrInstr(enum InstrOp op, uint64_t imm0, Reg reg1, Reg *reg2_)
{
    if (imm0 > 255) {
	size_t size = makeUnsignedIntegerTypeFromLiteral(imm0)->size;
	Reg tmp = loadUnsignedLiteral(size, imm0, 0);
	Reg reg2 = rrrInstr(op, tmp, reg1, reg2_);
	releaseReg(tmp);
	return reg2;
    }

    if (op == DIV_UI_INSTR || op == MOD_UI_INSTR) {
	size_t reg1_ = allocateReg(2) + 1;
	loadUnsignedLiteral(size_int, 0, &reg1_);
	--reg1_;
	loadReg(reg1, &reg1_);

	size_t tmp = allocateReg(3);
	printInstr("divq");
	printUImmArg(imm0);
	printRegArg(reg1_);
	printRegArg(tmp);
	printInstrEnd();

	releaseReg(reg1_);
	releaseReg(tmp);
	if (op == DIV_UI_INSTR) {
	    if (! reg2_) {
		return reallocateRegId(tmp);
	    }
	    return loadReg(tmp, reg2_);
	} else {
	    if (! reg2_) {
		return reallocateRegId(tmp + 2);
	    }
	    return loadReg(tmp + 2, reg2_);
	}
    }

    switch (op) {
	case ADD_INSTR:
	    printInstr("addq");
	    break;

	case SUB_INSTR:
	    printInstr("subq");
	    break;

	case MUL_INSTR:
	    return srrInstr(MUL_INSTR, imm0, reg1, reg2_);

	default:
	    fprintf(stderr, "internal error: 'urrInstr'\n");
	    exit(2);
    }

    Reg reg2 = reg2_ ? *reg2_ : allocateReg(1);
    printUImmArg(imm0);
    printRegArg(reg1);
    printRegArg(reg2);
    printInstrEnd();
    return reg2;
}

Reg
srrInstr(enum InstrOp op, int64_t imm0, Reg reg1, Reg *reg2_)
{
    if (imm0 > 127 || imm0 < -128) {
	size_t size = makeSignedIntegerTypeFromLiteral(imm0)->size;
	Reg tmp = loadSignedLiteral(size, imm0, 0);
	Reg reg2 = rrrInstr(op, tmp, reg1, reg2_);
	releaseReg(tmp);
	return reg2;
    }
    if (op == DIV_SI_INSTR || op == MOD_SI_INSTR) {
	size_t tmp = allocateReg(2);

	printInstr("idivq");
	printUImmArg(imm0);
	printRegArg(reg1);
	printRegArg(tmp);
	printInstrEnd();

	releaseReg(tmp);
	if (op == DIV_SI_INSTR) {
	    if (! reg2_) {
		return reallocateRegId(tmp);
	    }
	    return loadReg(tmp, reg2_);
	} else {
	    if (! reg2_) {
		return reallocateRegId(tmp + 1);
	    }
	    return loadReg(tmp + 1, reg2_);
	}
    }

    switch (op) {
	case ADD_INSTR:
	    if (imm0 < 0) {
		return urrInstr(SUB_INSTR, -imm0, reg1, reg2_);
	    }
	    return urrInstr(ADD_INSTR, imm0, reg1, reg2_);

	case SUB_INSTR:
	    if (imm0 < 0) {
		return urrInstr(ADD_INSTR, -imm0, reg1, reg2_);
	    }
	    return urrInstr(SUB_INSTR, imm0, reg1, reg2_);

	case MUL_INSTR:
	    printInstr("imulq");
	    break;

	default:
	    fprintf(stderr, "internal error: 'srrInstr'\n");
	    exit(2);
    }

    Reg reg2 = reg2_ ? *reg2_ : allocateReg(1);
    printSImmArg(imm0);
    printRegArg(reg1);
    printRegArg(reg2);
    printInstrEnd();
    return reg2;
}

Reg
rrrInstr(enum InstrOp op, Reg reg0, Reg reg1, Reg *reg2_)
{
    if (op == DIV_SI_INSTR || op == MOD_SI_INSTR) {
	size_t tmp = allocateReg(2);

	printInstr("idivq");
	printRegArg(reg0);
	printRegArg(reg1);
	printRegArg(tmp);
	printInstrEnd();

	releaseReg(tmp);
	if (op == DIV_SI_INSTR) {
	    if (! reg2_) {
		return reallocateRegId(tmp);
	    }
	    return loadReg(tmp, reg2_);
	} else {
	    if (! reg2_) {
		return reallocateRegId(tmp + 1);
	    }
	    return loadReg(tmp + 1, reg2_);
	}
    } else if (op == DIV_UI_INSTR || op == MOD_UI_INSTR) {
	size_t reg1_ = allocateReg(2) + 1;
	loadUnsignedLiteral(size_int, 0, &reg1_);
	--reg1_;
	loadReg(reg1, &reg1_);

	size_t tmp = allocateReg(3);
	printInstr("divq");
	printRegArg(reg0);
	printRegArg(reg1_);
	printRegArg(tmp);
	printInstrEnd();

	releaseReg(reg1_);
	releaseReg(tmp);
	if (op == DIV_UI_INSTR) {
	    if (! reg2_) {
		return reallocateRegId(tmp);
	    }
	    return loadReg(tmp, reg2_);
	} else {
	    if (! reg2_) {
		return reallocateRegId(tmp + 2);
	    }
	    return loadReg(tmp + 2, reg2_);
	}
    } else if (op == BITWISE_XOR_INSTR) {
	Reg tmp0 = rrInstr(BITWISE_NOT_INSTR, reg0, 0);
	Reg tmp1 = rrInstr(BITWISE_NOT_INSTR, reg1, 0);
	tmp1 = rrrInstr(BITWISE_AND_INSTR, reg0, tmp1, &tmp1);
	tmp0 = rrrInstr(BITWISE_AND_INSTR, reg1, tmp0, &tmp0);
	tmp0 = rrrInstr(BITWISE_OR_INSTR, tmp0, tmp1, &tmp0);
	releaseReg(tmp1);
	return tmp0;
    }

    switch (op) {
	case ADD_INSTR:
	    printInstr("addq");
	    break;

	case BITWISE_OR_INSTR:
	    printInstr("orq");
	    break;

	case BITWISE_AND_INSTR:
	    printInstr("andq");
	    break;

	case SUB_INSTR:
	    printInstr("subq");
	    break;

	case MUL_INSTR:
	    printInstr("imulq");
	    break;

	case SHL_UI_INSTR:
	case SHL_SI_INSTR:
	    printInstr("shlq");
	    break;

	case SHR_UI_INSTR:
	    printInstr("shrq");
	    break;

	case SHR_SI_INSTR:
	    printInstr("sarq");
	    break;

	default:
	    fprintf(stderr, "internal error: 'rrrInstr'\n");
	    fprintf(stderr, "op = %d\n", op);
	    exit(2);
    }

    size_t reg2 = reg2_ ? *reg2_ : allocateReg(1);
    printRegArg(reg0);
    printRegArg(reg1);
    printRegArg(reg2);
    printInstrEnd();
    return reg2;
}

Reg
rrInstr(enum InstrOp op, Reg reg0, Reg *reg1_)
{
    switch (op) {
	case BITWISE_NOT_INSTR:
	    printInstr("notq");
	    break;

	default:
	    fprintf(stderr, "internal error: 'rrInstr'\n");
	    exit(2);
    }

    size_t reg1 = reg1_ ? *reg1_ : allocateReg(1);
    printRegArg(reg0);
    printRegArg(reg1);
    printInstrEnd();
    return reg1;
}
