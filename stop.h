#ifndef ULMCC_STOP_H
#define ULMCC_STOP_H

#include <stdlib.h>
#include <stdio.h>

void deleteOutputOnStop(FILE *out, const char *outputfile);

void stop(int exitCode);

#endif // ULMCC_STOP_H
