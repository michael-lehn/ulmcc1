#ifndef ULMCC_STRING_H
#define ULMCC_STRING_H

#include <stddef.h>

/**
  * Checks if string str is already stored in a string pool. If not, it gets
  * added (the length of the string and its content are stored internally).
  * Returns a pointer to the internally stored string.
  *
  * Hence, for comparing stored strings you don't have to use strcmp, just
  * compare pointers.
  **/
const char *addString(const char *str);

const char *makeLabel(const char *prefix);

/**
  * Returns the length of a string.
  */
size_t getStringLen(const char *str);

/**
  * Returns the length of a string.
  */
const char *stringCat(const char *str1, const char *str2);

/**
  * extract path from file path. Always ends with '/'. E.g.
  * - file path "/usr/local/include/stdio.h" gives "/usr/local/include/"
  * - file path "stdio.h" gives "./"
  * - file path "../inlcude/stdio.h" gives "../include/"
  */
const char *path(const char *filepath);

/**
  * extract filename from filepath. Always ends with '/'. E.g.
  * - file path "/usr/local/include/stdio.h" gives "stdio.h"
  * - file path "stdio.h" gives "stdio.h"
  * - file path "../inlcude/stdio.h" gives "stdio.h"
  * - file path "../inlcude/" gives null pointer
  */
const char *filename(const char *filepath);

/**
  * extract extension from filepath. Always ends with '/'. E.g.
  * - file path "/usr/local/include/stdio.h" gives ".h"
  * - file path "stdio.h" gives ".h"
  * - file path "../inlcude/" gives null pointer
  */
const char *extension(const char *filepath);

/**
  * extract basename from filepath. Always ends with '/'. E.g.
  * - file path "/usr/local/include/stdio.h" gives "stdio"
  * - file path "stdio.h" gives "stdio"
  * - file path "../inlcude/" gives null pointer
  */
const char *basename(const char *filepath);

/**
  * For debugging
  */
void printStringPool();

#endif // ULMCC_STRING_H
