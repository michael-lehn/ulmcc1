#ifndef ULMCC_MEMREGION_H
#define ULMCC_MEMREGION_H

#include <stddef.h>

enum MemRegion {
    LEX,
    STRING,
    TYPE,
    SYMTAB,
    EXPR,
    STMT,
    NODE,
    MEMREGION_
};

void *alloc(enum MemRegion memRegion, size_t numBytes);

void dealloc(enum MemRegion memRegion);

/**
  * For debugging
  */
void printMemRegions();

#endif // ULMCC_MEMREGION_H
