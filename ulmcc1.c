#include <stdio.h>
#include <stdlib.h>

#include "instr_ulm.h"
#include "lexer.h"
#include "parse.h"
#include "string.h"
#include "type.h"

void
usage(const char *cmdname)
{
    fprintf(stderr, "Usage: %s [-o outfile] infile\n", cmdname);
    exit(1);
}

int
main(int argc, char **argv)
{
    char *inputfile = 0;
    char *outputfile = 0;

    for (int i = 1; i < argc; ++i) {
	if (argv[i][0] == '-') {
            if (argv[i][1] == 'o') {
                if (outputfile) {
                    usage(argv[0]);
                }
                if (i + 1 >= argc) {
                    usage(argv[0]);
                }
                ++i;
                outputfile = argv[i];
            } else {
                usage(argv[0]);
            }
        } else {
            if (inputfile) {
                usage(argv[0]);
            }
            inputfile = argv[i];
        }
    }

    FILE *in = stdin, *out;

    if (! inputfile) {
	fprintf(stderr, "no inputfile\n");
	exit(1);
    }

    in = fopen(inputfile, "r");
    if (! in) {
	fprintf(stderr, "%s: can not open for reading\n", inputfile);
	exit(1);
    }

    if (! outputfile) {
        char *infile = outputfile = inputfile;
	inputfile = (char *)addString(inputfile);
	while (*infile++) {
	}
	*(infile - 2) = 's';
    }

    if (parse(inputfile, in)) {
	out = fopen(outputfile, "w");
	if (! out) {
	    fprintf(stderr, "%s: can not open for writing\n", outputfile);
	    exit(2);
	}
	setOut(out);
	gencode();
	createGlobalSymtab();
    } else {
	printf("syntax error\n");
    }
    fclose(out);


    /*
    printf("Type set:\n");
    printTypeSet();
    */
}
