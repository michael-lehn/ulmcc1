#ifndef ULMCC_NODE_H
#define ULMCC_NODE_H 1

enum NodeKind {
    VARDEF,
    FUNCDEF,
};

#endif // ULMCC_NODE_H
