#include "type.h"

void
printInfo(const struct Type *t, const char *tag)
{
    printf("%s %p, string: '", tag, (void *) t);
    printType(t);
    printf("'\n");
}

int
main()
{
    const struct Type *t0 =
	makeConstType(makeConstType(makeUnsignedIntegerType(1)));

    const struct Type *t1 =
	makeConstType(makeConstType(makeUnsignedIntegerType(1)));
    const struct Type *t2 =
	makePointerType(makeConstType(makeUnsignedIntegerType(2)));
    const struct Type *t3 = makeUnsignedIntegerType(4);
    const struct Type *t4 = makeUnsignedIntegerType(8);

    const struct Type *t5 = makeSignedIntegerType(1);
    const struct Type *t6 = makeSignedIntegerType(2);
    const struct Type *t7 = makeSignedIntegerType(4);
    const struct Type *t8 = makeSignedIntegerType(8);

    printInfo(t0, "t0");
    printInfo(t1, "t1");
    printInfo(t2, "t2");
    printInfo(t3, "t3");
    printInfo(t4, "t4");
    printInfo(t5, "t5");
    printInfo(t6, "t6");
    printInfo(t7, "t7");
    printInfo(t8, "t8");

    const struct Type *a1 = makeArrayType(t2, 4);
    const struct Type *a2 = makeArrayType(a1, 8);
    printInfo(a1, "a1");
    printInfo(a2, "a2");


    const struct Type **p1 = allocParamArray(3);
    p1[0] = t1;
    p1[1] = t1;
    p1[2] = t2;
    const struct Type *f1 = makeFunctionType(t4, p1);
    printInfo(f1, "f1");

    const struct Type **p2 = allocParamArray(3);
    p2[0] = t1;
    p2[1] = t1;
    p2[2] = t2;
    const struct Type *f2 = makeFunctionType(t4, p2);
    printInfo(f2, "f2");

    printTypeSet(); 
}

