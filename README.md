C preprocessor and C compiler for the ULM. Requires the ULM assembler and ULM Linker.

Note that both, the preprocessor and the compiler, currently just support a subset of what is required for C99 ...

Examples: https://www.mathematik.uni-ulm.de/numerik/hpc/ss20/hpc0/session12/page02.html
