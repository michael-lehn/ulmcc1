#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "expr.h"
#include "memregion.h"
#include "msg.h"
#include "parse.h"
#include "string.h"

static const struct Type *
commonUnaryConversion(const struct Type *t)
{
    if (t->kind == ARRAY || t->kind == FUNCTION) {
	return makePointerType(t->u.ref);
    }
    if (t->kind == TK_STRUCT || t->kind == TK_UNION || t->kind == TK_VOID) {
	return 0;
    }
    return t;
}

static const struct Type *
commonBinaryConversion(const struct Type *t1, const struct Type *t2)
{
    if (!t1 || !t2) {
	return 0;
    } else if (t1 == t2) {
	return t1;
    } else if (t1->kind == INTEGER && t2->kind == INTEGER) {
	/* if signed and unsigned are mixed, unsigned wins */
	size_t size = t1->size > t2->size ? t1->size : t2->size;
	bool is_signed = t1->u.is_signed && t2->u.is_signed;

	if (is_signed) {
	    return makeSignedIntegerType(size);
	}
	return makeUnsignedIntegerType(size);
    }
    return t1;
}

static const struct Type *
computeTypeOfUnaryOp(enum ExprOp op, const struct Type *t)
{
    switch (op) {
	case EO_POSTFIX_INCR:
	case EO_POSTFIX_DECR:
	case EO_LOGICAL_NOT:
	case EO_BITWISE_NOT:
	case EO_UNARY_PLUS:
	    return commonUnaryConversion(t);

	case EO_UNARY_MINUS:
	    if (t->kind != INTEGER) {
		return 0;
	    }
	    return commonUnaryConversion(t);

	case EO_SIZEOF:
	    return makeUnsignedIntegerType(sizeOfSizeT);

	case EO_DEREF:
	    if (t->kind != POINTER && t->kind != ARRAY) {
		return 0;
	    }
	    return t->u.ref;

	case EO_ADDR:
	    if (t->kind == ARRAY) {
		return makePointerType(t->u.ref);
	    }
	    return makePointerType(t);

	default:
	    fprintf(stderr, "internal error: 'computeTypeOfUnaryOp'\n");
	    fprintf(stderr, "exprOp = %d, %d\n", op, EO_BITWISE_NOT);
	    exit(2);
	    return 0;
    }
}

static const struct Type *
computeTypeOfBinaryOp(enum ExprOp op, const struct Type *t1_,
		      const struct Type *t2_)
{
    const struct Type *t1 = commonUnaryConversion(t1_);
    const struct Type *t2 = commonUnaryConversion(t2_);

    if (! t1 || ! t2) {
	return 0;
    } else if (t1->kind != POINTER && t2->kind != POINTER) {
	return commonBinaryConversion(t1, t2);
    }
    if (op == EO_ADD) {
	if (t1->kind == POINTER && t2->kind == POINTER) {
	    return 0;
	}
	const struct Type *ptr = t1->kind == POINTER ? t1 : t2;
	const struct Type *oth = t1->kind == POINTER ? t2 : t1;
	if (oth->kind != INTEGER && oth->kind != TK_ENUM) {
	    return 0;
	}
	return ptr;
    } else if (op == EO_SUB) {
	if (t1->kind == POINTER && t2->kind == POINTER) {
	    return makeSignedIntegerType(8);
	}
    } else {
	return 0;
    }
    if (t1->kind != POINTER || t2->kind != INTEGER) {
	return 0;
    }
    return t1;
}

static const struct Type *
combineTypes(enum ExprOp op, const struct Type *t1, const struct Type *t2)
{
    t1 = unqualType(t1);
    t2 = unqualType(t2);

    switch (op) {
	case EO_CALL:
	    return t1->u.ref;

	case EO_GE:
	case EO_GT:
	case EO_LE:
	case EO_LT:
	case EO_LOGICAL_EQUAL:
	case EO_LOGICAL_NOT_EQUAL:
	case EO_LOGICAL_AND:
	case EO_LOGICAL_NOT:
	case EO_LOGICAL_OR:
	    // TODO: check if logical operation is allowed
	    return computeTypeOfBinaryOp(EO_SUB, t1, t2);

	case EO_ADD:
	case EO_DIV:
	case EO_MOD:
	case EO_MUL:
	case EO_SUB:
	case EO_BITWISE_OR:
	case EO_BITWISE_XOR:
	case EO_BITWISE_AND:
	    return computeTypeOfBinaryOp(op, t1, t2);

	case EO_ASSIGN:
	case EO_PREFIX_DECR:
	case EO_BITSHIFT_LEFT:
	case EO_BITSHIFT_RIGHT:
	case EO_PREFIX_INCR:
	    // TODO: check_assignment_compatibility
	    return t1;

	default:
	    fprintf(stderr, "internal error: 'combineTypes'\n");
	    exit(2);
    }
}

struct Expr *
castExpr(struct TokenLoc loc, const struct Expr *expr,
	 const struct Type *toType_)
{
    if (unqualType(toType_)->kind == ARRAY) {
	errorMsg(&loc, "cast specifies array type");
	return 0;
    }
    const struct Type *toType = commonUnaryConversion(unqualType(toType_));
    const struct Type *srcType = commonUnaryConversion(unqualType(expr->type));

    if (! toType) {
	errorMsg(&loc, "conversion to non-scalar type requested");
	return 0;
    }

    if (toType->kind == INTEGER && srcType->kind == INTEGER) {
	toType = toType->u.is_signed
	    ? makeSignedIntegerType(srcType->size)
	    : makeUnsignedIntegerType(srcType->size);
    } else if (toType->kind == POINTER && srcType->kind == INTEGER
	       && toType->size != srcType->size)
    {
	warningMsg(&loc, "cast from integer to pointer of different size");
    } else if (toType->kind == INTEGER && srcType->kind == POINTER
	       && toType->size != srcType->size)
    {
	warningMsg(&loc, "cast from pointer to integer of different size");
    }

    struct Expr *newExpr = alloc(EXPR, sizeof(*newExpr));
    *newExpr = *expr;
    newExpr->unfolded = newExpr;
    newExpr->type = toType;
    return newExpr;
}

struct Expr *
makeExprList(struct TokenLoc loc)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = EO_LIST;
    expr->u.l.first = expr->u.l.last = 0;
    expr->u.l.len = 0;
    expr->type = 0;
    expr->is_const = true;
    expr->is_lvalue = true;

    return expr;
}

void
exprListAppend(struct Expr *list, const struct Expr *expr)
{
    if (! expr) {
	return;
    }
    struct ExprListItem *item = alloc(EXPR, sizeof(*item));
    item->expr = expr;
    item->next = 0;
    list->type = expr->type;

    if (list->u.l.last) {
	list->u.l.last = list->u.l.last->next = item;
    } else {
	list->u.l.last =  list->u.l.first = item;
    }
    ++list->u.l.len;
}

struct Expr *
makeRefExpr(struct TokenLoc loc, const char *ident, const char *name,
	    ptrdiff_t offset, const  struct Type *type)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = EO_VAR;
    expr->u.v.memLoc.name = name;
    expr->u.v.memLoc.offset = offset;
    expr->u.v.ident = ident;

    expr->type = type;
    expr->is_const = false;
    expr->is_lvalue = true;

    return expr;
}

struct Expr *
makeVarExpr(struct TokenLoc loc, const struct Symbol *symbol)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = EO_VAR;
    expr->u.v.memLoc = symbol->memLoc;
    expr->u.v.ident = symbol->ident;
    expr->type = symbol->type->kind == TK_ENUM
	? symbol->type->p.enumRep
	: symbol->type;

    expr->is_const = false;
    if (expr->type->kind == ARRAY && expr->u.v.memLoc.name) {
	expr->is_const = true;
    }
    expr->is_lvalue = true;

    return expr;
}

struct Expr *
makeLitExpr(struct TokenLoc loc, const struct Type *type,
	    union LiteralValue literalValue)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = EO_LIT;
    expr->u.lit = literalValue;
    expr->type = type;
    expr->is_const = true;
    expr->is_lvalue = false;

    return expr;
}

struct Expr *
makeSIntExpr(struct TokenLoc loc, size_t size, int64_t literal)
{
    union LiteralValue literalValue;
    literalValue.int64 = literal;
    const struct Type *type = size
	? makeSignedIntegerType(size)
	: makeSignedIntegerTypeFromLiteral(literal);
    return makeLitExpr(loc, type, literalValue);
}

struct Expr *
makeUIntExpr(struct TokenLoc loc, size_t size, int64_t literal)
{
    union LiteralValue literalValue;
    literalValue.int64 = literal;
    const struct Type *type = size
	? makeUnsignedIntegerType(size)
	: makeUnsignedIntegerTypeFromLiteral(literal);
    return makeLitExpr(loc, type, literalValue);
}

struct Expr *
makeStrLitExpr(struct TokenLoc loc, const char *str, const char *escStr)
{
    union LiteralValue literalValue;
    literalValue.s.str = str;
    literalValue.s.escStr = escStr;
    const struct Type *type = makeArrayType(makeUnsignedIntegerType(1),
					    getStringLen(str));
    return makeLitExpr(loc, type, literalValue);
}

static struct Expr *
constFoldUnaryExpr(struct Expr *expr)
{
    if (expr->op == EO_UNARY_MINUS && expr->u.child[0]->op == EO_LIT) {
	union LiteralValue lit;
	lit.int64 = -expr->u.child[0]->u.lit.int64;
	lit.int64 &= (1 << 8 * expr->type->size) - 1;
	struct Expr *cf = makeLitExpr(expr->loc, expr->type, lit);
	cf->unfolded = expr;
	return cf;
    }
    if (expr->op == EO_LOGICAL_NOT && expr->u.child[0]->op == EO_LIT) {
	union LiteralValue lit;
	lit.int64 = !expr->u.child[0]->u.lit.int64;
	struct Expr *cf = makeLitExpr(expr->loc, expr->type, lit);
	cf->unfolded = expr;
	return cf;
    }
    return expr;
}

struct Expr *
makeUnaryExpr(struct TokenLoc loc, enum ExprOp op, const struct Expr *first)
{
    const struct Type *type = computeTypeOfUnaryOp(op, first->type);
    if (! type) {
	errorMsg(&loc, "invalid unary expression\n");
	printf("op = %d, first->type:\n", op);
	printType(first->type);
	printf("\n");
	exit(1);
	return 0;
    }
    if (first->op == EO_LIST) {
	printf("'makeUnaryExpr' fisrt is an expression list\n");
	//exit(2);
    }
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = op;
    expr->type = type;
    expr->u.child[0] = first;
    expr->is_const = false;
    expr->is_lvalue = false;

    if (op == EO_ADDR && first->op == EO_VAR && first->u.v.memLoc.name) {
	expr->is_const = true;
    } else if (op == EO_UNARY_MINUS) {
	expr->is_const = first->is_const;
    }

    if (op == EO_DEREF) {
	expr->is_lvalue = true;
    }

    struct Expr *cf = 0;
    if (op == EO_SIZEOF) {
	union LiteralValue lit;
	lit.uint64 = first->type->size;
	cf = makeLitExpr(loc, type, lit);
    } else if (op == EO_UNARY_PLUS) {
	cf = alloc(EXPR, sizeof(*cf));
	*cf = *first;
    } else if (op == EO_ADDR && first->op == EO_DEREF) {
	cf = alloc(EXPR, sizeof(*cf));
	*cf = *first->u.child[0];
    } else if (op == EO_DEREF && first->op == EO_ADDR) {
	cf = alloc(EXPR, sizeof(*cf));
	*cf = *first->u.child[0];
	cf->type = expr->type;
    } else if (op == EO_UNARY_MINUS && first->op == EO_UNARY_MINUS) {
	cf = alloc(EXPR, sizeof(*cf));
	*cf = *first->u.child[0];
    }

    if (cf) {
	cf->unfolded = expr;
	expr = cf;
    }

    return constFoldUnaryExpr(expr);
}

static struct Expr *
constFoldBinaryExpr(struct Expr *expr)
{
    const struct Expr *first = expr->u.child[0];
    const struct Expr *second = expr->u.child[1];

    if (first->op == EO_LIT && first->type->kind == INTEGER
	&& second->op == EO_LIT && second->type->kind == INTEGER)
    {
	bool is_signed = expr->type->u.is_signed;
	union LiteralValue lit;
	switch (expr->op) {
	    case EO_ADD:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 + second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 + second->u.lit.uint64;
		}
		break;

	    case EO_SUB:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 - second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 - second->u.lit.uint64;
		}
		break;

	    case EO_DIV:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 / second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 / second->u.lit.uint64;
		}
		break;

	    case EO_MUL:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 * second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 * second->u.lit.uint64;
		}
		break;

	    case EO_MOD:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 % second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 % second->u.lit.uint64;
		}
		break;

	    case EO_GE:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 >= second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 >= second->u.lit.uint64;
		}
		break;

	    case EO_GT:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 > second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 > second->u.lit.uint64;
		}
		break;

	    case EO_LE:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 <= second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 <= second->u.lit.uint64;
		}
		break;

	    case EO_LT:
		if (is_signed) {
		    lit.int64 = first->u.lit.int64 < second->u.lit.int64;
		} else {
		    lit.uint64 = first->u.lit.uint64 < second->u.lit.uint64;
		}
		break;

	    case EO_LOGICAL_EQUAL:
		lit.int64 = first->u.lit.int64 == second->u.lit.int64;
		break;

	    case EO_LOGICAL_NOT_EQUAL:
		lit.int64 = first->u.lit.int64 != second->u.lit.int64;
		break;

	    default:
		return expr;
	}
	struct Expr *cf = makeLitExpr(expr->loc, expr->type, lit);
	cf->unfolded = expr;
	return cf;
    }
    return expr;
}

struct Expr *
makeBinaryExpr(struct TokenLoc loc, enum ExprOp op, const struct Expr *first,
	       const struct Expr *second)
{
    const struct Type *type = combineTypes(op, first->type, second->type);
    if (! type) {
	errorMsg(&loc, "invalid binary expression\n");
	printf("first operand:\n");
	printExprTree(first);
	printf("second operand:\n");
	printExprTree(second);
	exit(1);
	return 0;
    }

    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = op;
    expr->type = type;
    expr->u.child[0] = first;
    expr->u.child[1] = second;
    expr->is_const = expr->u.child[0]->is_const && expr->u.child[1]->is_const;

    if (op == EO_ADD && first->type->kind == ARRAY &&  second->op == EO_LIT) {
	expr->is_const = true;
    }


    struct Expr *cf = 0;
    if (op == EO_ADD && second->op == EO_UNARY_MINUS) {
	cf = makeBinaryExpr(loc, EO_SUB, first, second->u.child[0]);
    } else if (op == EO_SUB && second->op == EO_UNARY_MINUS) {
	cf = makeBinaryExpr(loc, EO_ADD, first, second->u.child[0]);
    } else if (first->op == EO_LIT && second->op != EO_LIT) {
	if (op == EO_ADD || op == EO_MUL) {
	    cf = makeBinaryExpr(loc, op, second, first);
	} else if (op == EO_SUB) {
	    cf = makeBinaryExpr(loc, EO_ADD,
				makeUnaryExpr(second->loc, EO_UNARY_MINUS,
					      second),
				first);
	}
    } else if (op == EO_ADD && type->kind == POINTER
	       && (first->type->kind != POINTER && first->type->kind != ARRAY))
    {
	cf = makeBinaryExpr(loc, op, second, first);
    } else if (op == EO_PREFIX_INCR) {
	cf = makeBinaryExpr(loc, EO_ASSIGN, first,
			    makeBinaryExpr(loc, EO_ADD, first, second));
    } else if (op == EO_PREFIX_DECR) {
	cf = makeBinaryExpr(loc, EO_ASSIGN, first,
			    makeBinaryExpr(loc, EO_SUB, first, second));
    }

    if (cf) {
	cf->unfolded = expr;
	expr = cf;
    }
    return constFoldBinaryExpr(expr);
}

struct Expr *
makeTernaryExpr(struct TokenLoc loc, enum ExprOp op, const struct Expr *first,
		const struct Expr *second, const struct Expr *third)
{
    const struct Type *t1 = unqualType(second->type);
    const struct Type *t2 = unqualType(third->type);
    const struct Type *type = t1 == t2
	? t1
	: combineTypes(EO_SUB, t1, t2);

    if (! type) {
	errorMsg(&loc, "> invalid ternary expression\n");
	printf("op = %d, first->type:\n", op);
	printType(first->type);
	printf("\nsecond->type:\n");
	printType(second->type);
	printf("\nthird->type:\n");
	printType(third->type);
	printf("\n");
	exit(1);
	return 0;
    }
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    expr->unfolded = expr;
    expr->loc = loc;
    expr->op = op;
    expr->type = type;
    expr->u.child[0] = first;
    expr->u.child[1] = second;
    expr->u.child[2] = third;
    expr->is_const = false;
    expr->is_lvalue = false;

    if (first->is_const && first->op == EO_LIT) {
	struct Expr *cf = alloc(EXPR, sizeof(*cf));
	*cf = first->u.lit.uint64 ? *second : *third;
	cf->unfolded = expr;
	expr = cf;
    }

    return expr;
}

bool
assignmentCompatible(struct TokenLoc loc, bool init, const struct Type *lType,
		     const struct Expr *rExpr, const struct Type *rType)
{
    if (rExpr && rType) {
	fprintf(stderr, "internal error: in 'assignmentCompatible'\n");
	exit(2);
    }
    if (rExpr) {
	rType = rExpr->type;
    }

    if (!init && lType->kind == TK_CONST) {
	errorMsg(&loc, "assignment of read-only variable");
	exit(1);
    }
    rType = unqualType(rType);
    if (lType == rType) {
	return true;
    }
    if (lType->kind == TK_STRUCT || rType->kind == TK_STRUCT) {
	errorMsg(&loc, "incompatible types when assigning to type '%s' from"
		 " type '%s'", addString(typeStr(lType)),
		 addString(typeStr(rType)));
	return false;
    }
    if (lType->kind == TK_VOID) {
    }
    if (rType->kind == TK_VOID) {
	errorMsg(&loc, "void value not ignored as it ought to be");
	return false;
    }
    if (lType->kind == ARRAY) {
	errorMsg(&loc, "assignment to expression with array type");
	return false;
    }
    if (lType->kind == FUNCTION) {
	errorMsg(&loc, "lvalue required as left operand of assignment");
	return false;
    }
    if (lType->kind == POINTER
	&& (rType->kind == POINTER || rType->kind == ARRAY))
    {
	if (lType->u.ref->kind != TK_CONST && rType->u.ref->kind == TK_CONST) {
	    warningMsg(&loc, "assignment discards 'const' qualifier from"
		       " pointer target type");
	}
	const struct Type *lType_ = unqualType(lType->u.ref);
	const struct Type *rType_ = unqualType(rType->u.ref);

	if (lType_->kind != TK_VOID && rType_->kind != TK_VOID
	    && ! assignmentCompatible(loc, init, lType_, 0, rType_))
	{
	    warningMsg(&loc, "assignment to '%s' from incompatible pointer"
		       " type '%s'", addString(typeStr(lType)),
		       addString(typeStr(rType)));
	}
    }
    if (lType->kind == POINTER && rType->kind == INTEGER) {
	if (!rExpr || rExpr->op != EO_LIT || rExpr->u.lit.uint64 != 0) {
	    warningMsg(&loc, "assignment to '%s' from '%s' makes pointer from"
		       " integer without a cast", addString(typeStr(lType)),
		       addString(typeStr(rType)));
	}
    }
    if (lType->kind == INTEGER
	&& (rType->kind == POINTER || rType->kind == ARRAY))
    {
	warningMsg(&loc, "assignment to '%s' from '%s' makes integer from"
		   " pointer without a cast", addString(typeStr(lType)),
		   addString(typeStr(rType)));
    }
    return true;
}


static void
printIndent(size_t indent)
{
    for (size_t i = 0; i < indent; ++i) {
	printf(" ");
    }
}

#ifndef EXPR_BUFFER_SIZE
#define EXPR_BUFFER_SIZE 256
#endif

static char exprStr_[EXPR_BUFFER_SIZE];
static char *exprStrPos = exprStr_;

static void
exprStr_reset()
{
    exprStrPos = exprStr_;
    *exprStrPos = 0;
}

static void
exprStr_append(const char *s)
{
    while (exprStrPos - exprStr_ < EXPR_BUFFER_SIZE) {
	if (! (*exprStrPos++ = *s++)) {
	    --exprStrPos;
	    break;
	}
    }
    if (exprStrPos - exprStr_ >= EXPR_BUFFER_SIZE) {
	fprintf(stderr, "exprStr_append: limit of %d bytes for line buffer"
		" exeeded.\n",
		EXPR_BUFFER_SIZE);
	exit(2);
    }
}

static void
vexprStr_vappend(const char *fmt, va_list argp)
{
    size_t size = EXPR_BUFFER_SIZE - (exprStrPos - exprStr_);
    exprStrPos += vsnprintf(exprStrPos, size, fmt, argp);
}

static void
exprStr_vappend(const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vexprStr_vappend(fmt, argp);
    va_end(argp);
}

static const char *
exprStrOp(enum ExprOp op)
{
    switch (op) {
	case EO_ASSIGN:
	    return "=";

	case EO_ADDR:
	    return "&";

	case EO_CALL:
	    return "call: ";

	case EO_SIZEOF:
	    return "sizeof ";

	case EO_MOD:
	    return "%";

	case EO_DIV:
	    return "/";

	case EO_DEREF:
	    return "value at";

	case EO_MUL:
	    return "*";

	case EO_UNARY_PLUS:
	    return " +";

	case EO_ADD:
	    return "+";

	case EO_UNARY_MINUS:
	    return " -";

	case EO_POSTFIX_INCR:
	    return "++";

	case EO_POSTFIX_DECR:
	    return "--";

	case EO_SUB:
	    return "-";

	case EO_PREFIX_DECR:
	    return "-=";

	case EO_PREFIX_INCR:
	    return "+=";

	case EO_LOGICAL_EQUAL:
	    return "==";

	case EO_LOGICAL_NOT_EQUAL:
	    return "!=";

	case EO_LOGICAL_AND:
	    return "&&";

	case EO_LOGICAL_OR:
	    return "||";

	case EO_BITWISE_NOT:
	    return "~";

	case EO_BITWISE_XOR:
	    return "^";

	case EO_BITWISE_OR:
	    return "|";

	case EO_BITWISE_AND:
	    return "&";

	case EO_BITSHIFT_LEFT:
	    return "<<";

	case EO_BITSHIFT_RIGHT:
	    return ">>";

	case EO_GE:
	    return ">=";

	case EO_GT:
	    return ">";

	case EO_LE:
	    return "<=";

	case EO_LT:
	    return "<";

	case EO_LOGICAL_NOT:
	    return "!";

	default:
	    return "<?>";
    }
}

static void
printExprTree_(const struct Expr *expr, size_t indent)
{
    if (! expr) {
	printIndent(indent);
	printf("(null)\n");
	return;
    }
    // expr = expr->unfolded;
    switch (expr->op) {
	case EO_CALL:
	    {
		printIndent(indent);
		printf("function call: ");
		printExpr(expr);
	    }
	    break;

	case EO_VAR:
	    printIndent(indent);
	    if (expr->u.v.ident) {
		printf("'%s'", expr->u.v.ident);
	    } else {
		if ( expr->u.v.ident) {
		   printf("'%s' ", expr->u.v.ident);
		}
		printf("<stack>");
	    }
	    printf("%td", expr->u.v.memLoc.offset);
	    printf(" (type: ");
	    printType(expr->type);
	    printf(")\n");
	    break;

	case EO_LIT:
	    printIndent(indent);
	    if (expr->type->kind == INTEGER) {
		if (expr->type->u.is_signed) {
	    	    printf("%lld\n", expr->u.lit.int64);
	    	} else {
	    	    printf("%llu\n", expr->u.lit.uint64);
	    	}
	    } else if (expr->type->kind == ARRAY) {
		printf("%s\n", expr->u.lit.s.str);
	    } else {
		fprintf(stderr, "internal error: EO_LIT in 'printExprTree_'\n");
		exit(2);
	    }
	    break;

	case EO_LOGICAL_NOT:
	case EO_UNARY_MINUS:
	case EO_UNARY_PLUS:
	case EO_SIZEOF:
	case EO_ADDR:
	case EO_DEREF:
	    printIndent(indent);
	    printf("%s", exprStrOp(expr->op));
	    printf(", type: ");
	    printType(expr->type);
	    printf("\n");
	    printExprTree_(expr->u.child[0], indent + 4);
	    break;

	case EO_LIST:
	    {
		const struct ExprListItem *item = expr->u.l.first;
		for (; item; item = item->next) {
		    printExprTree_(item->expr, indent);
		    if (item->next) {
			printIndent(indent);
			printf(",\n");
		    }
		}
	    }
	    break;

	case EO_ADD:
	case EO_LOGICAL_EQUAL:
	case EO_LOGICAL_NOT_EQUAL:
	case EO_LOGICAL_OR:
	case EO_MOD:
	case EO_MUL:
	case EO_SUB:
    	case EO_ASSIGN:
    	case EO_DIV:
    	case EO_LOGICAL_AND:
	case EO_PREFIX_DECR:
	case EO_PREFIX_INCR:
	case EO_GE:
	case EO_GT:
	case EO_LE:
	case EO_LT:
	    printIndent(indent);
	    printf(exprStrOp(expr->op));
	    printf(", type: ");
	    printType(expr->type);
	    printf("\n");
	    printExprTree_(expr->u.child[0], indent + 4);
	    printExprTree_(expr->u.child[1], indent + 4);
	    break;

	case EO_COND:
	    printExprTree_(expr->u.child[0], indent + 4);
	    printf("?");
	    printExprTree_(expr->u.child[1], indent + 4);
	    printf(":");
	    printExprTree_(expr->u.child[2], indent + 4);
	    break;

	default:
	    fprintf(stderr, "internal error: 'printExprTree'\n");
	    errorMsg(&expr->loc, "expr->op = %d\n", expr->op);
	    exit(2);
    }
}

void
printExprTree(const struct Expr *expr)
{
    if (! expr) {
	return;
    }
    printExprTree_(expr, 0);
    printf("\n");
}

static void
makeExprStr_(const struct Expr *expr, bool isFactor)
{
    if (! expr) {
	return;
    }
    expr = expr->unfolded;
    switch (expr->op) {
	case EO_LIST:
	    {
		const struct ExprListItem *item = expr->u.l.first;
		for (; item; item = item->next) {
		    makeExprStr_(item->expr, false);
		    if (item->next) {
			exprStr_append(", ");
		    }
		}
	    }
	    break;
	    
	case EO_CALL:
	    {
		const char *fname = expr->u.child[0]->op == EO_VAR
		    ? expr->u.child[0]->u.v.ident
		    : "<func-ptr>";
		exprStr_vappend("%s(", fname);
		const struct ExprListItem *arg = expr->u.child[1]->u.l.first;
		for (size_t i = 0; arg; arg = arg->next, ++i) {
		    makeExprStr_(arg->expr, false);
		    if (arg->next) {
			exprStr_append(", ");
		    }
		}
		exprStr_append(")");
	    }
	    break;

	case EO_VAR:
	    if (expr->u.v.ident) {
		exprStr_vappend("%s", expr->u.v.ident);
	    } else {
		if (expr->u.v.memLoc.name) {
		    exprStr_vappend("[%s + %td]\n", expr->u.v.memLoc.name,
				    expr->u.v.memLoc.offset);
		} else {
		    exprStr_vappend("[%%FP + %td]", expr->u.v.memLoc.offset);
		}
	    }
	    break;

	case EO_LIT:
	    if (expr->type->kind == INTEGER) {
		if (expr->type->u.is_signed) {
	    	    exprStr_vappend("%lld", expr->u.lit.int64);
	    	} else {
	    	    exprStr_vappend("%lluu", expr->u.lit.uint64);
	    	}
	    } else if (expr->type->kind == ARRAY) {
		exprStr_vappend("%s", expr->u.lit.s.str);
	    } else {
		fprintf(stderr, "internal error: EO_LIT in 'makeExprStr_'\n");
		exit(2);
	    }
	    break;

	case EO_LOGICAL_NOT:
	case EO_UNARY_MINUS:
	case EO_UNARY_PLUS:
	case EO_BITWISE_NOT:
	case EO_SIZEOF:
	case EO_ADDR:
	    exprStr_append(exprStrOp(expr->op));
	    makeExprStr_(expr->u.child[0], true);
	    break;

	case EO_POSTFIX_INCR:
	case EO_POSTFIX_DECR:
	    makeExprStr_(expr->u.child[0], true);
	    exprStr_append(exprStrOp(expr->op));
	    break;

	case EO_DEREF:
	    exprStr_append("*");
	    makeExprStr_(expr->u.child[0], true);
	    break;

	case EO_ADD:
	case EO_GE:
	case EO_GT:
	case EO_LE:
	case EO_LT:
	case EO_LOGICAL_EQUAL:
	case EO_LOGICAL_NOT_EQUAL:
	case EO_LOGICAL_OR:
	case EO_BITWISE_OR:
	case EO_BITWISE_XOR:
	case EO_BITSHIFT_LEFT:
	case EO_BITSHIFT_RIGHT:
	case EO_SUB:
    	case EO_ASSIGN:
	    if (isFactor) {
		exprStr_append("(");
	    }
	    makeExprStr_(expr->u.child[0], isFactor);
	    exprStr_append(" ");
	    exprStr_append(exprStrOp(expr->op));
	    exprStr_append(" ");
	    makeExprStr_(expr->u.child[1], isFactor);
	    if (isFactor) {
		exprStr_append(")");
	    }
	    break;

	case EO_PREFIX_DECR:
	case EO_PREFIX_INCR:
    	case EO_LOGICAL_AND:
	case EO_BITWISE_AND:
	case EO_MOD:
	case EO_MUL:
    	case EO_DIV:
	    if (isFactor) {
		exprStr_append("(");
	    }
	    makeExprStr_(expr->u.child[0], true);
	    exprStr_append(" ");
	    exprStr_append(exprStrOp(expr->op));
	    exprStr_append(" ");
	    makeExprStr_(expr->u.child[1], true);
	    if (isFactor) {
		exprStr_append(")");
	    }
	    break;


	case EO_COND:
	    makeExprStr_(expr->u.child[0], isFactor);
	    exprStr_append("?");
	    makeExprStr_(expr->u.child[1], true);
	    exprStr_append(":");
	    makeExprStr_(expr->u.child[2], true);
	    break;

	default:
	    fprintf(stderr, "internal error: 'makeExprStr_'\n");
	    exit(2);
    }
}

const char *
exprStr(const struct Expr *expr)
{
    exprStr_reset();
    makeExprStr_(expr, false);
    return exprStr_;
}

static void
constExprStr_(const struct Expr *expr)
{
    switch (expr->op) {
	case EO_LIT:
	    if (expr->type->kind == INTEGER) {
		if (expr->type->u.is_signed) {
	    	    exprStr_vappend("%lld", expr->u.lit.int64);
	    	} else {
	    	    exprStr_vappend("%llu", expr->u.lit.uint64);
	    	}
	    } else if (expr->type->kind == ARRAY) {
		exprStr_vappend("\"%s\"", expr->u.lit.s.str);
	    } else {
		fprintf(stderr, "internal error: EO_LIT in 'constExprStr_'\n");
		exit(2);
	    }
	    break;

	case EO_ADD:
	    {
		size_t scale = expr->u.child[0]->type->kind == POINTER
			|| expr->u.child[0]->type->kind == ARRAY
		    ? expr->u.child[0]->type->u.ref->size
		    : 1;
		 constExprStr_(expr->u.child[0]);
		 exprStr_append(" +");
		 if (scale > 1) {
		     exprStr_vappend(" %zu * (", scale);
		     constExprStr_(expr->u.child[1]);
		     exprStr_append(")");
		 } else {
		     constExprStr_(expr->u.child[1]);
		 }
	    }
	    break;

	case EO_VAR:
	    exprStr_vappend("%s + %td", expr->u.v.memLoc.name,
			    expr->u.v.memLoc.offset);
	    break;

	case EO_ADDR:
	    expr = expr->u.child[0];
	    if (expr->u.v.memLoc.name) {
		const char *name = expr->u.v.memLoc.name;
		ptrdiff_t offset = expr->u.v.memLoc.offset;
		if (offset > 0) {
		    exprStr_vappend("%s + %td", name, offset);
		} else if (offset < 0) {
		    exprStr_vappend("%s - %td", name, -offset);
		} else {
		    exprStr_vappend("%s", name);
		}
	    }
	    break;

	default:
	    fprintf(stderr, "internal error: 'constExprStr_()'\n");
	    fprintf(stderr, "op = %d\n", expr->op);
	    //fprintf(stderr, "as tokenKindStr = %s\n", tokenKindStr(expr->op));
	    fprintf(stderr, "expr = %s\n", exprStr(expr));
	    printExprTree(expr);
	    exit(2);
	    break;
    }
}

const char *
constExprStr(const struct Expr *expr)
{
    exprStr_reset();
    if (! expr) {
	fprintf(stderr, "internal error: 'constExprStr()'\n");
	exit(2);
	return 0;
    }
    constExprStr_(expr);
    return exprStr_;
}

void
printExpr(const struct Expr *expr)
{
    printf("%s", exprStr(expr));
}


static const struct Expr *parseExpressionList();
static const struct Expr *parseAssignmentExpression();
static const struct Expr *parseLogicalAndExpression();

static const struct Expr *parseBitwiseOrExpression();
static const struct Expr *parseBitwiseXOrExpression();
static const struct Expr *parseBitwiseAndExpression();
static const struct Expr *parseBitshiftExpression();

static const struct Expr *parseEqualityExpression();
static const struct Expr *parseRelationalExpression();
static const struct Expr *parseAdditiveExpression();
static const struct Expr *parseMultiplicativeExpression();
static const struct Expr *parseUnaryPrefixExpression();
static const struct Expr *parsePostfixExpression(const struct Expr *primary);
static const struct Expr *parsePrimaryExpression();

const struct Expr *
parseExpr()
{
    return parseExpressionList();
}

const struct Expr *
parseInitializerList(const struct Type *type)
{
    if (type->kind == ARRAY && type->u.ref->size == 1
	&& tokenKind == STRING_LITERAL)
    {
	struct Expr *list = makeExprList(tokenLoc);
	const char *str = processedTokenValue;
	struct TokenLoc loc = tokenLoc;
	do {
	    ++loc.begin.col;
	    exprListAppend(list, makeSIntExpr(loc, 1, (unsigned char)*str));
	} while (*str++);
	getToken();
	return list;
    } else if (type->kind == ARRAY) {
	expected(LBRACE);
	struct Expr *list = makeExprList(tokenLoc);
	const struct Expr *item = 0;
	while ((item = parseInitializerList(type->u.ref))) {
	    exprListAppend(list, item);
	    if (tokenKind != COMMA) {
		break;
	    }
	    getToken();
	    if (tokenKind == RBRACE) {
		break;
	    }
	}
	expected(RBRACE);
	return list;
    } else if (type->kind == TK_STRUCT) {
	if (tokenKind == LBRACE) {
	    getToken();
	    struct Expr *list = makeExprList(tokenLoc);
	    const struct Symtab *members = type->u.tag->u.members;
	    for (const struct SymtabEntry *e = members->first; e; e = e->next) {
	        exprListAppend(list, parseInitializerList(e->symbol.type));
	        if (tokenKind != COMMA) {
		    break;
	        }
	        getToken();
	    }
	    expected(RBRACE);
	    return list;
	}
    } else if (type->kind == TK_UNION) {
	fprintf(stderr, "internal error: 'parseInitializerList' UNION\n");
    }
    const struct Expr *expr = parseAssignmentExpression();
    if (! assignmentCompatible(expr->loc, true, type, expr, 0)) {
	expr = 0;
	exit(1);
    }
    return expr;
}

static const struct Expr *
parseExpressionList()
{
    const struct Expr *expr = parseAssignmentExpression();
    if (! expr) {
	return 0;
    }
    if (tokenKind == COMMA) {
	struct Expr *list = makeExprList(tokenLoc);
	exprListAppend(list, expr);
	while (tokenKind == COMMA) {
    	    getToken();
    	    const struct Expr *expr = parseAssignmentExpression();
    	    exprListAppend(list, expr);
    	}
	expr = list;
    }
    return expr;
}

static const struct Expr *
parseAssignmentExpression()
{
    const struct Expr *expr = parseConditionalExpression();
    if (! expr) {
	return 0;
    }

    if (tokenKind == EQUAL || tokenKind == PLUS_EQ || tokenKind == MINUS_EQ
	|| tokenKind == ASTERISK_EQ || tokenKind == SLASH_EQ
	|| tokenKind == AMPERSAND_EQ || tokenKind == VBAR_EQ
	|| tokenKind == CARET_EQ || tokenKind == PERCENT_EQ)
    {
	if (! expr->is_lvalue) {
	    errorMsg(&expr->loc, "not an lvalue");
	    exit(1);
	}

	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp;
	switch (tokenKind) {
	    case EQUAL:
		exprOp = EO_ASSIGN;
		break;
	    case AMPERSAND_EQ:
		exprOp = EO_BITWISE_AND;
		break;
	    case VBAR_EQ:
		exprOp = EO_BITWISE_OR;
		break;
	    case CARET_EQ:
		exprOp = EO_BITWISE_XOR;
		break;
	    case PLUS_EQ:
		exprOp = EO_ADD;
		break;
	    case MINUS_EQ:
		exprOp = EO_SUB;
		break;
	    case ASTERISK_EQ:
		exprOp = EO_MUL;
		break;
	    case SLASH_EQ:
		exprOp = EO_DIV;
		break;
	    case PERCENT_EQ:
		exprOp = EO_MOD;
		break;
	    default:
		fprintf(stderr, "internal error:"
			" 'parseAssignmentExpression'\n");
		exit(2);
	}
	getToken();

	const struct Expr *assignExpr = parseAssignmentExpression();
	if (! assignmentCompatible(expr->loc, false, expr->type,
				   assignExpr, 0))
	{
	    errorMsg(&opLoc, "types are not assignment compatible");
	    errorMsg(&assignExpr->loc, "can not convert type");
	    printType(assignExpr->type);
	    printf("\n");
	    errorMsg(&expr->loc, "to type ");
	    printType(expr->type);
	    printf("\n");
	    exit(1);
	    return 0;
	}
	if (exprOp != EO_ASSIGN) {
	    assignExpr = makeBinaryExpr(opLoc, exprOp, expr, assignExpr);
	}
	expr = makeBinaryExpr(opLoc, EO_ASSIGN, expr, assignExpr);
    }
    return expr;
}

const struct Expr *
parseConditionalExpression()
{
    const struct Expr *expr = parseLogicalOrExpression(); 
    if (! expr) {
	return 0;
    }

    if (tokenKind == QMARK) {
	struct TokenLoc opLoc = tokenLoc;
	getToken();

	const struct Expr *expr0 = parseExpressionList();
	if (! expr0) {
	    errorMsg(&tokenLoc, "expression list expected");
	    return 0;
	}
	expected(COLON);
	const struct Expr *expr1 = parseConditionalExpression();
	expr = makeTernaryExpr(opLoc, EO_COND, expr, expr0, expr1);
	if (! expr) {
	    return 0;
	}
    }
    return expr;
}

const struct Expr *
parseLogicalOrExpression()
{
    const struct Expr *expr = parseLogicalAndExpression();
    while (tokenKind == VBAR2) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	// make || right associative
	//const struct Expr *expr1 = parseLogicalOrExpression();
	const struct Expr *expr1 = parseLogicalAndExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'logical and' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseLogicalAndExpression()
{
    const struct Expr *expr = parseBitwiseOrExpression();
    while (tokenKind == AMPERSAND2) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseBitwiseOrExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'bitwise Or' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

//-
static const struct Expr *
parseBitwiseOrExpression()
{
    const struct Expr *expr = parseBitwiseXOrExpression();
    while (tokenKind == VBAR) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseBitwiseXOrExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'bitwise XOr' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseBitwiseXOrExpression()
{
    const struct Expr *expr = parseBitwiseAndExpression();
    while (tokenKind == CARET) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseBitwiseAndExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'bitwise and' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseBitwiseAndExpression()
{
    const struct Expr *expr = parseEqualityExpression();
    while (tokenKind == AMPERSAND) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseEqualityExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'logical equal' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}
//-

static const struct Expr *
parseEqualityExpression()
{
    const struct Expr *expr = parseRelationalExpression();
    while (tokenKind == EQUAL2 || tokenKind == NE) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *exp1 = parseRelationalExpression();
        if (! exp1) {
	    errorMsg(&tokenLoc, "'logical equal' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, exp1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseRelationalExpression()
{
    const struct Expr *expr = parseBitshiftExpression();
    while (tokenKind == GE || tokenKind == GT || tokenKind == LE
	   || tokenKind == LT)
    {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseBitshiftExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'logical equal' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseBitshiftExpression()
{
    const struct Expr *expr = parseAdditiveExpression();
    while (tokenKind == GT2 || tokenKind == LT2) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseAdditiveExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'logical equal' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseAdditiveExpression()
{
    const struct Expr *expr = parseMultiplicativeExpression();
    while (tokenKind == PLUS || tokenKind == MINUS) {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseMultiplicativeExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'multiplicative' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseMultiplicativeExpression()
{
    const struct Expr *expr = parseUnaryPrefixExpression();
    while (tokenKind == ASTERISK || tokenKind == SLASH || tokenKind == PERCENT)
    {
	struct TokenLoc opLoc = tokenLoc;
	enum ExprOp exprOp = (enum ExprOp) tokenKind;
	getToken();

	const struct Expr *expr1 = parseUnaryPrefixExpression();
        if (! expr1) {
	    errorMsg(&tokenLoc, "'unary' expression expected");
	    return 0;
        }
	expr = makeBinaryExpr(opLoc, exprOp, expr, expr1);
        if (! expr) {
            return 0;
        }
    }
    return expr;
}

static const struct Expr *
parseUnaryPrefixExpression()
{
    struct TokenLoc opLoc = tokenLoc;
    enum ExprOp exprOp;

    switch (tokenKind) {
	case MINUS:
	    exprOp = EO_UNARY_MINUS;
	    break;

	case TILDE:
	    exprOp = EO_BITWISE_NOT;
	    break;

	case PLUS:
	    exprOp = EO_UNARY_PLUS;
	    break;

	case ASTERISK:
	    exprOp = EO_DEREF;
	    break;

	case AMPERSAND:
	    exprOp = EO_ADDR;
	    break;

	case EXCLAMATION:
	    exprOp = (enum ExprOp) tokenKind;
	    break;

	case SIZEOF:
	    {
                getToken();
		const struct Type *type = 0;
		if (tokenKind == LPAREN) {
		    getToken();
		    if (! foundTypeToken()) {
			const struct Expr *expr
			    = parsePostfixExpression(parseExpressionList());
			if (expr) {
			    type = expr->type;
			}
		    } else {
			type = parseAbstractDeclaration();
		    }
		    expected(RPAREN);
		} else {
		    const struct Expr *expr = parseUnaryPrefixExpression();
		    if (expr) {
			type = expr->type;
		    }
		}
		if (! type) {
		    errorMsg(&tokenLoc, "expression or typename expected");
		    exit(1);
		}
		return makeUIntExpr(opLoc, 8, type->size);
	    }

	case LPAREN:
	    {
		getToken();
		const struct Expr *expr = 0;
		if (! foundTypeToken()) {
		    expr = parseExpressionList();
		    expected(RPAREN);
		    expr = parsePostfixExpression(expr);
		} else {
		    const struct TokenLoc loc = tokenLoc;
		    const struct Type *type = parseAbstractDeclaration();
		    expected(RPAREN);
		    expr = parseUnaryPrefixExpression();
		    expr = castExpr(loc, expr, type);
		}
		return expr;
	    }


	case MINUS2:
	    getToken();
	    return makeBinaryExpr(opLoc, EO_PREFIX_DECR,
				  parseUnaryPrefixExpression(),
				  makeSIntExpr(opLoc, 0, 1));

	case PLUS2:
	    getToken();
	    return makeBinaryExpr(opLoc, EO_PREFIX_INCR,
				  parseUnaryPrefixExpression(),
				  makeSIntExpr(opLoc, 0, 1));

	default:
	    return parsePostfixExpression(parsePrimaryExpression());
    }
    getToken();
    return makeUnaryExpr(opLoc, exprOp, parseUnaryPrefixExpression());
}

static const struct Expr *
makeMemExpr(const struct Expr *tagged, const char *member)
{
    const struct Type *t = unqualType(tagged->type);
    if (t->kind != TK_STRUCT && t->kind != TK_UNION) {
	errorMsg(&tagged->loc, "is neither struct nor union type");
	printf("type is: ");
	printType(tagged->type);
	printf("\n");
	exit(1);
	return 0;
    }
    const struct Symbol *memSym = getMemberSymbol(t->u.tag, member);
    if (! memSym) {
	errorMsg(&tagged->loc, "'%s' has no field '%s'",
		 tagged->type->u.tag->ident, member);
	return 0;
    }

    tagged = makeUnaryExpr(tagged->loc, EO_ADDR, tagged);
    ((struct Expr *) tagged)->type = makePointerType(makeSignedIntegerType(1));
    const struct Expr *d = makeSIntExpr(tagged->loc, 8, memSym->memLoc.offset);
    tagged = makeBinaryExpr(tagged->loc, EO_ADD, tagged, d);

    ((struct Expr *) tagged)->type = makePointerType(memSym->type);

    tagged = makeUnaryExpr(tagged->loc, EO_DEREF, tagged);
    ((struct Expr *) tagged)->type = memSym->type;
    return tagged;
}

static const struct Expr *
parsePostfixExpression(const struct Expr *expr)
{
    if (! expr) {
	return 0;
    }

    while (true) {
	if (tokenKind == PLUS2) {
	    if (! expr->is_lvalue) {
		errorMsg(&expr->loc, "expression is not assignable");
		exit(1);
		return 0;
	    }
	    expr = makeUnaryExpr(expr->loc, EO_POSTFIX_INCR, expr);
	    getToken();
	    continue;
	} else if (tokenKind == MINUS2) {
	    if (! expr->is_lvalue) {
		errorMsg(&expr->loc, "expression is not assignable");
		exit(1);
		return 0;
	    }
	    expr = makeUnaryExpr(expr->loc, EO_POSTFIX_DECR, expr);
	    getToken();
	    continue;
	} else if (tokenKind == ARROW || tokenKind == DOT) {
	    if (tokenKind == ARROW) {
		expr = makeUnaryExpr(expr->loc, EO_DEREF, expr);
	    }
	    getToken();
	    if (tokenKind != IDENT) {
		errorMsg(&tokenLoc, "identifier expected");
		exit(1);
		return 0;
	    }
	    const char *member = addString(tokenValue);
	    getToken();
	    expr = makeMemExpr(expr, member);
	    continue;
	} else if (tokenKind == LPAREN) {
	    getToken();
	    struct TokenLoc loc = expr->loc;
	    if (expr->type->kind == POINTER) {
		expr = makeUnaryExpr(expr->loc, EO_DEREF, expr);
	    }
	    const struct Type *ftype = expr->type;
	    if (ftype->kind != FUNCTION) {
		errorMsg(&expr->loc, "not a function");
		printf("expr is: ");
		printExpr(expr);
		printf("\n");
		printExprTree(expr);
		printf("\n");
		printf("type is: ");
		ftype = ftype->u.ref;
		printType(ftype);
		exit(1);
	    }
	    size_t numArgs = 0;
	    struct Expr *argList = makeExprList(tokenLoc);
	    while (true) {
		const struct Expr *arg = parseAssignmentExpression();
		if (! arg) {
		    break;
		}
		exprListAppend(argList, arg);
		++numArgs;
		if (tokenKind != COMMA) {
		    break;
		}
		getToken();
	    }
	    size_t numArgsExpected = 0;
	    bool dot3 = false;
	    for (; ftype->p.param[numArgsExpected]; ++numArgsExpected) {
		enum TypeKind kind = ftype->p.param[numArgsExpected]->kind;
		if (numArgsExpected && kind == TK_VOID) {
		    dot3 = true;
		}
	    }
	    expected(RPAREN);
	    if (numArgs != numArgsExpected && !dot3) {
		errorMsg(&loc, "%zu parameter(s) expected, got %zu",
			 numArgsExpected, numArgs);
		exit(1);
	    }

	    struct ExprListItem *argItem = argList->u.l.first;
	    for (size_t i = 0; i< numArgs; ++i, argItem = argItem->next) {
		if (ftype->p.param[i]->kind == TK_VOID) {
		    break;
		}
		if (! assignmentCompatible(argItem->expr->loc, true,
					   ftype->p.param[i],
					   argItem->expr, 0))
		{
		    errorMsg(&argItem->expr->loc, "arg %zu: wrong type", i);
		    printf("got type '");
		    printType(argItem->expr->type);
		    printf("' expected '");
		    printType(ftype->p.param[i]);
		    printf("'\n");
		    exit(1);
		}
	    }
	    expr = makeBinaryExpr(expr->loc, EO_CALL, expr, argList);
	    continue;
	} else if (tokenKind == LBRACKET) {
	    getToken();
	    const struct Expr *index = parseExpressionList();
	    expected(RBRACKET);
	    expr = makeBinaryExpr(index->loc, EO_ADD, expr, index);
	    expr = makeUnaryExpr(expr->loc, EO_DEREF, expr);
	    continue;
	}
	break;
    }

    return expr;
}

static const struct Expr *
parsePrimaryExpression()
{
    const struct Expr *expr = 0;
    switch (tokenKind) {
	case IDENT:
	    {
		const char *ident = tokenValue;
		const struct Symbol *sym = getSymbol(ident);
		if (! sym) {
		    errorMsg(&tokenLoc, "unkown identifier '%s'", ident);
		    exit(1);
		    return 0;
		} else if (sym->storageClass == SC_TYPEDEF) {
		    errorMsg(&tokenLoc, "expected expression before '%s'",
			     ident);
		    exit(1);
		    return 0;
		}
		if (sym->storageClass == SC_LITERAL) {
		    expr = makeLitExpr(tokenLoc, sym->type, sym->u.value); 
		} else {
		    expr = makeVarExpr(tokenLoc, sym);
		}
	    }
	    getToken();
	    break;
	case STRING_LITERAL:
	    {
		const char *str = addString(processedTokenValue);
		const char *escStr = addString(tokenValue);
		const struct Symbol *sym = getStringLiteral(str);
		if (! sym) {
		    sym = addStringLiteral(str, escStr);
		    appendNode(makeStrLit(sym->memLoc.name, sym->type, str,
					  escStr));
		}
		expr = makeVarExpr(tokenLoc, sym);
	    }
	    getToken();
	    break;

	case CHARACTER_LITERAL:
	    expr = makeUIntExpr(tokenLoc, 1, *processedTokenValue);
	    getToken();
	    break;

	case DECIMAL_LITERAL:
	case OCTAL_LITERAL:
	case HEXADECIMAL_LITERAL:
	    expr = makeLitExpr(tokenLoc, literalType, literalValue);
	    getToken();
	    break;
	case LPAREN:
	    getToken();
	    expr = parseExpressionList();
	    expected(RPAREN);
	    break;
	default:
	    ;
    }
    return expr;
}
