#include "expr.h"
#include "lexer.h"
#include "string.h"
#include "symtab.h"
#include "type.h"

int
main()
{
    union LiteralValue v;
    v.uint64 = 123;
    const struct Expr *v1 = makeLitExpr(tokenLoc, makeSignedIntegerType(2), v);
    v.uint64 = 2;
    const struct Expr *v2 = makeLitExpr(tokenLoc, makeSignedIntegerType(2), v);
    const struct Expr *s = makeBinaryExpr(tokenLoc, EO_ADD, v1, v2);

    printExprTree(s);
}
