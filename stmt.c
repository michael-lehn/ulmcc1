#include <stdlib.h>

#include "memregion.h"
#include "stmt.h"


struct Stmt *
appendStmt(struct Stmt *last, struct Stmt *next)
{
    last->next = next;
    return next;
}

struct Stmt *
makeStmt(struct TokenLoc loc, enum StmtKind kind)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = kind;
    stmt->loc = loc;
    stmt->next = 0;
    return stmt;
}

struct Stmt *
makeCompoundStmt(struct TokenLoc loc, const struct Stmt *body)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = STMT_COMPOUND;
    stmt->loc = loc;
    stmt->u.body = body;
    stmt->next = 0;
    return stmt;
}

struct Stmt *
makeExprStmt(struct TokenLoc loc, enum StmtKind kind, const struct Expr *expr)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = kind;
    stmt->loc = loc;
    stmt->u.expr = expr;
    stmt->next = 0;
    return stmt;
}

struct Stmt *
makeLoopStmt(struct TokenLoc loc, enum StmtKind kind, const struct Expr *cond,
	     const struct Stmt *body)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = kind;
    stmt->loc = loc;
    stmt->u.l.init = 0;
    stmt->u.l.cond = cond;
    stmt->u.l.incr = 0;
    stmt->u.l.stmt = body;
    stmt->next = 0;
    return stmt;
}

struct Stmt *
makeForLoopStmt(struct TokenLoc loc, const struct Stmt *init,
		const struct Expr *cond, const struct Expr *incr,
		const struct Stmt *body)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = STMT_FOR;
    stmt->loc = loc;
    stmt->u.l.init = init;
    stmt->u.l.cond = cond;
    stmt->u.l.incr = incr;
    stmt->u.l.stmt = body;
    stmt->next = 0;
    return stmt;
}

struct Stmt *
makeIfStmt(struct TokenLoc loc, const struct Expr *cond,
	   const struct Stmt *thenStmt, const struct Stmt *elseStmt)
{
    struct Stmt *stmt = alloc(STMT, sizeof(*stmt));
    stmt->kind = STMT_IF;
    stmt->loc = loc;
    stmt->u.c.cond = cond;
    stmt->u.c.thenStmt = thenStmt;
    stmt->u.c.elseStmt = elseStmt;
    stmt->next = 0;
    return stmt;
}

static void
printIndent(size_t indent)
{
    for (size_t i = 0; i < indent; ++i) {
	printf(" ");
    }
}

static void
printStmt_(const struct Stmt *stmt, size_t indent)
{
    if (! stmt) {
	return;
    }

    switch (stmt->kind) {
	case STMT_BEGIN:
	    break;

	case STMT_BREAK:
	    printIndent(indent);
	    printf("break;\n");
	    break;

	case STMT_CONTINUE:
	    printIndent(indent);
	    printf("continue;\n");
	    break;

	case STMT_RETURN:
	    printIndent(indent);
	    printf("return ");
	    printExpr(stmt->u.expr);
	    printf(";\n");
	    break;

	case STMT_ASSIGN:
	    printExpr(stmt->u.expr);
	    printf(";\n");
	    break;

	case STMT_COMPOUND:
	    printStmt_(stmt->u.body, indent + 4);
	    break;

	case STMT_IF:
	    printIndent(indent);
	    printf("if ");
	    printExpr(stmt->u.c.cond);
	    printf("\n");
	    printIndent(indent);
	    printf("then\n");
	    printStmt_(stmt->u.c.thenStmt, indent + 4);
	    printIndent(indent);
	    printf("else\n");
	    printStmt_(stmt->u.c.elseStmt, indent + 4);
	    break;

	case STMT_DO:
	    printIndent(indent);
	    printf("do\n");
	    printStmt_(stmt->u.l.stmt, indent + 4);
	    printf("while ");
	    printExpr(stmt->u.l.cond);
	    printf("\n");
	    break;

	case STMT_WHILE:
	    printIndent(indent);
	    printf("while ");
	    printExpr(stmt->u.l.cond);
	    printf("\n");
	    printStmt_(stmt->u.l.stmt, indent + 4);
	    break;

	case STMT_FOR:
	    printIndent(indent);
	    printf("for ");
	    printStmt(stmt->u.l.init);
	    printf("; ");
	    printExpr(stmt->u.l.cond);
	    printf("; ");
	    printExpr(stmt->u.l.incr);
	    printStmt_(stmt->u.l.stmt, indent + 4);
    }

    printStmt_(stmt->next, indent);
}

void
printStmt(const struct Stmt *stmt)
{
    printStmt_(stmt, 0);
}
